<?php

use Illuminate\Database\Seeder;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'name'=>'admin',
            'email'=>'nauman@admin.com',
            'password'=>bcrypt('nauman@4321'),
            'role'=>1,
        ]);

        DB::table('users')->insert([
            'name'=>'admin',
            'email'=>'nomiadmin@admin.com',
            'password'=>bcrypt('nomiadmin@1234'),
            'role'=>1,
        ]);
    }
}
