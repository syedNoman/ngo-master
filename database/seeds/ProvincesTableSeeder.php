<?php

use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//        DB::table('provinces')->insert();
//        $countries = [
//            ['name' => 'Punjab', 'code' => 'PK'],
//            ['name' => 'Sindh', 'code' => 'PK'],
//            ['name' => 'Balochistan', 'code' => 'PK'],
//            ['name' => 'Khyber Pakhtunkhwa', 'code' => 'PK'],
//            ];
//        DB::table('provinces')->insert($countries);


        DB::table('provinces')->insert([
            ['name' => 'Punjab', 'code' => 'PK'],
        ]);
        DB::table('provinces')->insert([
            ['name' => 'Sindh', 'code' => 'PK'],
        ]);
        DB::table('provinces')->insert([
            ['name' => 'Balochistan', 'code' => 'PK'],
        ]);
        DB::table('provinces')->insert([
            ['name' => 'Khyber Pakhtunkhwa', 'code' => 'PK'],
        ]);

    }
}
