<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnionConcilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('union_concils', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tehsil_id')->nullable();
            $table->string('name');
            $table->timestamps();
            $table->foreign('tehsil_id')
                ->references('id')->on('tehsils')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('union_concils');
    }
}
