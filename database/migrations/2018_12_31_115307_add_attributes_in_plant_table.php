<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributesInPlantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plants', function (Blueprint $table) {
//
            $table->string('city')->nullable()->after('id');
            $table->string('code')->nullable()->after('city');
            $table->string('name')->nullable()->after('code');
            $table->integer('filter_quantity')->nullable()->after('name');
            $table->date('implementation_date')->nullable()->after('filter_quantity');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plant', function (Blueprint $table) {
            //
        });
    }
}
