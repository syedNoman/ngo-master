<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('form_no')->nullable();
            $table->string('barcode')->nullable();
            $table->tinyInteger('card_issued')->default(0);
            $table->date('card_issue_date')->nullable();
            $table->longText('thumbnail')->nullable();

            $table->integer('distric_id')->unsigned()->nullable();
            $table->integer('tehsil_id')->unsigned()->nullable();
            $table->integer('union_council_id')->unsigned()->nullable();

            $table->string('village')->nullable();
            $table->string('name')->nullable();
            $table->string('father_name')->nullable();
            $table->string('cnic')->nullable();
            $table->string('mobile')->nullable();
            $table->text('address')->nullable();
            $table->string('way_of_income')->nullable();
            $table->integer('house_member')->nullable();
            $table->integer('male')->nullable();
            $table->integer('female')->nullable();
            $table->integer('less_five_year_child')->nullable();
            $table->integer('school_child')->nullable();

            $table->foreign('distric_id')
                ->references('id')->on('districs')
                ->onDelete('cascade');

            $table->foreign('tehsil_id')
                ->references('id')->on('tehsils')
                ->onDelete('cascade');

            $table->foreign('union_council_id')
                ->references('id')->on('union_concils')
                ->onDelete('cascade');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumers');
    }
}
