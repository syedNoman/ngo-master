<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('consumer_id')->unsigned()->nullable();
            $table->integer('plant_id')->unsigned()->nullable();
            $table->integer('today_consumer_serve')->nullable();
            $table->bigInteger('total_consumer_serve')->nullable();
            $table->bigInteger('today_total_consumer_serve')->nullable();
            $table->bigInteger('total_consumption')->nullable();
            $table->timestamps();

            $table->foreign('consumer_id')
                ->references('id')->on('consumers')
                ->onDelete('cascade');

            $table->foreign('plant_id')
                ->references('id')->on('plants')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumptions');

    }
}
