<?php

namespace App\Exports;

use App\Consumer;
use App\People;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
//        return People::all('name','father_name','cnic','barcode');
        return Consumer::all([
            'name'=>'name',
            'father_name'=>'father_name',
            'cnic'=>'cnic',
            'barcode'=>'barcode',
        ]);
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Name',
            'Father Name',
            'CNIC',
            'Barcode',
        ];
    }
}
