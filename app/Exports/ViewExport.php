<?php

namespace App\Exports;

use App\Consumer;
use App\People;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class ViewExport implements FromView
{

    public function view(): View
    {
        return view('admin.consumer.viewexcel', [
            'users' => Consumer::all()
        ]);
    }
}
