<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consumption extends Model
{
    protected $fillable=[
        'today_consumer_serve',
        'total_consumer_serve',
        'today_total_consumer_serve',
        'total_consumption',
    ];

    public function conse(){
        return $this->hasOne('App\Consumer','consumer_id','id');

    }
}
