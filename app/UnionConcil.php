<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnionConcil extends Model
{
    protected $fillable=[
        'name',
        'tehsil_id',
    ];

    public function tehsil(){
        return $this->belongsTo('App\Tehsil');
    }
}
