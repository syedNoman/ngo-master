<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    protected $fillable=[
        'user_id',
        'complaint',
        'status',
    ];

    public function user(){
        return $this->belongsTo('App\Consumer');
    }
}
