<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    protected $fillable=[
        'user_id',
        'plant_id',
        'start_date',
        'change_date',
        'days',
        'quantity',
    ];

    public function plant(){
        return $this->belongsTo('App\Plant');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
