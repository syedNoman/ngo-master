<?php

namespace App\Http\Controllers\Consumer;

use App\Consumer;
use App\Consumption;
use App\Distric;
use App\Tehsil;
use App\UnionConcil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ConsumerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function Consumer()
    {
//        dd((Auth::user()->id));

         if(Auth::user()->role=='1'){
//             $consumers=Consumer::paginate(100)->last();
             $consumers=Consumer::orderBy('id', 'desc')->paginate(100);
             return view('admin.consumer.index',compact('consumers'));
         }
         else{
             $consumers=Consumer::orderBy('id', 'desc')->where('user_id', Auth::user()->id)->paginate(100);
             return view('admin.consumer.index',compact('consumers'));
         }

    }





    public function ConsumerCreate()
    {

         $districs=Distric::all();
         $tehsils=Tehsil::all();
         $concils=UnionConcil::all();
        return view('admin.consumer.create',compact('districs','tehsils','concils'));
    }

    public function ConsumerSave(Request $request)
    {
//        dd($request->all());

        $request->validate([
         'cnic'=>'required|min:3|unique:consumers'
     ]);


        $bar=bin2hex(openssl_random_pseudo_bytes(5));

        //data insert into the table
        $consmuer= new Consumer();

        $consmuer_create=$consmuer->insert([
            'plant_id'=>Auth()->user()->plant_id,
            'user_id'=>Auth()->user()->id,
            'barcode'=>$bar,
            'form_no'=>$request->form_no,
//            'card_issue_date'=>\Carbon\Carbon::now(),
            'thumbnail'=>$request->thumbnail,
            'distric_id'=>$request->distric_id,
            'tehsil_id'=>$request->tehsil_id,
            'union_council_id'=>$request->union_council_id,
            'village'=>$request->village,
            'name'=>$request->name,
            'father_name'=>$request->father_name,
            'cnic'=>$request->cnic,
            'mobile'=>$request->mobile,
            'address'=>$request->address,
            'way_of_income'=>$request->way_of_income,
            'house_member'=>$request->house_member,
            'male'=>$request->male,
            'female'=>$request->female,
            'less_five_year_child'=>$request->less_five_year_child,
            'school_child'=>$request->school_child,
            "created_at" =>  \Carbon\Carbon::now(),
            "updated_at" => \Carbon\Carbon::now(),
        ]);


        if($consmuer){
            return  back()->with('message','User has been Added Successfully');
        }

    }

    public function ConsumerDelete($id){
        $cosumer = Consumer::findOrFail($id);
        $delt=$cosumer->delete();
        if($delt){
            return back()->with('message', 'User has been Deleted  Successfully');
        }
    }

    public function ConsumerEdit($id){
        $user=Consumer::find($id);
        $districs=Distric::all();
        $tehsils=Tehsil::all();
        $concils=UnionConcil::all();
        return view('admin.consumer.edit',compact('user','districs','tehsils','concils'));
    }

    public function ConsumerShow($id){
        $user=Consumer::find($id);
        $consump=Consumption::where('consumer_id',$user->id)->first();
        return view('admin.consumer.view',compact('user','consump'));
    }

    public function ConsumerUpdate($id,Request $request){
//        dd($request->all());
        $thumbnail="";
         $user=Consumer::find($id);

        if ($file = $request->hasFile('thumbnail_upload')) {
            $extension = $request->file('thumbnail_upload')->extension();
            $imageData = base64_encode(file_get_contents($request->file('thumbnail_upload')));
            $thumbnail = 'data:image/' . $extension . ';base64,' . $imageData;
            $user->update(['thumbnail'=>$thumbnail]);
        }
        $update=$user->update($request->all());
        if ($update) {
            return back()->with('message', 'User has been Updated Successfully');
        }


    }

    public function ExcelExport()
    {
        return Excel::download(new ViewExport(), 'Member-List.xlsx');
    }

}
