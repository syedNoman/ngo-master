<?php

namespace App\Http\Controllers\Distric;

use App\Distric;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DistricController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function Distric()
    {
        $districs=Distric::paginate('10');
        $provinces=DB::table('provinces')->get();

//
//        foreach ($provinces as $province)
//        {
//            dd($province->name);
//        }


        return view('admin.distric.list-distric',compact('districs','provinces'));
    }

    public function CreateDistric(Request $request)
    {
        $create=Distric::create($request->all());
        if($create){
            return  back()->with('message','District has been Added Successfully');
        }
    }

    public function DelDistric($id){
        $del = Distric::findOrFail($id);
        $delt=$del->delete();
        if($delt){
            return back()->with('message-delete', ' District has been Deleted Successfully');
        }
    }

    public function EditDistric($id){
        $distric=Distric::find($id);
        $provinces=DB::table('provinces')->get();
        return view('admin.distric.edit-distric',compact('distric','provinces'));
    }

    public function UpdateDistric($id,Request $request){
        $distric=Distric::find($id);
        $update=$distric->update($request->all());
        if ($update) {
            return back()->with('message', 'District has been Updated Successfully');
        }
    }
}
