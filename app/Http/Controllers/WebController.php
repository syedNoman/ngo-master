<?php

namespace App\Http\Controllers;

use App\Consumer;
use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index(){
        $users=Consumer::all();
        return view('admin.consumer.barcode_scaner_view',compact('users'));
    }
}
