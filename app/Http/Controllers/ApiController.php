<?php

namespace App\Http\Controllers;

use App\Complaint;
use App\Consumer;
use App\Consumption;
use App\Filter;
use App\Plant;
use App\TDS;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{

    // Login Api
    public function AdminLogin(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'password' => 'required',
        ]);

        if ($validator->fails()){
            return response($validator->errors(),500);
        }

        $user = User::where('email', '=', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $res['status'] = true;
                $status = 200;
                $res['message'] = 'Successfully Login';
                $res['user'] = array($user);


                return $res;
            } else {
                $res['status'] = false;
                $status = 500;
                $res['message'] = 'Password not matched';
            }
        } else {
            $res['status'] = true;
            $status = 500;
            $res['message'] = 'user not exist with email = ' . $request->email;
        }

        return response()->json([$res], $status);

    }

    // Get All Plant Api
    public function getDetail(){
       $plant = Plant::all();
       if ($plant->count() > 0) {

           $response['status'] = true;
           $response['data'] = $plant;
       }
       else
       {

           $response['status'] = false;
           $response['message'] = 'Plants not found';
       }
       return response()->json($response);
   }

    // Get Specific Plant Detail
    public function PlantDetail(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required',

        ]);

        if ($validator->fails()){
            return response($validator->errors(),500);
        }

//        dd($request->id);

        $plants=Plant::all();


        $maintainer=User::where('plant_id',$request->id)->count('name');
//
//
        $member_total=Consumer::where('plant_id',$request->id)->count();
//
//
//
        $pending=Complaint::where('status',0)->count();
        $processing=Complaint::where('status',1)->count();
        $complete=Complaint::where('status',2)->count();


        $no_card_issue=Consumer::where('plant_id',$request->id)->where('card_issued',0)->count();
        $user_card_issue=Consumer::where('plant_id',$request->id)->where('card_issued',1)->count();
//
        $total_consumption=Consumption::where('plant_id',$request->id)->pluck('total_consumer_serve')->sum();


        if ($plants->count() > 0) {

            $response['status'] = true;
            $response['plant'] = $plants;
            $response['maintainer'] = $maintainer;
            $response['member_total'] = $member_total;
            $response['no_card_issue'] = $no_card_issue;
            $response['user_card_issue'] = $user_card_issue;
            $response['total_consumption'] = $total_consumption;

            $response['pending'] = $pending;
            $response['processing'] = $processing;
            $response['complete'] = $complete;
        }
        else
        {

            $response['status'] = false;
            $response['message'] = 'Plants not found';
        }
        return ($response);


    }




    // Get Specific Plant Consumer
    public function PlantConsumer(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required',

        ]);

        if ($validator->fails()){
            return response($validator->errors(),500);
        }

//        dd($request->id);


        $member_total=Consumer::where('plant_id',$request->id)->count();
        $member_show=Consumer::where('plant_id',$request->id)->get();



        if ($member_show->count() > 0) {

            $response['status'] = true;
            $response['member_total'] = $member_total;
            $response['member_show'] = $member_show;

        }
        else
        {

            $response['status'] = false;
            $response['message'] = 'Plants not found';
        }
        return ($response);


    }


    // Get Specific Complaint Cnic
    public function complaint(Request $request){

        $validator = Validator::make($request->all(), [
            'cnic' => 'required',

        ]);

        if ($validator->fails()){
            return response($validator->errors(),500);
        }

        $member_show=Consumer::where('cnic',$request->cnic)->first(['id','thumbnail','mobile','name','father_name','village','card_issued']);


        if ($member_show) {

            $response['status'] = true;
            $response['member_show'] = $member_show;

        }
        else
        {

            $response['status'] = false;
            $response['message'] = 'Consumer not found against this CNIC.';
        }
        return ($response);


    }


    public function complaintStatus(Request $request){

        $validator = Validator::make($request->all(), [

        ]);

        if ($validator->fails()){
            return response($validator->errors(),500);
        }

        $member_show=Complaint::with('user')->get();


        if ($member_show) {

            $response['status'] = true;
            $response['member_show'] = $member_show;

        }
        else
        {

            $response['status'] = false;
            $response['message'] = 'Complaint not found';
        }
        return ($response);


    }

    // Add Specific Complaint base on Cnic
    public function complaintAdd(Request $request){
//        dd($request->all());

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'complaint' => 'required',

        ]);

        if ($validator->fails()){
            return response($validator->errors(),500);
        }


        $complaint_add=Complaint::create($request->all());


        if ($complaint_add) {

            $response['status'] = true;
            $response['message'] = 'Complaint has been add Successfully';

        }
        else
        {

            $response['status'] = false;
            $response['message'] = 'Complaint not found';
        }
        return ($response);


    }

    // Edit Specific Complaint base on id
    public function complaintEdit(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'status' => 'required',

        ]);

        if ($validator->fails()){
            return response($validator->errors(),500);
        }

        $comp=Complaint::find($request->id);
        if($comp){
            $update=$comp->update($request->all());
            $response['status'] = true;
            $response['message'] = 'Complaint has been Update Successfully';
        }

        else
        {

            $response['status'] = false;
            $response['message'] = 'Complaint not found against this id.';
        }
        return ($response);


    }

    // Edit Specific Complaint base on id
    public function complaintCheck(Request $request){

        $validator = Validator::make($request->all(),[
            'status' => 'required',

        ]);

        if ($validator->fails()){
            return response($validator->errors(),500);
        }

        $comp=Complaint::where('status',$request->status)->with('user')->get();
        if($comp){
            $response['status'] = true;
            $response['complaint_show'] = $comp;
        }
        else
        {
            $response['status'] = false;
            $response['message'] = 'Complaint not found against this id.';
        }
        return ($response);
    }

    // Get Specific Plant Tds
    public function PlantTds(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required',

        ]);

        if ($validator->fails()){
            return response($validator->errors(),500);
        }




        $member_shows=Tds::where('plant_id',$request->id)->get();



        if ($member_shows->count() > 0) {
            foreach ($member_shows as $member_show) {
                $member_show['plant_name'] = $member_show->plant->name;
            }
            $response['status'] = true;
            $response['member_show'] = $member_shows;
        }
        else
        {

            $response['status'] = false;
            $response['message'] = 'Tds not found';
        }
        return ($response);


    }

    // Get Specific Plant Filter Change History
    public function PlantFilter(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required',

        ]);

        if ($validator->fails()){
            return response($validator->errors(),500);
        }


        $member_shows=Filter::where('plant_id',$request->id)->with('user')->with('plant')->get(['id','user_id','plant_id','start_date','change_date','days','quantity',]);



        if ($member_shows->count() > 0) {
            foreach ($member_shows as $member_show) {

                $member_show['user_name'] = $member_show->user->name;
                $member_show['plant_name'] = $member_show->plant->name;
            }
            $response['status'] = true;
            $response['member_show'] = $member_shows;
        }
        else
        {

            $response['status'] = false;
            $response['message'] = 'Filter Information not found';
        }
        return ($response);


    }

    // Get Specific Plant PlantReport
    public function PlantReportDate(Request $request){



        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'date' => 'required',

        ]);

        if ($validator->fails()){
            return response($validator->errors(),500);
        }


        $card_issue=Consumer::whereDate('created_at',$request->date)->where('plant_id',$request->id)->where('card_issued',1)->count();

        $member_registerd=Consumer::whereDate('created_at',$request->date)->where('plant_id',$request->id)->count();

        $card_not_issue=Consumer::whereDate('created_at',$request->date)->where('plant_id',$request->id)->where('card_issued',0)->count();
        $consu=Consumption::whereDate('updated_at',$request->date)->where('plant_id',$request->id)->pluck('total_consumer_serve')->sum();


        if (1 > 0) {

            $response['status'] = true;
            $response['card_issue'] = $card_issue;
            $response['member_registerd'] = $member_registerd;
            $response['card_not_issue'] = $card_not_issue;
            $response['consu'] = $consu;
        }
        else
        {

            $response['status'] = false;
            $response['message'] = 'Plants not found';
        }
        return ($response);


    }



}
