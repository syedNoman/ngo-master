<?php

namespace App\Http\Controllers;

use App\Complaint;
use App\Consumer;
use App\Consumption;
use App\Exports\UsersExport;
use App\Exports\ViewExport;
use App\People;
use App\Plant;
use App\User;
use Illuminate\Database\Connection;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use function Sodium\randombytes_random16;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function WaterServe(){
        $users=Consumer::all();
        return view('admin.consumer.barcode_scaner_view',compact('users'));
    }



    public function ListMemeber()
    {
        if(Auth::user()->name=='admin'){
            $consumers=Consumer::where('card_issued',0)->get();
            return view('admin.report.list_member',compact('consumers'));
        }
        $consumers=Consumer::where('user_id', Auth::user()->id)->where('card_issued',0)->get();
        return view('admin.report.list_member',compact('consumers'));
    }



    public function Updatecol(){
        DB::table('consumptions')->where('today_consumer_serve', '=',0)
            ->chunkById(100, function ($consumps) {
                foreach ($consumps as $consu) {
                    DB::table('consumptions')
                        ->where('id', $consu->id)
                        ->update(['today_consumer_serve' => 19]);
                }
            });

        DB::table('consumptions')->where('today_total_consumer_serve', '>',0)
            ->chunkById(100, function ($consumps) {
                foreach ($consumps as $consu) {
                    DB::table('consumptions')
                        ->where('id', $consu->id)
                        ->update(['today_total_consumer_serve' => 0]);
                }
            });

    }


    public static $add_water_serve_static=null ;

    public function ajxmethod(Request $request){

        $consumer=Consumer::where('barcode',$request->id)->first();
        $sum_total_consumer_serve = DB::table('consumptions')->sum('today_total_consumer_serve');
        if($consumer==null){

            $invalid_card="";
            $invalid_card.=0;
            $res['status'] = true;
            $res['invalid_card'] = $invalid_card;
            return $res;
        }

        $update=$consumer->update(['card_issued'=>1,
            'card_issue_date'=>\Carbon\Carbon::now(),
        ]);



        $consump=new Consumption();

        $consmp=Consumption::where('consumer_id',$consumer->id)->first();
        $total_sum_total_consumer_serve=DB::table('consumptions')->pluck('total_consumer_serve')->sum();

        if($consmp==null){
            $consump->insert([
                'consumer_id'=>$consumer->id,
                'plant_id'=>Auth()->user()->plant_id,
                'today_consumer_serve'=>'0',
                'total_consumer_serve'=>'19',
                'today_total_consumer_serve'=>19,
                'total_consumption'=>$total_sum_total_consumer_serve+19,
            ]);
            if($consump){
                $consmp=Consumption::where('consumer_id',$consumer->id)->first();
                $total_water_serve=$consmp->total_consumer_serve;


                $sum_total_consumpation=DB::table('consumptions')->pluck('total_consumer_serve')->sum();
                $option="";
                $cons_pic="";
                $consmp_ser_water="";
                $total_consmp_ser_water="";
                $today_total_consumer_serve="";
                $total_consumpation="";
//                $consmp_ser_water.="$consmp->today_consumer_serve";
                $total_consmp_ser_water.="$total_water_serve";
                $today_total_consumer_serve.=$sum_total_consumer_serve+19;
                $total_consumpation.=$sum_total_consumpation;
                $option.="$consumer->name";
                $cons_pic.=" <img style='height:130px;padding:0px;width: 100%;' class='' src='".("$consumer->thumbnail")."'> ";
                $res['status'] = true;
                $res['options'] = $option;
                $res['cons_pics'] = $cons_pic;
                $res['consmp_ser_water'] = '19';
                $res['total_consmp_ser_water'] = $total_consmp_ser_water;
                $res['today_total_consumer_serve'] = $today_total_consumer_serve;
                $res['total_consumpation'] = $total_consumpation;

                return $res;



            }
//
        }


        else{
            $add_water_serve=$consmp->today_consumer_serve;

            if($add_water_serve==0){

                $total_water_serve=$consmp->total_consumer_serve;

                $option="";
                $cons_pic="";
                $consmp_ser_water="";
                $total_consmp_ser_water="";
                $today_total_consumer_serve="";
                $total_consumpation="";
                $consmp_ser_water.="$consmp->today_consumer_serve";
                $total_consmp_ser_water.="$total_water_serve";
                $today_total_consumer_serve.=$sum_total_consumer_serve;
                $total_consumpation.=Consumption::where('plant_id',Auth::user()->plant_id)->sum('total_consumer_serve');
                $option.="$consumer->name";
                $cons_pic.=" <img style='height:130px;padding:0px;width: 100%;' class='' src='".("$consumer->thumbnail")."'> ";
                $res['status'] = true;
                $res['options'] = $option;
                $res['cons_pics'] = $cons_pic;
                $res['consmp_ser_water'] = $consmp_ser_water;
                $res['total_consmp_ser_water'] = $total_consmp_ser_water;
                $res['today_total_consumer_serve'] = $today_total_consumer_serve;
                $res['total_consumpation'] = $total_consumpation;
                return $res;

            }

            if($add_water_serve==19){
                $total_water_serve=$consmp->total_consumer_serve;

                $grnd_water_serve=$total_water_serve+19;
                $sum_total_consumer_serve = DB::table('consumptions')->sum('total_consumer_serve');

                $consmp->update([
                    'total_consumer_serve'=>$grnd_water_serve,
                    'today_consumer_serve'=>0,
                    'today_total_consumer_serve'=>19,
                    'total_consumption'=>$sum_total_consumer_serve,
                ]);

                $total_water_serve=$consmp->total_consumer_serve;
                $sum_today_total_consumer_serve = DB::table('consumptions')->sum('today_total_consumer_serve');
                $sum_total_consumer_serve = DB::table('consumptions')->sum('total_consumer_serve');
//
            $option="";
            $cons_pic="";
            $consmp_ser_water="";
            $total_consmp_ser_water="";
            $today_total_consumer_serve="";
            $total_consumpation="";
            //$consmp_ser_water.="$consmp->today_consumer_serve";
            $total_consmp_ser_water.="$total_water_serve";
            $today_total_consumer_serve.=$sum_today_total_consumer_serve;
            $total_consumpation.=$sum_total_consumer_serve;
            $option.="$consumer->name";
            $cons_pic.=" <img style='height:130px;padding:0px;width: 100%;' class='' src='".("$consumer->thumbnail")."'> ";
            $res['status'] = true;
            $res['options'] = $option;
            $res['cons_pics'] = $cons_pic;
            $res['consmp_ser_water'] = '19';
            $res['total_consmp_ser_water'] = $total_consmp_ser_water;
            $res['today_total_consumer_serve'] = $today_total_consumer_serve;
            $res['total_consumpation'] = $total_consumpation;

            return $res;
            }

//

//            $total_water_serve=$consmp->total_consumer_serve;
//
//            $option="";
//            $cons_pic="";
//            $consmp_ser_water="";
//            $total_consmp_ser_water="";
//            $consmp_ser_water.="$consmp->today_consumer_serve";
//            $total_consmp_ser_water.="$total_water_serve";
//            $option.="$consumer->name";
//            $cons_pic.=" <img style='height: 218px;padding: 27px;width: 100%;' class='' src='".("$consumer->thumbnail")."'> ";
//            $res['status'] = true;
//            $res['options'] = $option;
//            $res['cons_pics'] = $cons_pic;
//            $res['consmp_ser_water'] = $consmp_ser_water;
//            $res['total_consmp_ser_water'] = $total_consmp_ser_water;
//
//            return $res;

        }


    }


    public function ajxmethod2(Request $request){
//        dd($request->id);
        $consumer=Consumer::where('cnic',$request->id)->first();

        if($consumer){
            $cons_pic="";
            $cons_cell="";
            $name="";
            $cons_fname="";
            $cons_village="";
            $cons_card="";
            $no_cons_card="";
            $card_issue_date="";


            $cons_pic.=" <img style='height: 218px;padding: 27px;width: 100%;' class='' src='".("$consumer->thumbnail")."'> ";

            $cons_cell.="$consumer->mobile";
            $name.="$consumer->name";
            $cons_fname.="$consumer->father_name";
            $cons_village.="$consumer->village";
            $cons_card.="$consumer->card_issued";
            $card_issue_date.="$consumer->card_issue_date";


            $res['status'] = true;
            $res['cons_pics'] = $cons_pic;
            $res['cons_cell'] = $cons_cell;
            $res['options'] = $name;
            $res['$cons_fname'] = $cons_fname;
            $res['$cons_village'] = $cons_village;
            if($cons_card == 1){
                $res['cons_card'] = '<span style="color: green">Card Issue</span>';
            }
            else{
                $res['cons_card'] = '<span style="color: Red">No Card Issue</span>';
            }

            $res['$card_issue_date'] = $card_issue_date;

//            no_cons_card
            return $res;
        }


    }

    public function index()
    {
        $pending=Complaint::where('status',0)->count();
        if($pending){
            $pend=1;
            return view('admin.welcome3',compact('pend'));
        }
        else{
            $pend=0;
            return view('admin.welcome3',compact('pend'));
        }


    }
    public function complaintDashboard()
    {


        return view('admin.complaint-dashboard');
    }



    public function users()
    {
        $users=User::where('role','!=',1)->paginate(5);
        $plants=Plant::all();
        return view('admin.users.list-users',compact('users','plants'));

//        Connection::
    }


    public function usersAdd(Request $request)
    {
//        dd($request->all());

        $user=new User();

        $usr=$user->insert([
            'plant_id'=>$request->plant_id,
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'role'=>0,
        ]);

       if($usr){
           return  back()->with('message','Maintainer has been Added Successfully');
       }

    }

    public function usersDelete($id){
//        dd('id');
        $del = User::findOrFail($id);
        $delt=$del->delete();
        if($delt){
            return back()->with('message-delete', 'Maintainer has been Delete  Successfully');
        }
    }

    public function usersEdit($id){
//        dd($id);

        $plants=Plant::all();
        $user=User::find($id);
        return view('admin.users.edit-users',compact('user','plants'));
    }



    public function UpdateUser($id,Request $request){
//        dd( $request->all());
//       dd(bcrypt($request->password));

        $user=User::find($id);


        $update=$user->update(['name'=>$request->name,'email'=>$request->email,'password'=>bcrypt($request->password)]);

        if ($update) {
            return back()->with('message', 'Maintainer has been Update Successfully');
        }


    }


    public function ExcelExport()
    {
        return Excel::download(new ViewExport(), 'Member-List.xlsx');
    }

    public function Reports(){
//        dd('report');
        $plants=Plant::all();
        $total_consumers=Consumer::paginate(3);
        return view('admin.report.report',compact('plants','total_consumers'));

    }

    public function ReportsPlant(Request $request){

        $plants=Plant::all();
        $users=User::where('plant_id',$request->id)->count('name');


        $member_total=Consumer::where('plant_id',$request->id)->count();

        $total_consumer=Consumer::where('plant_id',$request->id)->paginate(3);
        if($total_consumer){
            $total_consumers=$total_consumer;
        }


        $no_card_issue=Consumer::where('plant_id',$request->id)->where('card_issued',0)->count();
        $user_card_issue=Consumer::where('plant_id',$request->id)->where('card_issued',1)->count();

        $total_consumption=Consumption::where('plant_id',$request->id)->pluck('total_consumer_serve')->sum();

//
//        echo($users->count('name'));

        return view('admin.report.report',compact('users','plants','member_total','no_card_issue','user_card_issue','total_consumption','total_consumers'));

    }

    public function ReportGet(Request $request){

        $date1 = Carbon::today()->toDateString();
        $date1 = Carbon::yesterday()->toDateString();

        $cons=Consumer::whereDate('created_at',$request->date)->get();
        foreach($cons as $con){
              echo  $con->name;
              echo '<td>$con->name</td>';
        }

//        dd($request->all());
    }


    public  function YearFilter(Request $request){

        $year = explode('-',$request->data)[0];
        $month = explode('-',$request->data)[1];

        $card_issue=Consumer::whereYear('created_at',$year)->whereMonth('created_at',$month)->where('plant_id',$request->id)->where('card_issued',1)->count();
        $member_registerd=Consumer::whereYear('created_at',$year)->whereMonth('created_at',$month)->where('plant_id',$request->id)->count();

        $card_not_issue=Consumer::whereYear('created_at',$year)->whereMonth('created_at',$month)->where('plant_id',$request->id)->where('card_issued',0)->count();



        $consu=Consumption::whereYear('updated_at',$year)->whereMonth('updated_at',$month)->where('plant_id',$request->id)->pluck('total_consumer_serve')->sum();



        $member_register="";
        $member_register.=$member_registerd;
        $card_issued="";
        $card_issued.=$card_issue;
        $card_not_issued="";
        $card_not_issued.=$card_not_issue;
        $consum="";
        $consum.=$consum;
        $res['status'] = true;
        $res['member_registerd'] = $member_register;
        $res['card_issued'] = $card_issued;
        $res['card_not_issued'] = $card_not_issued;
        $res['consum'] = $consu;
        return $res;



    }


    public  function DateFilter(Request $request){


        $card_issue=Consumer::whereDate('created_at',$request->data)->where('plant_id',$request->id)->where('card_issued',1)->count();

        $member_registerd=Consumer::whereDate('created_at',$request->data)->where('plant_id',$request->id)->count();

        $card_not_issue=Consumer::whereDate('created_at',$request->data)->where('plant_id',$request->id)->where('card_issued',0)->count();
        $consu=Consumption::whereDate('updated_at',$request->data)->where('plant_id',$request->id)->pluck('total_consumer_serve')->sum();

        $member_register="";
        $member_register.=$member_registerd;
        $card_issued="";
        $card_issued.=$card_issue;
        $card_not_issued="";
        $card_not_issued.=$card_not_issue;
        $consum="";
        $consum.=$consum;
        $res['status'] = true;
        $res['member_registerd'] = $member_register;
        $res['card_issued'] = $card_issued;
        $res['card_not_issued'] = $card_not_issued;
        $res['consum'] = $consu;
        return $res;



    }
    public  function RangeFilter(Request $request){

//        dd($request->data,$request->end);


        $card_issue=Consumer::whereBetween('created_at',[$request->data,$request->end])->where('plant_id',$request->id)->where('card_issued',1)->count();
//        dd($card_issue);

        $member_registerd=Consumer::whereBetween('created_at',[$request->data,$request->end])->where('plant_id',$request->id)->count();

        $card_not_issue=Consumer::whereBetween('created_at',[$request->data,$request->end])->where('plant_id',$request->id)->where('card_issued',0)->count();
        $consu=Consumption::whereBetween('updated_at',[$request->data,$request->end])->where('plant_id',$request->id)->pluck('total_consumer_serve')->sum();

        $member_register="";
        $member_register.=$member_registerd;
        $card_issued="";
        $card_issued.=$card_issue;
        $card_not_issued="";
        $card_not_issued.=$card_not_issue;
        $consum="";
        $consum.=$consum;
        $res['status'] = true;
        $res['member_registerd'] = $member_register;
        $res['card_issued'] = $card_issued;
        $res['card_not_issued'] = $card_not_issued;
        $res['consum'] = $consu;
        return $res;



    }


    public function card(){
        return view('admin.consumer.get-card');
    }


}
