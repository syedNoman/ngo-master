<?php

namespace App\Http\Controllers;

use App\Filter;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class FilterController extends Controller
{

    public function indexFilter()
    {
        $filters=Filter::where('plant_id', Auth::user()->plant_id)->orderBy('id', 'desc')->paginate(50);
        return view('admin.filter.index',compact('filters'));
    }



    public function StoreFilter(Request $request)
    {
        $request->validate([
            'start_date'=>'required',
            'change_date'=>'required',
            'quantity'=>'required',
        ]);

        $first_date = new DateTime($request->start_date);
        $second_date = new DateTime($request->change_date);
        $res_days= $first_date->diff($second_date)->days;
        $create=Filter::create([
            'user_id'=>Auth::user()->id,
            'plant_id'=>Auth::user()->plant_id,
            'start_date'=>$request->start_date,
            'change_date'=>$request->change_date,
            'quantity'=>$request->quantity,
            'days'=>$res_days,
        ]);
        if($create){
            return  back()->with('message','Filter Information has been Added Successfully');
        }

    }

    public function PlantFilter($id){
        $filters=Filter::where('plant_id',$id)->orderBy('id', 'desc')->paginate(10);
        return view('admin.filter.index',compact('filters'));
    }

    public function editFilter($id)
    {

        $filter=Filter::find($id);
        return view('admin.filter.edit',compact('filter'));
    }


    public function UpdateFilter(Request $request, $id)
    {

//        $tds=Filter::find($id);
//        $update=$tds->update($request->all());

        $first_date = new DateTime($request->start_date);
        $second_date = new DateTime($request->change_date);
        $res_days= $first_date->diff($second_date)->days;
        $tds=Filter::find($id);
        $update=$tds->update([
            'user_id'=>Auth::user()->id,
            'plant_id'=>Auth::user()->plant_id,
            'start_date'=>$request->start_date,
            'change_date'=>$request->change_date,
            'quantity'=>$request->quantity,
            'days'=>$res_days,
        ]);

        if ($update) {
            return back()->with('message', 'TDS has been Updated Successfully');
        }
    }

    public function DelFilter($id)
    {
        $del = Filter::findOrFail($id);
        $delt=$del->delete();
        if($delt){
            return back()->with('message-delete', ' TDS has been Deleted Successfully');
        }
    }
}
