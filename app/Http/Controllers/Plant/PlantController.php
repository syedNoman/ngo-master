<?php

namespace App\Http\Controllers\Plant;

use App\Plant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlantController extends Controller
{

    public function Plant()
    {
        $plants=Plant::paginate(5);
        return view('admin.plants.list-plants',compact('plants'));

//        Connection::
    }


    public function PlantAdd(Request $request)
    {


        $plant=new Plant();

        $plnt=$plant->insert([
            'city'=>'Khushab',
            'code'=>$request->code,
            'name'=>$request->name,
            'filter_quantity'=>$request->filter_quantity,
            'implementation_date'=>\Carbon\Carbon::now(),
            'detail'=>$request->detail,
            'village'=>$request->village,
        ]);

        if($plnt){
            return  back()->with('message','Plant has been Added Successfully');
        }

    }

    public function PlantDelete($id){
//        dd('id');
        $del = Plant::findOrFail($id);
        $delt=$del->delete();
        if($delt){
            return back()->with('message', 'Plant has been Delete  Successfully');
        }
    }

    public function PlantEdit($id){
//        dd($id);
        $plant=Plant::find($id);
        return view('admin.plants.edit-plants',compact('plant'));
    }



    public function UpdatePlant($id,Request $request){
//        dd($id);
//       dd(bcrypt($request->password));

        $user=Plant::find($id);


        $update=$user->update([
            'code'=>$request->code,
            'name'=>$request->name,
            'city'=>'Khushab',
            'filter_quantity'=>$request->filter_quantity,
            'detail'=>$request->detail,
            'village'=>$request->village,
        ]);

        if ($update) {
            return back()->with('message', 'Plant has been Update Successfully');
        }


    }
}
