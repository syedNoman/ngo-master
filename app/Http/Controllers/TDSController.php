<?php

namespace App\Http\Controllers;

use App\TDS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TDSController extends Controller
{

    public function indexTds()
    {
//        dd('ok');

        $tdas=TDS::where('plant_id', Auth::user()->plant_id)->orderBy('id', 'desc')->paginate(50);
        return view('admin.tdas.index',compact('tdas'));
    }

    public function PlantTds($id)
    {

        $tdas=TDS::where('plant_id',$id)->orderBy('id', 'desc')->paginate(50);
        return view('admin.tdas.index',compact('tdas'));
    }




    public function StoreTds(Request $request)
    {
//        dd($request->all());

        $request->validate([
            'tda'=>'required|min:2|',
            'plant_id'=>'required',
        ]);


        $create=TDS::create($request->all());
        if($create){
            return  back()->with('message','TDS has been Added Successfully');
        }
    }




    public function editTds($id)
    {

        $tds=TDS::find($id);
        return view('admin.tdas.edit',compact('tds'));
    }


    public function UpdateTds(Request $request, $id)
    {

        $tds=TDS::find($id);
        $update=$tds->update($request->all());
        if ($update) {
            return back()->with('message', 'TDS has been Updated Successfully');
        }
    }


    public function DelTds($id)
    {
        $del = TDS::findOrFail($id);
        $delt=$del->delete();
        if($delt){
            return back()->with('message-delete', ' TDS has been Deleted Successfully');
        }
    }
}
