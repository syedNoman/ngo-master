<?php

namespace App\Http\Controllers\Tehsil;

use App\Distric;
use App\Tehsil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TehsilController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function Tehsil()
    {
        $tehsils=Tehsil::paginate('10');
        $districs=Distric::all();
        return view('admin.tehsil.list-tehsil',compact('tehsils','districs'));
    }

    public function CreateTehsil(Request $request)
    {
        $create=Tehsil::create($request->all());
        if($create){
            return  back()->with('message','Tehsil has been Added Successfully');
        }
    }

    public function DelTehsil($id){
        $del = Tehsil::findOrFail($id);
        $delt=$del->delete();
        if($delt){
            return back()->with('message-delete', ' Tehsil has been Deleted Successfully');
        }
    }

    public function EditTehsil($id){
        $tehsil=Tehsil::find($id);
        $districs=Distric::all();
        return view('admin.tehsil.edit-tehsil',compact('tehsil','districs'));
    }

    public function UpdateTehsil($id,Request $request){
        $distric=Tehsil::find($id);
        $update=$distric->update($request->all());
        if ($update) {
            return back()->with('message', 'Tehsil has been Updated Successfully');
        }
    }
}
