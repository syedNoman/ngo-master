<?php

namespace App\Http\Controllers\UnionConcil;

use App\Tehsil;
use App\UnionConcil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UnionConcilController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function UnionConcil()
    {
        $concils=UnionConcil::paginate('10');
        $tehsils=Tehsil::all();
        return view('admin.union-concil.list-union-concil',compact('concils','tehsils'));
    }

    public function CreateUnionConcil(Request $request)
    {
        $create=UnionConcil::create($request->all());
        if($create){
            return  back()->with('message','Union Concil has been Added Successfully');
        }
    }

    public function DelUnionConcil($id){
        $del = UnionConcil::findOrFail($id);
        $delt=$del->delete();
        if($delt){
            return back()->with('message-delete', 'Union Concil has beed Deleted Successfully');
        }
    }

    public function EditUnionConcil($id){
        $concil=UnionConcil::find($id);
        $tehsils=Tehsil::all();
        return view('admin.union-concil.edit-union-concil',compact('concil','tehsils'));
    }

    public function UpdateUnionConcil($id,Request $request){
        $concil=UnionConcil::find($id);
        $update=$concil->update($request->all());
        if ($update) {
            return back()->with('message', 'Union Concil has been Updated Successfully');
        }
    }
}
