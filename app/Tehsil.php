<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tehsil extends Model
{
    protected $fillable=[
        'name',
        'distric_id',
    ];

    public function distric(){
        return $this->belongsTo('App\Distric');
    }
}
