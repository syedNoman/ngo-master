<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consumer extends Model
{
    protected $table="consumers";

    protected $fillable=[
        'plant_id',
        'user_id',
        'form_no',
        'thumbnail',
        'district_id',
        'card_issued',
        'card_issue_date',
        'tehsil_id',
        'union_council_id',
        'village',
        'name',
        'father_name',
        'cnic',
        'mobile',
        'address',
        'way_of_income',
        'house_member',
        'male',
        'female',
        'less_five_year_child',
        'school_child',
    ];

    public function distric(){
        return $this->belongsTo('App\Distric');
    }

    public function tehsil(){
        return $this->belongsTo('App\Tehsil');
    }


}
