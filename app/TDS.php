<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TDS extends Model
{
    protected $table='t_d_s';
    protected $fillable=[
        'plant_id',
        'tda',
    ];

    public function plant(){
        return $this->belongsTo('App\Plant');
    }
}


