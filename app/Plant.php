<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
    protected $fillable=[
        'city',
        'code',
        'name',
        'village',
        'filter_quantity',
        'implementation_date',
        'detail',

    ];


}
