<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    protected $fillable=[
        'form_no',
        'thumbnail',
        'district',
        'tehsil',
        'village',
        'union_council',
        'name',
        'name_ur',
        'father_name',
        'father_name_ur',
        'cnic',
        'mobile',
        'address',
        'way_of_income',
        'house_member',
        'male',
        'female',
        'less_five_year_child',
        'school_child',
    ];
}
