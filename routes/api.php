<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Get All Plant Api
Route::get('/get-plants', 'ApiController@getDetail');

// Get All Plant Api
Route::get('/login', 'ApiController@AdminLogin');

// Get Specific Plant Detail
Route::get('/plant-detail', 'ApiController@PlantDetail');


// Get Specific Plant Detail according to date
Route::get('/plant-report', 'ApiController@PlantReportDate');

// Get Specific Plant TDS Detail
Route::get('/plant-tds', 'ApiController@PlantTds');

// Get Specific Plant Filter Detail
Route::get('/plant-filter', 'ApiController@PlantFilter');


// Get Specific Plant Consumer Detail
Route::get('/plant-consumer', 'ApiController@PlantConsumer');


// Get Complaint Plant
Route::get('/plant-consumer', 'ApiController@PlantConsumer');

// Get Complaint Plant Consumer Detail
Route::get('/complaint', 'ApiController@complaint');

// Get Complaint Status
Route::get('/complaint-status', 'ApiController@complaintStatus');

// Add Complaint Plant
Route::post('/complaint-add', 'ApiController@complaintAdd');


// edit Complaint Plant
Route::post('/complaint-edit', 'ApiController@complaintEdit');


// check Complaint Plant
Route::get('/complaint-check', 'ApiController@complaintCheck');








