<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('migrate');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('route:clear');
    return ('done !!!');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('dashboard');
Route::get('/complaint-dashboard', 'HomeController@complaintDashboard')->name('complaint-dashboard');

Route::get('/card', 'HomeController@card')->name('card');

Route::get('/list-member', 'HomeController@ListMemeber')->name('list-member');


Route::get('/users', 'HomeController@users')->name('users');
Route::post('/users', 'HomeController@usersAdd')->name('add-user');
Route::post('/del-user/{id}', 'HomeController@usersDelete')->name('del-user');
Route::get('/user/{id}', 'HomeController@usersEdit')->name('edit-user');
Route::post('/user/{id}', 'HomeController@UpdateUser')->name('update-user');
Route::get('/water-serve', 'HomeController@WaterServe')->name('water-serve');

Route::get('/reports', 'HomeController@Reports')->name('reports');
Route::get('/report/{id}', 'HomeController@ReportsPlant')->name('report');
Route::post('/report-get', 'HomeController@ReportGet')->name('report-get');

// ajax  date filter calender
Route::post('/date-filter', 'HomeController@DateFilter')->name('date-filter');
Route::post('/year-filter', 'HomeController@YearFilter')->name('year-filter');
Route::post('/range-filter', 'HomeController@RangeFilter')->name('range-filter');


Route::get('/plants', 'Plant\PlantController@Plant')->name('plants');
Route::post('/plants', 'Plant\PlantController@PlantAdd')->name('add-plant');
Route::post('/del-plant/{id}', 'Plant\PlantController@PlantDelete')->name('del-plant');
Route::get('/plant/{id}', 'Plant\PlantController@PlantEdit')->name('edit-plant');
Route::post('/plant/{id}', 'Plant\PlantController@UpdatePlant')->name('update-plant');




Route::get('/distric', 'Distric\DistricController@Distric')->name('distric');
Route::post('/distric', 'Distric\DistricController@CreateDistric')->name('add-distric');
Route::post('/del-distric/{id}', 'Distric\DistricController@DelDistric')->name('delete-distric');
Route::get('/distric/{id}', 'Distric\DistricController@EditDistric')->name('edit-distric');
Route::post('/distric/{id}', 'Distric\DistricController@UpdateDistric')->name('update-distric');


Route::get('/tehsil', 'Tehsil\TehsilController@Tehsil')->name('tehsil');
Route::post('/tehsil', 'Tehsil\TehsilController@CreateTehsil')->name('add-tehsil');
Route::post('/del-tehsil/{id}', 'Tehsil\TehsilController@DelTehsil')->name('delete-tehsil');
Route::get('/tehsil/{id}', 'Tehsil\TehsilController@EditTehsil')->name('edit-tehsil');
Route::post('/tehsil/{id}', 'Tehsil\TehsilController@UpdateTehsil')->name('update-tehsil');


Route::get('/union-concil', 'UnionConcil\UnionConcilController@UnionConcil')->name('union-concil');
Route::post('/union-concil', 'UnionConcil\UnionConcilController@CreateUnionConcil')->name('add-union-concil');
Route::post('/del-union-concil/{id}', 'UnionConcil\UnionConcilController@DelUnionConcil')->name('delete-union-concil');
Route::get('/union-concil/{id}', 'UnionConcil\UnionConcilController@EditUnionConcil')->name('edit-union-concil');
Route::post('/union-concil/{id}', 'UnionConcil\UnionConcilController@UpdateUnionConcil')->name('update-union-concil');

Route::get('/consumer', 'Consumer\ConsumerController@Consumer')->name('consumer');
Route::get('/consumer-create', 'Consumer\ConsumerController@ConsumerCreate')->name('consumer-create');
Route::post('/consumer-save', 'Consumer\ConsumerController@ConsumerSave')->name('consumer-save');
Route::get('/consumer-edit/{id}', 'Consumer\ConsumerController@ConsumerEdit')->name('consumer-edit');
Route::get('/consumer-delete/{id}', 'Consumer\ConsumerController@ConsumerDelete')->name('consumer-del');
Route::post('/consumer-delete/{id}', 'Consumer\ConsumerController@ConsumerUpdate')->name('consumer-update');
Route::get('/consumer-show/{id}', 'Consumer\ConsumerController@ConsumerShow')->name('consumer-show');


Route::get('/tds', 'TDSController@indexTds')->name('tds.index');
Route::get('/plant-tds/{id}', 'TDSController@PlantTds')->name('plant-tds');
Route::post('/tds', 'TDSController@StoreTds')->name('add-tds');
Route::get('/tds/{id}', 'TDSController@editTds')->name('edit-tds');
Route::post('/del-tds/{id}', 'TDSController@DelTds')->name('delete-tds');
Route::post('/tds/{id}', 'TDSController@UpdateTds')->name('update-tds');



Route::get('/filter', 'FilterController@indexFilter')->name('filter.index');
Route::get('/plant-filter/{id}', 'FilterController@PlantFilter')->name('plant-filter');
Route::post('/filter', 'FilterController@StoreFilter')->name('add-filter');
Route::get('/filter/{id}', 'FilterController@editFilter')->name('edit-filter');
Route::post('/del-filter/{id}', 'FilterController@DelFilter')->name('delete-filter');
Route::post('/filter/{id}', 'FilterController@UpdateFilter')->name('update-filter');


Route::get('/excel-export', 'HomeController@ExcelExport')->name('excel-export');

Route::post('/scane', 'HomeController@ajxmethod')->name('scane');

Route::post('/scane2', 'HomeController@ajxmethod2')->name('scane2');

Route::post('/crone-update', 'HomeController@Updatecol')->name('crone-update');



Route::resource('/complaint','ComplaintController');


