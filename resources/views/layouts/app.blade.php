<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ 'PANI SAB KAY LIYE' }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
{{--<body style=" background-image: url({{URL(asset('assets/fisher.png'))}});">--}}
<body style="background-color:#85eefe">
    <div id="app">

            {{--<div style="height:auto;" class="col-md-12">--}}

                {{--<div class="row">--}}
                    {{--<div class="col-md-2 col-xs-12">--}}
                        {{--<img style="  border-radius: 43px; height: 202px; padding: 24px;" src="{{asset('assets/images/panisab.jpg')}}">--}}
                    {{--</div>--}}
                    {{--<div class="col-md-7 col-xs-12">--}}
                        {{--<p style="margin-top: 37px">--}}
                        {{--<h1>PANI SAB KAY LIYE</h1>--}}
                        {{--<h4>A Project Of Rasheeda Trust</h4>--}}
                        {{--</p>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-3 col-xs-12">--}}
                        {{--<img style="float: right;--}}
											{{--height: 102px;--}}
											{{--padding: 20px;--}}
											{{--margin-top: 41px;" src="{{asset('assets/images/rasheedalogo.png')}}">--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}


        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
