@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div style="margin-top:150px"  class="col-md-6">
            <div style="
               padding: 10px;
    border-radius: 15px;box-shadow: 0px 0px 21px 3px #1c88b3;" class="card">
                <div style="padding: .75rem 1.25rem;
    text-align: center;
    margin-bottom: 0;
    font-size: 62px;
    font-family: unset;">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-4 col-xs-12">
                            <img style=" border-radius: 50%;
                            height: 120px;
                            padding: 24px;
                            box-shadow: 1px 0px 12px 0px #ccc;" src="{{asset('assets/images/panisab.jpg')}}">
                            {{--<img style="float: right; border-radius: 50%;--}}
                            {{--height: 120px;--}}
                            {{--padding: 24px;--}}
                            {{--box-shadow: 1px 0px 12px 0px #ccc;" src="{{asset('assets/images/rasheedalogo.png')}}">--}}
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <form id="form" method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input style="padding:18px;
    font-size: 18px;
    /*color: #cccccc !important;*/
    height: 65px;
    border-radius: 18px;
    box-shadow: rgb(204, 204, 204) 0px 0px 8px 0px;
" placeholder="Email Address" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div style="margin-top: 15px"  class="col-md-12 col-sm-12 col-xs-12">
                                <input style="padding: 18px;
    font-size: 18px;
    /*color: #cccccc !important;*/
    height: 65px;
    border-radius: 18px;
    box-shadow: rgb(204, 204, 204) 0px 0px 8px 0px;
" id="password" placeholder="Password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-check">
                                    <input style="height: 17px;" class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label style="font-size: 17px;" class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div style="margin: auto" class="col-md-6 col-sm-6 col-xs-6">
                                <a style="width: 100%;
    background-color: rgb(0, 201, 128);
    border-color: rgb(0, 201, 128);
    height: 35px;
    border-radius: 40px;
    font-size: 18px;
    padding-top: 2px;
color: white" class="loginbtn btn btn-primary">
                                    Login
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $('.loginbtn').click(function(){
            $('#form').submit();
            this.innerHTML='<i class="fa fa-spinner fa-spin"></i> Please wait…';
            this.disabled=true;
        });
    });

</script>

@endsection
