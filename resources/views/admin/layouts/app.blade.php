<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.layouts.header-script')
</head>
<style>

    .main-container:before {
        background-image: url({{asset('background.png')}});
    }
    .main-container:after {
        background-image: url({{asset('background.png')}});
    }
</style>
<body style="font-family: 'Lato', sans-serif;" class="no-skin">
@include('admin.layouts.header')

<div class="main-container ace-save-state" id="maincontainer">
    <script type="text/javascript">
        try{ace.settings.loadState('main-container')}catch(e){}
    </script>

    <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <script type="text/javascript">
            try{ace.settings.loadState('sidebar')}catch(e){}
        </script>

        <div class="sidebar-shortcuts" id="sidebar-shortcuts">


            <i><img style="height: 40px;" src="{{asset('cleanwateraid_logo.png')}}"></i>
        </div><!-- /.sidebar-shortcuts -->

        @include('admin.layouts.lift-manu-bar')<!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>

    <!-- /.main-content -->
    @yield('content')

        <!-- Footer Area Start -->
        {{--@include('admin.layouts.footer')--}}
        <!-- Footer Area End -->

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>

    @include('admin.layouts.footer-script')
    @yield('script')

</div><!-- /.main-container -->

</body>
</html>
