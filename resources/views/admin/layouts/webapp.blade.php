<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.layouts.header-script')
</head>

<body class="no-skin">

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try{ace.settings.loadState('main-container')}catch(e){}
    </script>



    <!-- /.main-content -->
    @yield('content')

        <!-- Footer Area Start -->
        {{--@include('admin.layouts.footer')--}}
        <!-- Footer Area End -->

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>

    @include('admin.layouts.footer-script')
    @yield('script')

</div><!-- /.main-container -->

</body>
</html>
