<ul class="nav nav-list">

    @if(Auth::user()->role==2)
        <li class="@if(request()->url() == route('dashboard')){{'active'}}@endif">
            <a href="{{route('complaint-dashboard')}}">
                <i><img style="height: 20px;
    margin-top: -5px;
    margin-left: 10px;
    margin-right: 3px;" src="{{asset('assets/dash-icon/dashboard.png')}}"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
        </li>
    @else

    <li class="@if(request()->url() == route('dashboard')){{'active'}}@endif">
        <a href="{{route('dashboard')}}">
            <i><img style="height: 20px;
    margin-top: -5px;
    margin-left: 10px;
    margin-right: 3px;" src="{{asset('assets/dash-icon/dashboard.png')}}"></i>
            <span class="menu-text"> Dashboard </span>
        </a>

        <b class="arrow"></b>
    </li>
    @endif
    @if(Auth::user()->role==1 or Auth::user()->role==0)
    <li class="@if(request()->url() == route('water-serve')){{'active'}}@endif">
        <a href="{{route('water-serve')}}">
            {{--<i class="menu-icon fa fa-tachometer"></i>--}}
            <i><img style="height: 20px;
    margin-top: -5px;
    margin-left: 10px;
    margin-right: 3px;" src="{{asset('assets/dash-icon/water-serve.png')}}"></i>
            <span class="menu-text"> Water Serve </span>
        </a>

        <b class="arrow"></b>
    </li>
    @endif
        @if(Auth::user()->role==1 or Auth::user()->role==0)
    <li class="@if(request()->url() == route('consumer')){{'active open'}} @elseif(request()->url() == route('consumer-create')){{'active open'}}@endif">
        <a href="#" class="dropdown-toggle">
            {{--<i class="menu-icon fa fa-desktop"></i>--}}
            <i><img style="height: 20px;
    margin-top: -5px;
    margin-left: 10px;
    margin-right: 3px;" src="{{asset('assets/dash-icon/consumers.png')}}"></i>
            <span class="menu-text">
								Consumers
							</span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(request()->url() == route('consumer-create')){{'active open'}}@endif">
                <a href="{{route('consumer-create')}}">
                    <i class="menu-icon fa fa-plus purple"></i>
                    Add Consumers
                </a>

                <b class="arrow"></b>
            </li>

            <li class="@if(request()->url() == route('consumer')){{'active open'}}@endif">
                <a href="{{route('consumer')}}">
                    <i class="menu-icon fa fa-eye pink"></i>
                    Show Consumers
                </a>

                <b class="arrow"></b>
            </li>

        </ul>
    </li>
    @endif

    @if(Auth::user()->role==2 or Auth::user()->role==1)
    <li class="@if(request()->url() == route('complaint.index')){{'active open'}} @elseif(request()->url() == route('complaint.create')){{'active open'}}@endif">
        <a href="#" class="dropdown-toggle">
            {{--<i class="menu-icon fa fa-desktop"></i>--}}
            <i><img style="height: 20px;
    margin-top: -5px;
    margin-left: 10px;
    margin-right: 3px;" src="{{asset('assets/pskl_icons/consumers.png')}}"></i>
            <span class="menu-text">
								complaint
							</span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            @if(Auth::user()->role==2 )
            <li class="@if(request()->url() == route('complaint.create')){{'active open'}}@endif">
                <a href="{{route('complaint.create')}}">
                    <i class="menu-icon fa fa-plus purple"></i>
                    Add complaint
                </a>

                <b class="arrow"></b>
            </li>
            @endif

            <li class="@if(request()->url() == route('complaint.index')){{'active open'}}@endif">
                <a href="{{route('complaint.index')}}">
                    <i class="menu-icon fa fa-eye pink"></i>
                    Show complaint
                </a>

                <b class="arrow"></b>
            </li>

        </ul>
    </li>
    @endif




    @if(Auth::user()->role==1 or Auth::user()->role==0)

    <li class="@if(request()->url() == route('distric')){{'active open'}}@endif">
        <a href="#" class="dropdown-toggle">
            <i><img style="height: 20px;
    margin-top: -5px;
    margin-left: 10px;
    margin-right: 3px;" src="{{asset('assets/dash-icon/district.png')}}"></i>
            <span class="menu-text">
								Distric
							</span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(request()->url() == route('distric')){{'active open'}}@endif">
                <a href="{{route('distric')}}">
                    <i class="menu-icon fa fa-eye pink"></i>
                    Show Distric
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>
    <li class="@if(request()->url() == route('tehsil')){{'active open'}}@endif">
        <a href="#" class="dropdown-toggle">
            <i><img style="height: 20px;
    margin-top: -5px;
    margin-left: 10px;
    margin-right: 3px;" src="{{asset('assets/dash-icon/tehsil.png')}}"></i>
            <span class="menu-text">
								Tehsil
							</span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(request()->url() == route('tehsil')){{'active open'}}@endif">
                <a href="{{route('tehsil')}}">
                    <i class="menu-icon fa fa-eye pink"></i>
                    Show Tehsil
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>
    <li class="@if(request()->url() == route('union-concil')){{'active open'}}@endif">
        <a href="#" class="dropdown-toggle">
            <i><img style="height: 20px;
    margin-top: -5px;
    margin-left: 10px;
    margin-right: 3px;" src="{{asset('assets/pskl_icons/union-counsil.png')}}"></i>
            <span class="menu-text">
								Union-Concil
							</span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(request()->url() == route('union-concil')){{'active open'}}@endif">
                <a href="{{route('union-concil')}}">
                    <i class="menu-icon fa fa-eye pink"></i>
                    Show Union-Concil
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>

    @endif

    @if(Auth::user()->role==1)
    <li class="@if(request()->url() == route('users')){{'active open'}}@endif">
        <a href="#" class="dropdown-toggle">
            <i><img style="height: 20px;
    margin-top: -5px;
    margin-left: 10px;
    margin-right: 3px;" src="{{asset('assets/dash-icon/consumers.png')}}"></i>
            <span class="menu-text">
								Maintainer
							</span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(request()->url() == route('users')){{'active open'}}@endif">
                <a href="{{route('users')}}">
                    <i class="menu-icon fa fa-eye pink"></i>
                    Show All User
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>
        @endif

    @if(Auth::user()->role==1)
    <li class="@if(request()->url() == route('plants')){{'active open'}}@endif">
        <a href="#" class="dropdown-toggle">
            <i><img style="height: 20px;
    margin-top: -5px;
    margin-left: 10px;
    margin-right: 3px;" src="{{asset('assets/pskl_icons/plants.png')}}"></i>
            <span class="menu-text">
								Plant
							</span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(request()->url() == route('plants')){{'active open'}}@endif">
                <a href="{{route('plants')}}">
                    <i class="menu-icon fa fa-eye pink"></i>
                    Show All Plants
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>
        @endif
    @if(Auth::user()->role==1 or Auth::user()->role==0)
    <li class="@if(request()->url() == route('tds.index')){{'active open'}}@endif">
        <a href="#" class="dropdown-toggle">
            <i><img style="height: 20px;
    margin-top: -5px;
    margin-left: 10px;
    margin-right: 3px;" src="{{asset('assets/dash-icon/tds-tracking.png')}}"></i>
            <span class="menu-text">
								TDS Tracking
							</span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(request()->url() == route('tds.index')){{'active open'}}@endif">
                <a href="{{route('tds.index')}}">
                    <i class="menu-icon fa fa-eye pink"></i>
                    TDS Tracking Show
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>

    <li class="@if(request()->url() == route('filter.index')){{'active open'}}@endif">
        <a href="#" class="dropdown-toggle">
            <i><img style="height: 20px;
    margin-top: -5px;
    margin-left: 10px;
    margin-right: 3px;" src="{{asset('assets/dash-icon/filter-change.png')}}"></i>
            <span class="menu-text">
								Filter
							</span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(request()->url() == route('filter.index')){{'active open'}}@endif">
                <a href="{{route('filter.index')}}">
                    <i class="menu-icon fa fa-eye pink"></i>
                    Filter change History
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>
    @endif

    @if(Auth::user()->role==1)
    <li class="@if(request()->url() == route('reports')){{'active open'}}@endif">
        <a href="#" class="dropdown-toggle">
            <i><img style="height: 20px;
    margin-top: -5px;
    margin-left: 10px;
    margin-right: 3px;" src="{{asset('assets/pskl_icons/report.png')}}"></i>
            <span class="menu-text">
								Reports
							</span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="@if(request()->url() == route('reports')){{'active open'}}@endif">
                <a href="{{route('reports')}}">
                    <i class="menu-icon fa fa-eye pink"></i>
                    Show  Reports
                </a>

                <b class="arrow"></b>
            </li>
        </ul>
    </li>
        @endif

</ul><!-- /.nav-list -->