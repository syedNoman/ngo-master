<div class="sidebar" data-color="purple" data-background-color="white"
     data-image="{{asset('sidebar-bg.jpg')}}">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div style="padding: 0px 0px; !important;" class="logo">
        <div class="col-md-12">
            <div>
                <img class="img-circle" style="height: 202px;padding: 41px;" src="{{asset('pskllogo.png')}}">
            </div>
        </div>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            @if(Auth::user()->role==1 or Auth::user()->role==0)
                <li class="nav-item @if(request()->url() == route('dashboard')){{'active'}}@endif">
                    <a class="nav-link" href="{{route('dashboard')}}">
                        {{--<i class="material-icons">dashboard</i>--}}
                        @if(request()->url() == route('dashboard'))
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/dashboard-active.png')}}"></i>
                            @else
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/dashboard.png')}}"></i>
                        @endif

                        <p>Dashboard</p>
                    </a>
                </li>
                @if(Auth::user()->role==0)
                <li class="nav-item @if(request()->url() == route('water-serve')){{'active'}}@endif">
                    <a class="nav-link" href="{{route('water-serve')}}">
                        @if(request()->url() == route('water-serve'))
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/server-water-active.png')}}"></i>
                        @else
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/water-serve-icon.png')}}"></i>
                        @endif
                        <p>Water Serve</p>
                    </a>
                </li>
                @endif

                @if(Auth::user()->role==1)
                    <li class="nav-item @if(request()->url() == route('plants')){{'active open'}}@endif">
                        <a class="nav-link" href="{{route('plants')}}">
                            @if(request()->url() == route('plants'))
                                <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/plants-active.png')}}"></i>
                            @else
                                <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/plants.png')}}"></i>
                            @endif
                            <p>Plants</p>
                        </a>
                    </li>
                    <li class="nav-item @if(request()->url() == route('users')){{'active open'}}@endif">
                        <a class="nav-link" href="{{route('users')}}">
                            @if(request()->url() == route('users'))
                                <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/maintiners-active.png')}}"></i>
                            @else
                                <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/maintianers.png')}}"></i>
                            @endif
                            <p>Maintainers</p>
                        </a>
                    </li>
                @endif

                <li class="nav-item @if(request()->url() == route('consumer')){{'active open'}} @elseif(request()->url() == route('consumer-create')){{'active open'}}@endif">
                    <a class="nav-link" href="{{route('consumer')}}">
                        @if(request()->url() == route('consumer'))
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/all-consumer-active.png')}}"></i>
                        @else
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/all consumers.png')}}"></i>
                        @endif
                        <p>Consumers</p>
                    </a>
                </li>
            @endif

            @if(Auth::user()->role==2 or Auth::user()->role==1)
                @if(Auth::user()->role==2)
                    <li class="nav-item @if(request()->url() == route('complaint-dashboard')){{'active'}}@endif">
                        <a class="nav-link" href="{{route('complaint-dashboard')}}">
                            @if(request()->url() == route('complaint-dashboard'))
                                <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/dashboard-active.png')}}"></i>
                            @else
                                <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/dashboard.png')}}"></i>
                            @endif
                            <p>Dashboard</p>
                        </a>
                    </li>
                @endif
                <li class="nav-item @if(request()->url() == route('complaint.index')){{'active open'}} @elseif(request()->url() == route('complaint.create')){{'active open'}}@endif">
                    <a class="nav-link" href="{{route('complaint.index')}}">
                        @if(request()->url() == route('complaint.index'))
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/complaints-active.png')}}"></i>
                        @else
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/complaints.png')}}"></i>
                        @endif
                        <p>Complaints</p>
                    </a>
                </li>
            @endif

            @if(Auth::user()->role==1 or Auth::user()->role==0)

                    <li class="nav-item @if(request()->url() == route('distric')){{'active open'}}@endif">
                    <a class="nav-link" href="{{route('distric')}}">
                        @if(request()->url() == route('distric'))
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/tehsil-active.png')}}"></i>
                        @else
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/district-tehsil-union-council.png')}}"></i>
                        @endif
                        <p>Districts</p>
                    </a>
                </li>
                    <li class="nav-item @if(request()->url() == route('tehsil')){{'active open'}}@endif">
                    <a class="nav-link" href="{{route('tehsil')}}">
                        @if(request()->url() == route('tehsil'))
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/tehsil-active.png')}}"></i>
                        @else
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/district-tehsil-union-council.png')}}"></i>
                        @endif
                        <p>Tehsils</p>
                    </a>
                </li>
                    <li class="nav-item @if(request()->url() == route('union-concil')){{'active open'}}@endif">
                    <a class="nav-link" href="{{route('union-concil')}}">
                        @if(request()->url() == route('union-concil'))
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/tehsil-active.png')}}"></i>
                        @else
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/district-tehsil-union-council.png')}}"></i>
                        @endif
                        <p>Union Councils</p>
                    </a>
                </li>

                    <li class="nav-item @if(request()->url() == route('tds.index')){{'active open'}}@endif">
                    <a class="nav-link" href="{{route('tds.index')}}">
                        @if(request()->url() == route('tds.index'))
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/tds-active.png')}}"></i>
                        @else
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/tds tracker.png')}}"></i>
                        @endif
                        <p>TDS Tracking</p>
                    </a>
                </li>

                    <li class="nav-item @if(request()->url() == route('filter.index')){{'active open'}}@endif">
                    <a class="nav-link" href="{{route('filter.index')}}">
                        @if(request()->url() == route('filter.index'))
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/filter-change-active.png')}}"></i>
                        @else
                            <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/filter change hjistory.png')}}"></i>
                        @endif
                        <p>Filter Change History</p>
                    </a>
                </li>

            @endif

                @if(Auth::user()->role==1)
                    <li class="nav-item @if(request()->url() == route('reports')){{'active open'}}@endif">
                        <a class="nav-link" href="{{route('reports')}}">
                            @if(request()->url() == route('reports'))
                                <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/reports-active.png')}}"></i>
                            @else
                                <i class="material-icons"><img style="margin-top:-12px" src="{{asset('web-assets/images/reports.png')}}"></i>
                            @endif
                            <p>Reports</p>
                        </a>
                    </li>
                @endif

                    <li>
                        <a style="background:transparent !important;">

                        </a>
                    </li>

                    <li >
                        <a style="background:transparent !important;" >

                        </a>
                    </li>

                    <li >
                        <a  style="background:transparent !important;">

                        </a>
                    </li>

                    <li>
                        <a  style="background:transparent !important;">

                        </a>
                    </li>
                    <li>
                        <a style="background:transparent !important;" >

                        </a>
                    </li>
                    <li>
                        <a style="background:transparent !important;" >

                        </a>
                    </li>



        </ul>
    </div>
</div>