<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<title>PANI SAB KAY LIYE</title>

<!-- CSRF Token -->
{{--<meta nadadadcme="csrf-token" content="{{ csrf_token() }}">--}}
<meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="description" content="Common form elements and layouts" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />



<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">

<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}" />
<link rel="stylesheet" href="{{asset('assets/font-awesome/4.5.0/css/font-awesome.min.css')}}" />

<!-- page specific plugin styles -->
<link rel="stylesheet" href="{{asset('assets/css/jquery-ui.custom.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/chosen.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-timepicker.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/daterangepicker.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datetimepicker.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-colorpicker.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/summernote/summernote.css')}}" />

<!-- text fonts -->
<link rel="stylesheet" href="{{asset('assets/css/fonts.googleapis.com.css')}}" />

<!-- ace styles -->
<link rel="stylesheet" href="{{asset('assets/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />

<!--[if lte IE 9]>
<link rel="stylesheet" href="{{asset('assets/css/ace-part2.min.css')}}" class="ace-main-stylesheet" />
<![endif]-->
<link rel="stylesheet" href="{{asset('assets/css/ace-skins.min.css')}}" />
<link rel="stylesheet" href="{{asset('assets/css/ace-rtl.min.css')}}" />

<!--[if lte IE 9]>
<link rel="stylesheet" href="{{asset('assets/css/ace-ie.min.css')}}" />

<![endif]-->

<!-- inline styles related to this page -->

<!-- ace settings handler -->
<script src="{{asset('assets/js/ace-extra.min.js')}}"></script>

<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

<!--[if lte IE 8]>
<script src="{{asset('assets/js/html5shiv.min.js')}}"></script>
<script src="{{asset('assets/js/respond.min.js')}}"></script>
<![endif]-->

@yield('style')

