<form id="editfrom" method="POST" action="/tehsil/{{ $tehsil->id }}" enctype="multipart/form-data">
    <div class="modal-body">
        {{ csrf_field() }}

        <div style="margin-top: 40px" class="form-group">
            {{--<label for="exampleInputName"><h4>Tehsil Name</h4></label>--}}
            <input style="text-transform:capitalize;" class="form-control" id="nameid" value="{{ $tehsil->name }}" name="name"
                   type="text" placeholder="Enter Name"
                   required>
        </div>

        <div style="margin-top: 30px" class="form-group">
            {{--<label for="exampleInputEmail1"><h4>Select Distric</h4></label>--}}

            <select style="text-transform:capitalize" required  class="form-control" name="distric_id" >
                <option value="" hidden>Select distric name</option>
                @foreach($districs as $distric)
                    <option style="text-transform:capitalize" @if($tehsil->distric_id == $distric->id) selected @endif value="{{$distric->id}}">{{$distric->name}}</option>
                @endforeach
            </select>
        </div>

    </div>
    <div style="border: none" class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
</form>
