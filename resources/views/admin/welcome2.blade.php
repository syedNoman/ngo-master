@extends('admin.layouts.app')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="{{route('complaint-dashboard')}}">Home</a>
                    </li>


                    <li>
                        <a href="{{route('complaint-dashboard')}}">dashboard</a>
                    </li>
                </ul><!-- /.breadcrumb -->


            </div>
            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    {{--<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">--}}
                        {{--<i class="ace-icon fa fa-cog bigger-130"></i>--}}
                    {{--</div>--}}

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->
                <div class="page-header">
                    {{--onchange="location = this.value;"--}}





                    <div class="row">
                        <div class="col-md-3">
                            <h1>
                                Welcome to Report Dashboard
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    {{--Static &amp; Dynamic Tables--}}
                                </small>
                            </h1>
                        </div>

                    </div>

                </div><!-- /.page-header -->
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div id="page-wrapper">

                            <!-- /.row -->
                            <div class="row">







                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-danger">
                                        <div style="height: 250px;" class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i><img style="height: 70px;" src="{{asset('assets/dash_icons/cards.png')}}" alt=""></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                        <div>Pending</div>
                                                    @if(Auth::user()->role == 2)
                                                        <div>{{\App\Complaint::where('status',0)->count()}}</div>
                                                    @endif

                                                    </div>
                                            </div>
                                            @if(Auth::user()->role == 2)
                                                <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Complaint::where('status',0)->count()}}</div>
                                            @endif

                                            <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: 'Lato', sans-serif;">Pending</div>


                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-warning">
                                        <div style="color: #AB6D0A;
                                                background-color: wheat;
                                                border-color: #faebcc; height: 250px;" class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i><img style="height: 70px;" src="{{asset('assets/dash_icons/consumer.png')}}" alt=""></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    <div>Processing</div>
                                                    @if(Auth::user()->role == 2)
                                                        <div>{{\App\Complaint::where('status',1)->count()}}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            @if(Auth::user()->role == 2)
                                                <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Complaint::where('status',1)->count()}}</div>
                                          @endif

                                            <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: 'Lato', sans-serif;">Processing</div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-success">
                                        <div style="height: 250px;" class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i><img style="height: 70px;" src="{{asset('assets/dash_icons/cards.png')}}" alt=""></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    <div>Complete</div>
                                                    @if(Auth::user()->role == 2)
                                                        <div>{{\App\Complaint::where('status',2)->count()}}</div>
                                                    @endif

                                                </div>
                                            </div>
                                            @if(Auth::user()->role == 2)
                                                <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Complaint::where('status',2)->count()}}</div>
                                            @endif

                                            <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: 'Lato', sans-serif;">Complete</div>


                                        </div>
                                    </div>
                                </div>



                            </div>


                            <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div>

        </div>

    </div>
@endsection

@section('script')



    <script>
        //        $('input[name="dates"]').daterangepicker();

        $('#myDatepicker').datepicker({
            format: "yyyy/mm",
            startView: "year",
            minViewMode: "months"
        });
        $('.filter_by_month').change(function (){
            var abc=$('.filter_by_month').val();
            var url = document.URL;
            var stuff = url.split('/');
            var id = stuff[stuff.length-1];
//            alert(id);
            if(abc){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                $.ajax({
                    url:'/year-filter?id='+id,
                    type:'POST',
                    data:{data:abc},
                    dataType:'JSON',


                    success:function (Result) {
                        $('#member_register').html(Result.member_registerd);
                        $('#user_card_issue').html(Result.card_issued);
                        $('#no_card_issue').html(Result.card_not_issued);
                        $('#consum').html(Result.consum);
                    },
                    error:function (Result) {

                    }
                });
            }


        });

        $('.filter_by_date').change(function (){
            var abc=$('.filter_by_date').val();
            var url = document.URL;
            var stuff = url.split('/');
            var id = stuff[stuff.length-1];
//            alert(id);
            if(abc){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                $.ajax({
                    url:'/date-filter?id='+id,
                    type:'POST',
                    data:{data:abc},
                    dataType:'JSON',


                    success:function (Result) {
                        $('#member_register').html(Result.member_registerd);
                        $('#user_card_issue').html(Result.card_issued);
                        $('#no_card_issue').html(Result.card_not_issued);
                        $('#consum').html(Result.consum);
                    },
                    error:function (Result) {

                    }
                });
            }


        });

        $('.filter_by_date_range2').change(function (){
            var start=$('.filter_by_date_range1').val();
            var end=$('.filter_by_date_range2').val();
//             alert('ok');
            var url = document.URL;
            var stuff = url.split('/');
            var id = stuff[stuff.length-1];
//            alert(id);
            if(start && end){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                $.ajax({
                    url:'/range-filter?id='+id,
                    type:'POST',
                    data:{data:start,end},
                    dataType:'JSON',


                    success:function (Result) {
                        $('#member_register').html(Result.member_registerd);
                        $('#user_card_issue').html(Result.card_issued);
                        $('#no_card_issue').html(Result.card_not_issued);
                        $('#consum').html(Result.consum);
                    },
                    error:function (Result) {

                    }
                });
            }


        });
    </script>




@endsection