<form id="editfrom" method="POST" action="/union-concil/{{$concil->id}}" enctype="multipart/form-data">
    <div class="modal-body">
        {{ csrf_field() }}

        <div style="margin-top: 40px" class="form-group">
            {{--<label for="exampleInputName"><h4>Tehsil Name</h4></label>--}}
            <input style="text-transform:capitalize;" class="form-control" id="nameid" value="{{ $concil->name }}" name="name"
                   type="text" placeholder="Enter Name"
                   required>
        </div>

        <div style="margin-top: 30px" class="form-group">
            {{--<label for="exampleInputEmail1"><h4>Select Tehsil</h4></label>--}}
            <select style="text-transform:capitalize" required  class="form-control" name="tehsil_id">
                @foreach($tehsils as $tehsil)
                    <option style="text-transform:capitalize" @if($concil->tehsil_id == $tehsil->id) selected @endif value="{{$tehsil->id}}">{{$tehsil->name}}</option>
                @endforeach
            </select>
        </div>

    </div>
    <div style="border: none" class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
</form>
