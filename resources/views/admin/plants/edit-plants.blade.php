<form id="editfrom" method="POST" action="/plant/{{ $plant->id }}" enctype="multipart/form-data">
    <div class="modal-body">
        {{ csrf_field() }}

        <div style="margin-top: 30px" class="form-group">
            <input  class="form-control" id="name" name="city" value="Khushab"
                    type="text" placeholder="Enter City Name"
                    required>
        </div>

        <div style="margin-top: 30px" class="form-group">
            {{--<lable for="exampleInputCat" class="control-label"><h4> Code</h4></lable>--}}
            <input style="text-transform:uppercase;" class="form-control" id="code" name="code" type="text" placeholder="Enter City Code" required value="{{@$plant->code}}">
        </div>

        <div style="margin-top: 30px" class="form-group">
            <input  class="form-control" id="name" name="name" type="text" placeholder="Enter Plant Name" required value="{{@$plant->name}}">
        </div>

        <div style="margin-top: 30px" class="form-group">

            <input id="filter_quantity" type="text" class="form-control" placeholder="Enter village Name" name="village" required value="{{@$plant->village}}">
        </div>

        <div style="margin-top: 30px" class="form-group">

            <input id="filter_quantity" type="number" class="form-control" placeholder="Enter Filter Quantity" name="filter_quantity" required value="{{@$plant->filter_quantity}}">
        </div>

        <div style="margin-top:30px" class="form-group">
            <textarea rows="4" id="filter_quantity" placeholder="Enter Plant detail" type="text" class="form-control" name="detail" required>{!! @$plant->detail !!}</textarea>
        </div>



    </div>
    <div style="border: none" class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
</form>
