<form id="editfrom" method="POST" action="{{ route('update-filter',$filter->id)}}" enctype="multipart/form-data">
    <div class="modal-body">

        @csrf
        <input hidden name="plant_id" value="{{Auth::user()->plant_id}}">
        <input hidden name="plant_id" value="{{Auth::user()->id}}">

        <div class="form-group">
            <lable for="exampleInputCat" class="control-label">
                <h4>Quantity</h4>
            </lable>
            <select  class="form-control" id="tda" name="quantity" required>
                <option value="" hidden>Select Quantity Filter </option>
                <option @if($filter->quantity == 1 ) selected @endif value="1" >1</option>
                <option @if($filter->quantity == 2 ) selected @endif value="2" >2</option>
                <option @if($filter->quantity == 3 ) selected @endif value="3" >3</option>
                <option @if($filter->quantity == 4 ) selected @endif value="4" >4</option>
            </select>
        </div>
        <div class="form-group">
            <lable for="exampleInputCat" class="control-label"></lable>
            <h4>Start Date</h4>
            <div class="input-group">
                {{--<input  placeholder="Select Date" class="form-control date-picker filter_by_date" id="id-date-picker-1" name="start_date" value="{{$filter->start_date}}" type="text" data-date-format="yyyy-mm-dd" required   />--}}
                {{--<span class="input-group-addon">--}}
																		{{--<i class="fa fa-calendar bigger-110"></i>--}}
																	{{--</span>--}}
                <input type="date" name="start_date" max="1-1-2001" min="1-1-2019" class="form-control" value="{{$filter->start_date}}" >
            </div>
        </div>

        <div class="form-group">
            <lable for="exampleInputCat" class="control-label">
                <h4>Change Date</h4>
            </lable>
            <div class="input-group">
                {{--<input  placeholder="Select Date" class="form-control date-picker filter_by_date" id="id-date-picker-1" name="change_date" value="{{$filter->change_date}}" type="text" data-date-format="yyyy-mm-dd"  required  />--}}
                {{--<span class="input-group-addon">--}}
																		{{--<i class="fa fa-calendar bigger-110"></i>--}}
																	{{--</span>--}}
                <input type="date" name="change_date" max="1-1-2001" min="1-1-2019" class="form-control" value="{{$filter->change_date}}">
            </div>
        </div>


    </div>
    <div style="border: none" class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger">Update</button>
    </div>
</form>
