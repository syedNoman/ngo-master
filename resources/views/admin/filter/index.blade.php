@extends('admin.layouts.app2')
@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Filter Change History</h4>

                            {{--<a style="background: linear-gradient(145deg,#ffffff,#ffffff);--}}
                            {{--color: #07cdff;--}}
                            {{--margin-top: -22px;" class="btn pull-right" href="{{route('tds.create')}}">Add TDS</a>--}}
                            @if(Auth::user()->role==0)
                                <a style="cursor: pointer; background: linear-gradient(145deg,#ffffff,#ffffff);
                                color: #07cdff;
                                margin-top: -22px;" class="btn-info btn-sm pull-right b" data-toggle="modal" data-target="#exampleModal">Add
                                    Filter Change History
                                </a>
                            @endif

                            @if(Auth::user()->role==1)
                                {{--<div class="col-md-4 offset-4">--}}
                                <select style="
                                            margin-top: -17px;
                                            color: #438EB9;
                                            font-size: 20px;
                                            margin-bottom: 10px;
                                            font-family: inherit;
                                            height: 38px;
                                            border: 1px solid #6FB3E0;
                                            /* border-radius: 15px; */
                                            padding: 0px 17px;
                                            background: white;"
                                        class="col-md-4 offset-4 form-control" name="forma" onchange="window.location.href = '/plant-filter/'+value" >
                                    <option hidden value="" href="">Select Plant</option>
                                    @foreach(\App\Plant::all() as $plant)
                                        <option  value="{{(@$plant->id)}}" href="" @if(request()->url() == url('plant-filter/'.$plant->id))selected @endif>{{@$plant->name}}</option>
                                    @endforeach
                                </select>

                                {{--</div>--}}
                            @endif

                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    @if(Session::has('message'))
                                        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em></div>
                                    @endif
                                    @if(Session::has('message-delete'))
                                        @if(Session::has('message-delete'))
                                            <div class="alert alert-danger"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message-delete') !!}</em></div>
                                        @endif
                                    @endif
                                    <thead class="text-primary">
                                    <tr>
                                        <th  class="detail-col">NO#</th>
                                        <th style="text-align:center" >Name</th>
                                        <th style="text-align:center" >Plant Name</th>
                                        <th style="text-align:center" >Start Date</th>
                                        <th style="text-align:center" >Change Date</th>
                                        <th style="text-align:center" >Days</th>
                                        <th style="text-align:center" >quantity</th>
                                        <th style="text-align:center">Action</th>
                                    </tr>
                                    </thead>
                                        <tbody>
                                        @foreach($filters as $key => $filter)

                                            <tr>
                                                <td style="text-align: center">
                                                    {{ ($filters->currentpage() - 1) * $filters->perpage() + 1 + $key }}

                                                </td>
                                                <td style="text-transform: capitalize">{{ $filter->user->name }}</td>
                                                <td style="text-transform: capitalize">{{@$filter->plant->name}}</td>
                                                <td style="text-transform: capitalize">{{$filter->start_date }}</td>
                                                <td style="text-transform: capitalize">{{$filter->change_date }}</td>
                                                <td style="text-transform: capitalize">{{$filter->days }}</td>
                                                <td style="text-transform: capitalize">{{$filter->quantity }}</td>
                                                {{--<td style="text-transform: capitalize">{{ @$filter->plants->name }}</td>--}}
                                                <td style="text-align: center">
                                                    <div class="hidden-sm hidden-xs btn-group">
                                                        <button title="Update" class="btn btn-xs btn-success edit-btn"
                                                                data-toggle="modal"
                                                                data-action="{{route('edit-filter',['id'=>$filter->id])}}"
                                                                data-target="#exampleModal2">
                                                            <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                        </button>
                                                        @if(Auth::user()->role==1)
                                                            <button title="Delete" type="button" data-toggle="modal"
                                                                    data-target="#delete-tehsil"
                                                                    data-action="{{route('delete-filter',['id'=>$filter->id])}}"
                                                                    class="show-delete-modal btn btn-xs btn-danger delete-btn ">
                                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                            </button>
                                                        @endif

                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                        @endforeach

                                </table>
                                <div class="text-left">{{ $filters->links() }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Add Module -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div style="background: linear-gradient(145deg, #07cdff, #46e3ff);" class="modal-header">

                    <u style="color:#fff"><h1 style="font-size: 39px;
    											color: #fff;">Add Filter</h1></u>
                    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                </div>
                <form id="form" method="POST" action="{{route('add-filter')}}"
                      enctype="multipart/form-data">
                    <div class="modal-body">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <lable for="exampleInputCat" class="control-label">
                                <h4>Quantity</h4>
                            </lable>
                            <select  class="form-control" id="tda" name="quantity" required>
                                <option value="" hidden>Select Quantity Filter </option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <lable for="exampleInputCat" class="control-label"></lable>
                            <h4>Start Date</h4>
                            <div class="input-group">
                                {{--<input  placeholder="Select Date"  class="form-control datetime date-picker filter_by_date" id="id-date-picker-1" name="start_date" value="" type="datetime" data-date-format="yyyy-mm-dd" required   />--}}

                                    {{--<input type="text" class=" datepicker form-control" name="" value="10/06/2019">--}}
                                <input type="date" name="start_date" max="1-1-2001"
                                       min="1-1-2019" class="form-control">

                            </div>
                        </div>

                        <div class="form-group">
                            <lable for="exampleInputCat" class="control-label">
                                <h4>Change Date</h4>
                            </lable>
                            <div class="input-group">
                                {{--<input readonly placeholder="Select Date" class="form-control date-picker filter_by_date" id="id-date-picker-1" name="change_date" value="" type="text" data-date-format="yyyy-mm-dd"  required  />--}}
                                {{--<span class="input-group-addon">--}}
																		{{--<i class="fa fa-calendar bigger-110"></i>--}}
																	{{--</span>--}}

                                <input type="date" name="change_date" max="1-1-2001"
                                       min="1-1-2019" class="form-control">
                            </div>
                        </div>


                    </div>
                    <div style="border: none" class="modal-footer">
                        <button type="button" class="btn btn-dark"
                                data-dismiss="modal">Close
                        </button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--edit part--}}
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div style="background: linear-gradient(145deg, #07cdff, #46e3ff);" class="modal-header">

                    <u style="color:#fff"><h1 style="font-size: 39px;
    											color: #fff;">Edit Filter</h1></u>
                    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                </div>
                <div id="edit-content">
                </div>
            </div>
        </div>
    </div>
    {{--delete modal part--}}
    <div class="modal fade" id="delete-tehsil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form  method="POST" id="delete-form">
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                        <h3>Delete</h3>
                    </div>
                    <div class="modal-body">
                        <p> Are you sure to delete?</p>
                    </div>
                    <div style="border: none" class="modal-footer">
                        <button class="btn btn-dark" type="submit">Delete</button>
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>



        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-success').slideUp('slow');
            }, 1000);
        });
        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-danger').slideUp('slow');
            }, 1000);
        });
        $('.edit-btn').click(function (e) {
            var request = $.ajax({
                url: $(this).data('action'),
                type: "get",
                dataType: "html",
                cache: false,
                success:
                    function (result) {
                        $('#edit-content').html(result);

                    },
                error: function (result) {
                    alert('error this form');
                }
            });
        });

        $('.delete-btn').click(function (e) {
            var url = $(this).data('action');
            $('#delete-form').attr('action',url);
        });

        $('#myDatepicker').datepicker({
            format: "yyyy/mm",
            startView: "year",
            minViewMode: "months"
        });

        // Summernote
        $('#summernote').summernote({
            height: 200
        });
    </script>
@endsection



