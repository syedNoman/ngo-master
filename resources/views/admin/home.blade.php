@extends('admin.layouts.app')
@section('style')
    <style>
        #chartdiv {
            width		: 100%;
            height		: 500px;
            font-size	: 11px;
        }
        #chartdiv2 {
            width		: 100%;
            height		: 500px;
            font-size	: 11px;
        }
        #chartdiv3 {
            width		: 100%;
            height		: 500px;
            font-size	: 11px;
        }
    </style>
    <style>
        .panel-yellow > .panel-heading {
            border-color: #f0ad4e;
            color: white;
            background-color: #f0ad4e;
        }

        .panel-green > .panel-heading {
            border-color: #5cb85c;
            color: white;
            background-color: #5cb85c;
        }

        .panel-red > .panel-heading {
            border-color: #d9534f;
            color: white;
            background-color: #d9534f;
        }

        .panel-green > a {
            color: #5cb85c;
        }

        .panel-green {
            border-color: #5cb85c;
        }

        .panel-yellow > a {
            color: #f0ad4e;
        }

        .panel-yellow {
            border-color: #f0ad4e;
        }

        .panel-red > a {
            color: #d9534f;
        }

        .panel-red {
            border-color: #d9534f;
        }
    </style>
@endsection
@section('content')

    @include("admin.layouts.lift-manu-bar")

    <div class="main-content">
        <div class="main-content-inner">
            @include('admin.layouts.navbar')

            <div class="page-content">
                {{--<div class="ace-settings-container" id="ace-settings-container">--}}
                    {{--<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">--}}
                        {{--<i class="ace-icon fa fa-cog bigger-130"></i>--}}
                    {{--</div>--}}

                    {{--<div class="ace-settings-box clearfix" id="ace-settings-box">--}}
                        {{--<div class="pull-left width-50">--}}
                            {{--<div class="ace-settings-item">--}}
                                {{--<div class="pull-left">--}}
                                    {{--<select id="skin-colorpicker" class="hide">--}}
                                        {{--<option data-skin="no-skin" value="#438EB9">#438EB9</option>--}}
                                        {{--<option data-skin="skin-1" value="#222A2D">#222A2D</option>--}}
                                        {{--<option data-skin="skin-2" value="#C6487E">#C6487E</option>--}}
                                        {{--<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                                {{--<span>&nbsp; Choose Skin</span>--}}
                            {{--</div>--}}

                            {{--<div class="ace-settings-item">--}}
                                {{--<input type="checkbox" class="ace ace-checkbox-2 ace-save-state"--}}
                                       {{--id="ace-settings-navbar" autocomplete="off"/>--}}
                                {{--<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>--}}
                            {{--</div>--}}

                            {{--<div class="ace-settings-item">--}}
                                {{--<input type="checkbox" class="ace ace-checkbox-2 ace-save-state"--}}
                                       {{--id="ace-settings-sidebar" autocomplete="off"/>--}}
                                {{--<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>--}}
                            {{--</div>--}}

                            {{--<div class="ace-settings-item">--}}
                                {{--<input type="checkbox" class="ace ace-checkbox-2 ace-save-state"--}}
                                       {{--id="ace-settings-breadcrumbs" autocomplete="off"/>--}}
                                {{--<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>--}}
                            {{--</div>--}}

                            {{--<div class="ace-settings-item">--}}
                                {{--<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"--}}
                                       {{--autocomplete="off"/>--}}
                                {{--<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>--}}
                            {{--</div>--}}

                            {{--<div class="ace-settings-item">--}}
                                {{--<input type="checkbox" class="ace ace-checkbox-2 ace-save-state"--}}
                                       {{--id="ace-settings-add-container" autocomplete="off"/>--}}
                                {{--<label class="lbl" for="ace-settings-add-container">--}}
                                    {{--Inside--}}
                                    {{--<b>.container</b>--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div><!-- /.pull-left -->--}}

                        {{--<div class="pull-left width-50">--}}
                            {{--<div class="ace-settings-item">--}}
                                {{--<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"--}}
                                       {{--autocomplete="off"/>--}}
                                {{--<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>--}}
                            {{--</div>--}}

                            {{--<div class="ace-settings-item">--}}
                                {{--<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"--}}
                                       {{--autocomplete="off"/>--}}
                                {{--<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>--}}
                            {{--</div>--}}

                            {{--<div class="ace-settings-item">--}}
                                {{--<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"--}}
                                       {{--autocomplete="off"/>--}}
                                {{--<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>--}}
                            {{--</div>--}}
                        {{--</div><!-- /.pull-left -->--}}
                    {{--</div><!-- /.ace-settings-box -->--}}
                {{--</div><!-- /.ace-settings-container -->--}}

                <div class="page-header">
                    <h1>
                        Dashboard
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            overview &amp; stats
                        </small>
                    </h1>
                </div><!-- /.page-header -->
                <div id="page-wrapper">

                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-check fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            {{--<div class="huge">{{ \App\Lecture::where('role', '=' , 1)->count() }}</div>--}}
                                            <div>Total PSD!</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-support fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            {{--<div class="huge">{{ \App\Lecture::where('role', '=' , 2)->count() }}</div>--}}
                                            <div>Total Video!</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-yellow">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-shopping-cart fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            {{--<div class="huge"> {{ \App\User::where('role', '=' , 2)->count() }}</div>--}}
                                            <div>Schalors!</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6" style="">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-tasks fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            {{--<div class="huge"> {{ \App\Lecture::where('role', '=' , 3)->count() }}</div>--}}
                                            <div>Books!</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                            </div>
                        </div>

                    </div>
<div class="row">

    <div class="col-md-12">


        <!-- Resources -->
        <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
        <script src="https://www.amcharts.com/lib/3/serial.js"></script>
        <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
        <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
        <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

        <!-- Chart code -->
        {{--<script>--}}
            {{--var chart = AmCharts.makeChart( "chartdiv", {--}}
                {{--"type": "serial",--}}
                {{--"theme": "light",--}}
                {{--"dataProvider": [--}}
                    {{--@foreach($orders as $order)--}}
                            {{--{--}}
                            {{--"country": "{{ $order->hour }}",--}}
                            {{--"visits": "{{ $order->total }}"--}}
                        {{--},--}}
                    {{--@endforeach--}}
                   {{--],--}}
                {{--"valueAxes": [ {--}}
                    {{--"gridColor": "#FFFFFF",--}}
                    {{--"gridAlpha": 0.2,--}}
                    {{--"dashLength": 0--}}
                {{--} ],--}}
                {{--"gridAboveGraphs": true,--}}
                {{--"hideCredits": true,--}}
                {{--"startDuration": 1,--}}
                {{--"graphs": [ {--}}
                    {{--"balloonText": "[[category]]: <b>[[value]]</b>",--}}
                    {{--"fillAlphas": 0.8,--}}
                    {{--"lineAlpha": 0.2,--}}
                    {{--"type": "column",--}}
                    {{--"valueField": "visits"--}}
                {{--} ],--}}
                {{--"chartCursor": {--}}
                    {{--"categoryBalloonEnabled": false,--}}
                    {{--"cursorAlpha": 0,--}}
                    {{--"zoomable": false--}}
                {{--},--}}
                {{--"categoryField": "country",--}}
                {{--"categoryAxis": {--}}
                    {{--"gridPosition": "start",--}}
                    {{--"gridAlpha": 0,--}}
                    {{--"tickPosition": "start",--}}
                    {{--"tickLength": 20--}}
                {{--},--}}
                {{--"export": {--}}
                    {{--"enabled": false--}}
                {{--}--}}

            {{--} );--}}
        {{--</script>--}}

        <!-- HTML -->
        {{--<h1>Hourly Sales Chart</h1>--}}

        <div id="chartdiv"></div>
        {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}

                {{--<script>--}}
                    {{--var chart = AmCharts.makeChart( "chartdiv3", {--}}
                        {{--"type": "serial",--}}
                        {{--"theme": "light",--}}
                        {{--"dataProvider": [--}}
                                {{--@foreach($dailyorders as $order)--}}
                            {{--{--}}
                                {{--"country": "{{ $order->day }}",--}}
                                {{--"visits": "{{ $order->sums }}"--}}
                            {{--},--}}
                            {{--@endforeach--}}
                        {{--],--}}
                        {{--"valueAxes": [ {--}}
                            {{--"gridColor": "#FFFFFF",--}}
                            {{--"gridAlpha": 0.2,--}}
                            {{--"dashLength": 0--}}
                        {{--} ],--}}
                        {{--"gridAboveGraphs": true,--}}
                        {{--"hideCredits": true,--}}
                        {{--"startDuration": 1,--}}
                        {{--"graphs": [ {--}}
                            {{--"balloonText": "[[category]]: <b>[[value]]</b>",--}}
                            {{--"fillAlphas": 0.8,--}}
                            {{--"lineAlpha": 0.2,--}}
                            {{--"type": "column",--}}
                            {{--"valueField": "visits"--}}
                        {{--} ],--}}
                        {{--"chartCursor": {--}}
                            {{--"categoryBalloonEnabled": false,--}}
                            {{--"cursorAlpha": 0,--}}
                            {{--"zoomable": false--}}
                        {{--},--}}
                        {{--"categoryField": "country",--}}
                        {{--"categoryAxis": {--}}
                            {{--"gridPosition": "start",--}}
                            {{--"gridAlpha": 0,--}}
                            {{--"tickPosition": "start",--}}
                            {{--"tickLength": 20--}}
                        {{--},--}}
                        {{--"export": {--}}
                            {{--"enabled": false--}}
                        {{--}--}}

                    {{--} );--}}
                {{--</script>--}}

                {{--<!-- HTML -->--}}
                {{--<h1>Daily Sales Chart</h1>--}}

                {{--<div id="chartdiv3"></div>--}}

            {{--</div></div>--}}

        {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}

                {{--<script>--}}
                    {{--var chart = AmCharts.makeChart( "chartdiv2", {--}}
                        {{--"type": "serial",--}}
                        {{--"theme": "light",--}}
                        {{--"dataProvider": [--}}
                                {{--@foreach($orderproducts as $order)--}}
                            {{--{--}}
                                {{--"country": "{{ $order->month }}",--}}
                                {{--"visits": "{{ $order->total }}"--}}
                            {{--},--}}
                            {{--@endforeach--}}
                        {{--],--}}
                        {{--"valueAxes": [ {--}}
                            {{--"gridColor": "#FFFFFF",--}}
                            {{--"gridAlpha": 0.2,--}}
                            {{--"dashLength": 0--}}
                        {{--} ],--}}
                        {{--"gridAboveGraphs": true,--}}
                        {{--"hideCredits": true,--}}
                        {{--"startDuration": 1,--}}
                        {{--"graphs": [ {--}}
                            {{--"balloonText": "[[category]]: <b>[[value]]</b>",--}}
                            {{--"fillAlphas": 0.8,--}}
                            {{--"lineAlpha": 0.2,--}}
                            {{--"type": "column",--}}
                            {{--"valueField": "visits"--}}
                        {{--} ],--}}
                        {{--"chartCursor": {--}}
                            {{--"categoryBalloonEnabled": false,--}}
                            {{--"cursorAlpha": 0,--}}
                            {{--"zoomable": false--}}
                        {{--},--}}
                        {{--"categoryField": "country",--}}
                        {{--"categoryAxis": {--}}
                            {{--"gridPosition": "start",--}}
                            {{--"gridAlpha": 0,--}}
                            {{--"tickPosition": "start",--}}
                            {{--"tickLength": 20--}}
                        {{--},--}}
                        {{--"export": {--}}
                            {{--"enabled": false--}}
                        {{--}--}}

                    {{--} );--}}
                {{--</script>--}}

                {{--<!-- HTML -->--}}
                {{--<h1>Monthly Sales Chart</h1>--}}

                {{--<div id="chartdiv2"></div>--}}

            {{--</div></div>--}}


    </div>

</div>
                </div>




            </div><!-- /.page-content -->
        </div>
    </div>
    </div><!-- /.main-content -->
@endsection