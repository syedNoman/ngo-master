@extends('admin.layouts.app')
@section('content')

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="{{route('dashboard')}}">Home</a>
                    </li>


                    <li>
                        <a href="{{route('dashboard')}}">dashboard</a>
                    </li>
                </ul><!-- /.breadcrumb -->


            </div>
            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    {{--<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">--}}
                        {{--<i class="ace-icon fa fa-cog bigger-130"></i>--}}
                    {{--</div>--}}

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->
                <div class="page-header">

                 <div class="row">
                        <div class="col-md-3">
                            <h1>
                                Welcome to Report Dashboard
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                </small>
                            </h1>
                        </div>

                    </div>

                </div><!-- /.page-header -->
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div id="page-wrapper">

                            <!-- /.row -->
                            <div class="row">

                                @if(Auth::user()->role==1)
                                    <div class="col-lg-2 col-md-6">
                                        <div style="border-color:#3aaca8;" class="panel panel-primary">
                                            <div style="background-color:#3aaca8 !important;height: 250px;border-color:#3aaca8;" class="panel-heading">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <i class="fa fa-user fa-5x"></i>
                                                        {{--<i><img src="{{asset('assets/dash_icons/')}}" alt=""></i>--}}
                                                    </div>
                                                    <div class="col-xs-9 text-right">
                                                        {{--<div class="huge">{{ \App\Lecture::where('role', '=' , 1)->count() }}</div>--}}
                                                        <div> Water Plants</div>
                                                        <div>
                                                            {{\App\Plant::all()->count()}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Plant::all()->count()}}</div>
                                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: 'Lato', sans-serif;">Total Plants </div>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-lg-2 col-md-6">
                                        <div style="border-color:#3aaca8;" class="panel panel-primary">
                                            <div style="background-color:#3aaca8 !important;height: 250px;border-color:#3aaca8;" class="panel-heading">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <i><img style="height: 70px;" src="{{asset('assets/dash_icons/today-consumption.png')}}" alt=""></i>
                                                    </div>
                                                    <div class="col-xs-9 text-right">
                                                        {{--<div class="huge">{{ \App\Lecture::where('role', '=' , 1)->count() }}</div>--}}
                                                        <div> Today Consumptions</div>
                                                        <div>
                                                            {{DB::table('consumptions')->where('plant_id',Auth::user()->plant_id)->pluck('today_total_consumer_serve')->sum()}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{DB::table('consumptions')->where('plant_id',Auth::user()->plant_id)->pluck('today_total_consumer_serve')->sum()}}</div>
                                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: 'Lato', sans-serif;">Today Consumptions </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="col-lg-2 col-md-6">
                                    <div class="panel panel-primary">
                                        <div  style="height: 250px;
                                                    background-color: #263c87;
                                                    border-color: #263c87;" class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i><img style="height: 70px;" src="{{asset('assets/dash_icons/maintainer.png')}}" alt=""></i>
                                                </div>
                                                <div class="col-xs-9 text-right">

                                                        <div>Total Maintainers </div>

                                                    @if(Auth::user()->role==1)
                                                        {{\App\User::where('role',0)->count()}}
                                                    @else
                                                        {{\App\User::where('role',0)->where('plant_id',Auth::user()->plant_id)->count()}}
                                                    @endif



                                                </div>
                                            </div>

                                            @if(Auth::user()->role==1)
                                                <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\User::where('role',0)->count()}}</div>
                                                @else
                                                <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\User::where('role',0)->where('plant_id',Auth::user()->plant_id)->count()}}</div>
                                                @endif
                                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: 'Lato', sans-serif;">Total Maintainers </div>
                                        </div>

                                    </div>
                                </div>

                                    <div class="col-lg-2 col-md-6">
                                    <div class="panel panel-warning">
                                        <div style="color: #AB6D0A;
                                                background-color: wheat;
                                                border-color: #faebcc; height: 250px;" class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i><img style="height: 70px;" src="{{asset('assets/dash_icons/consumer.png')}}" alt=""></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    <div>Total Consumers </div>
                                                    @if(Auth::user()->role==1)
                                                        <div>{{\App\Consumer::all()->count()}}</div>
                                                        @else
                                                        <div>{{\App\Consumer::where('plant_id',Auth::user()->plant_id)->count()}}</div>
                                                    @endif
                                                </div>
                                                </div>
                                            @if(Auth::user()->role == 1)
                                                <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Consumer::all()->count()}}</div>
                                                @else
                                                <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Consumer::where('plant_id',Auth::user()->plant_id)->count()}}</div>
                                                @endif

                                            <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: 'Lato', sans-serif;">Total Consumers </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-lg-2 col-md-6">
                                    <div class="panel panel-success">
                                        <div style="height: 250px;" class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i><img style="height: 70px;" src="{{asset('assets/dash_icons/cards.png')}}" alt=""></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                        <div>Card issued</div>
                                                    @if(Auth::user()->role == 1)
                                                        <div>{{\App\Consumer::where('card_issued',1)->count()}}</div>
                                                    @else
                                                        <div>{{\App\Consumer::where('plant_id',Auth::user()->plant_id)->where('card_issued',1)->count()}}</div>
                                                    @endif

                                                    </div>
                                            </div>
                                            @if(Auth::user()->role == 1)
                                                <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Consumer::where('card_issued',1)->count()}}</div>
                                            @else
                                                <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Consumer::where('plant_id',Auth::user()->plant_id)->where('card_issued',1)->count()}}</div>
                                                @endif

                                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: 'Lato', sans-serif;">Card Issue </div>


                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-2 col-md-6">
                                    <div class="panel panel-danger">
                                        <div style="height:250px;" class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i><img style="height: 70px;" src="{{asset('assets/dash_icons/no card.png')}}" alt=""></i>
                                                </div>
                                                <div class="col-xs-9 text-right">

                                                    <div>NO Card issue</div>
                                                    @if(Auth::user()->role == 1)
                                                        <div>{{\App\Consumer::where('card_issued',0)->count()}}</div>
                                                    @else
                                                        <div>{{\App\Consumer::where('plant_id',Auth::user()->plant_id)->where('card_issued',0)->count()}}</div>
                                                    @endif

                                                </div>
                                            </div>
                                            @if(Auth::user()->role == 1)
                                                <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Consumer::where('card_issued',0)->count()}}</div>
                                            @else
                                                <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Consumer::where('plant_id',Auth::user()->plant_id)->where('card_issued',0)->count()}}</div>
                                            @endif
                                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: 'Lato', sans-serif;">No Card Issue </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-2 col-md-6">
                                    <div style="border-color:#D15B47!important" class="panel panel-primary">
                                        <div style="height: 250px;background-color: #D15B47!important; border-color:#D15B47!important" class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i><img style="height: 70px;" src="{{asset('assets/dash_icons/water-consumption.png')}}" alt=""></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    {{--<div class="huge">{{ \App\Lecture::where('role', '=' , 1)->count() }}</div>--}}
                                                    <div> Water Consumptions</div>
                                                    <div>
                                                        @if(Auth::user()->role == 1)
                                                            {{DB::table('consumptions')->pluck('total_consumer_serve')->sum()}}
                                                            @else
                                                            {{DB::table('consumptions')->where('plant_id',Auth::user()->plant_id)->pluck('total_consumer_serve')->sum()}}
                                                            @endif

                                                    </div>
                                                </div>
                                            </div>
                                            @if(Auth::user()->role == 1)
                                                <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{DB::table('consumptions')->pluck('total_consumer_serve')->sum()}}</div>
                                            @else
                                                <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{DB::table('consumptions')->where('plant_id',Auth::user()->plant_id)->pluck('total_consumer_serve')->sum()}}</div>
                                            @endif

                                            <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: 'Lato', sans-serif;">Total Consumptions </div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->


                <div class="page-header">

                    <div class="row">
                        <div class="col-md-12">
                            <h1>
                                Welcome to Complaint Dashboard
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    {{--Static &amp; Dynamic Tables--}}
                                </small>
                            </h1>
                        </div>

                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-2 col-md-6">
                        <div class="panel panel-danger">
                            <div style="height: 250px;" class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i><img style="height: 70px;" src="{{asset('assets/dash_icons/cards.png')}}" alt=""></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div>Pending</div>
                                        @if(Auth::user()->role == 1)
                                            <div >{{\App\Complaint::where('status',0)->count()}}</div>
                                        @elseif(Auth::user()->role == 0)
                                            <div>{{\App\Complaint::where('status',0)->where('user_id',Auth::user()->user_id)->count()}}</div>
                                        @endif

                                    </div>
                                </div>
                                @if(Auth::user()->role == 1)
                                    <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Complaint::where('status',0)->count()}}</div>
                                    @elseif(Auth::user()->role == 0)
                                    <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Complaint::where('status',0)->where('user_id',Auth::user()->user_id)->count()}}</div>
                                @endif
                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: 'Lato', sans-serif;">Pending</div>


                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-6">
                        <div class="panel panel-warning">
                            <div style="color: #AB6D0A;
                                                background-color: wheat;
                                                border-color: #faebcc; height: 250px;" class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i><img style="height: 70px;" src="{{asset('assets/dash_icons/consumer.png')}}" alt=""></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div>Processing</div>
                                        @if(Auth::user()->role == 1)
                                            <div>{{\App\Complaint::where('status',1)->count()}}</div>
                                        @elseif(Auth::user()->role == 0)
                                            <div>{{\App\Complaint::where('status',1)->where('user_id',Auth::user()->user_id)->count()}}</div>
                                        @endif
                                    </div>
                                </div>
                                @if(Auth::user()->role == 1)
                                    <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Complaint::where('status',1)->count()}}</div>
                                @elseif(Auth::user()->role == 0)
                                    <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Complaint::where('status',1)->where('user_id',Auth::user()->user_id)->count()}}</div>
                                @endif

                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: 'Lato', sans-serif;">Processing</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-6">
                        <div class="panel panel-success">
                            <div style="height: 250px;" class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i><img style="height: 70px;" src="{{asset('assets/dash_icons/cards.png')}}" alt=""></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div>Complete</div>
                                        @if(Auth::user()->role == 1)
                                            <div>{{\App\Complaint::where('status',2)->count()}}</div>
                                        @elseif(Auth::user()->role == 0)
                                            <div>{{\App\Complaint::where('status',2)->where('user_id',Auth::user()->user_id)->count()}}</div>
                                        @endif

                                    </div>
                                </div>
                                @if(Auth::user()->role == 1)
                                    <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Complaint::where('status',2)->count()}}</div>
                                @elseif(Auth::user()->role == 0)
                                    <div  style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: 'Lato', sans-serif;
                                                        margin-top:0px;">{{\App\Complaint::where('status',2)->where('user_id',Auth::user()->user_id)->count()}}</div>
                                @endif

                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: 'Lato', sans-serif;">Complete</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
@endsection

@section('script')

    <script type="text/javascript">
        $(window).load(function() {
            $(".loader").fadeOut("slow");
        });
    </script>

    <script>
        //        $('input[name="dates"]').daterangepicker();

        $('#myDatepicker').datepicker({
            format: "yyyy/mm",
            startView: "year",
            minViewMode: "months"
        });
        $('.filter_by_month').change(function (){
            var abc=$('.filter_by_month').val();
            var url = document.URL;
            var stuff = url.split('/');
            var id = stuff[stuff.length-1];
//            alert(id);
            if(abc){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                $.ajax({
                    url:'/year-filter?id='+id,
                    type:'POST',
                    data:{data:abc},
                    dataType:'JSON',


                    success:function (Result) {
                        $('#member_register').html(Result.member_registerd);
                        $('#user_card_issue').html(Result.card_issued);
                        $('#no_card_issue').html(Result.card_not_issued);
                        $('#consum').html(Result.consum);
                    },
                    error:function (Result) {

                    }
                });
            }


        });

        $('.filter_by_date').change(function (){
            var abc=$('.filter_by_date').val();
            var url = document.URL;
            var stuff = url.split('/');
            var id = stuff[stuff.length-1];
//            alert(id);
            if(abc){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                $.ajax({
                    url:'/date-filter?id='+id,
                    type:'POST',
                    data:{data:abc},
                    dataType:'JSON',


                    success:function (Result) {
                        $('#member_register').html(Result.member_registerd);
                        $('#user_card_issue').html(Result.card_issued);
                        $('#no_card_issue').html(Result.card_not_issued);
                        $('#consum').html(Result.consum);
                    },
                    error:function (Result) {

                    }
                });
            }


        });

        $('.filter_by_date_range2').change(function (){
            var start=$('.filter_by_date_range1').val();
            var end=$('.filter_by_date_range2').val();
//             alert('ok');
            var url = document.URL;
            var stuff = url.split('/');
            var id = stuff[stuff.length-1];
//            alert(id);
            if(start && end){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                $.ajax({
                    url:'/range-filter?id='+id,
                    type:'POST',
                    data:{data:start,end},
                    dataType:'JSON',


                    success:function (Result) {
                        $('#member_register').html(Result.member_registerd);
                        $('#user_card_issue').html(Result.card_issued);
                        $('#no_card_issue').html(Result.card_not_issued);
                        $('#consum').html(Result.consum);
                    },
                    error:function (Result) {

                    }
                });
            }


        });
    </script>




@endsection