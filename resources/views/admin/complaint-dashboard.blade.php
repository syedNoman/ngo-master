@extends('admin.layouts.app2')
@section('content')

    <div style="background: url('{{asset('background.png')}}')" class="content">
        <div class="container-fluid">
            <h2 style="font-size: 30px;
    color: #696969;
    font-weight: 300;
    margin-bottom:0px;
    margin-top: 20px;">Complaint Tracking</h2>
            <div class="row">

                {{--Remaing tabs--}}
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div style="height:125px" class="card card-stats">
                        <div style="text-align: left" class="card-header card-header-success card-header-icon">

                            <i class="material-icons">
                                <img style="margin-top: -15px;margin-bottom: 0px;"
                                     src="{{asset('web-assets/images/pending.png')}}">
                            </i>
                        </div>
                        <div style="display: block;">
                            <div class="stats">

                                <div style="font-size: 30px;padding: 30px">
                                    <span style="font-size: 26px" class="card-title">Pending Complaints</span>
                                    <span style="float: right;font-size: 26px">{{\App\Complaint::where('status',0)->count()}}</span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End Maintaniner tabs--}}

                {{--Consumer Issue tabs--}}
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div style="height:125px" class="card card-stats">
                        <div style="text-align: left" class="card-header card-header-success card-header-icon">

                            <i class="material-icons">
                                <img style="margin-top: -15px;margin-bottom: 0px;"
                                     src="{{asset('web-assets/images/processing.png')}}">
                            </i>
                        </div>
                        <div style="display: block;">
                            <div class="stats">

                                <div style="font-size: 30px;padding: 30px">
                                    <span style="font-size: 26px" class="card-title">Processing Complaints</span>
                                    <span style="float: right;font-size: 26px">{{\App\Complaint::where('status',1)->count()}}</span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End Consumer Issue tabs--}}

                {{--Card Issue tabs--}}
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div style="height:125px" class="card card-stats">
                        <div style="text-align: left" class="card-header card-header-success card-header-icon">

                            <i class="material-icons">
                                <img style="margin-top: -15px;margin-bottom: 0px;"
                                     src="{{asset('web-assets/images/completed.png')}}">
                            </i>
                        </div>
                        <div style="display: block;">
                            <div class="stats">

                                <div style="font-size: 30px;padding: 30px">
                                    <span style="font-size: 26px" class="card-title">Complete Complaints</span>
                                    <span style="float: right;font-size: 26px">{{\App\Complaint::where('status',2)->count()}}</span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End Card Issue tabs--}}


            </div>
            {{--Graph tabs--}}

            {{--<div class="row">--}}
            {{--<div class="col-md-4">--}}
            {{--<div class="card card-chart">--}}
            {{--<div class="card-header card-header-success">--}}
            {{--<div class="ct-chart" id="dailySalesChart"></div>--}}
            {{--</div>--}}
            {{--<div class="card-body">--}}
            {{--<h4 class="card-title">Daily Sales</h4>--}}
            {{--<p class="card-category">--}}
            {{--<span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>--}}
            {{--</div>--}}
            {{--<div class="card-footer">--}}
            {{--<div class="stats">--}}
            {{--<i class="material-icons">access_time</i> updated 4 minutes ago--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4">--}}
            {{--<div class="card card-chart">--}}
            {{--<div class="card-header card-header-warning">--}}
            {{--<div class="ct-chart" id="websiteViewsChart"></div>--}}
            {{--</div>--}}
            {{--<div class="card-body">--}}
            {{--<h4 class="card-title">Email Subscriptions</h4>--}}
            {{--<p class="card-category">Last Campaign Performance</p>--}}
            {{--</div>--}}
            {{--<div class="card-footer">--}}
            {{--<div class="stats">--}}
            {{--<i class="material-icons">access_time</i> campaign sent 2 days ago--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4">--}}
            {{--<div class="card card-chart">--}}
            {{--<div class="card-header card-header-danger">--}}
            {{--<div class="ct-chart" id="completedTasksChart"></div>--}}
            {{--</div>--}}
            {{--<div class="card-body">--}}
            {{--<h4 class="card-title">Completed Tasks</h4>--}}
            {{--<p class="card-category">Last Campaign Performance</p>--}}
            {{--</div>--}}
            {{--<div class="card-footer">--}}
            {{--<div class="stats">--}}
            {{--<i class="material-icons">access_time</i> campaign sent 2 days ago--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}

            {{--End Grpah tabs--}}

        </div>
    </div>
@endsection
