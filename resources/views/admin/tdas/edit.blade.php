<form id="editfrom" method="POST" action="{{ route('update-tds',$tds->id)}}" enctype="multipart/form-data">
    <div class="modal-body">

        @csrf
        <input hidden name="plant_id" value="{{Auth::user()->plant_id}}">
        <div style="margin-top: 60px" class="form-group">
            <input  class="form-control" id="nameid" value="{{ $tds->tda }}" name="tda"
                   type="number" placeholder="Enter TDS Number"
                   required>
        </div>


    </div>
    <div style="border: none" class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
</form>
