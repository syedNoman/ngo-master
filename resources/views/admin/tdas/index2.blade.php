
@extends('admin.layouts.app')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>

                    <li>
                        <a href="{{route('dashboard')}}">Dashboard</a>
                    </li>
                </ul><!-- /.breadcrumb -->

                <div class="nav-search" id="nav-search">
                    <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
                    </form>
                </div><!-- /.nav-search -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        TDS
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Show All TDS
                        </small>
                    </h1>

                </div><!-- /.page-header -->

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="card-header">
                            @if(Auth::user()->role==0)
                                <button style="margin-bottom: 15px" class="btn-info btn-sm pull-right b" data-toggle="modal" data-target="#exampleModal">Add
                                    TDS
                                </button>
                                @endif
                                @if(Auth::user()->role==1)
                            <div class="col-md-4 col-md-offset-4">
                                <select style="color: #438EB9;
                                        font-size: 20px;
                                        margin-bottom: 10px;
                                        font-family: inherit;
                                        height: 38px;
                                         border: 1px solid #6FB3E0;
                                        border-radius: 15px; "  class="form-control" name="forma" onchange="window.location.href = '/plant-tds/'+this.value" >
                                    <option hidden value="" href="">Select Plant</option>
                                    @foreach(\App\Plant::all() as $plant)
                                        <option  value="{{(@$plant->id)}}" href="" @if(request()->url() == url('report/'.$plant->id))selected @endif>{{@$plant->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                                @endif

                        </div>
                        <div class="col-md-8 col-md-offset-2">
                                @if(Session::has('message'))
                                    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em></div>
                                @endif
                            @if(Session::has('message-delete'))
                                @if(Session::has('message-delete'))
                                    <div class="alert alert-danger"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message-delete') !!}</em></div>
                                @endif
                            @endif
                        </div>

                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">

                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <!-- PAGE CONTENT BEGINS -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel"><h3>Add
                                                                TDA
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button></h3>
                                                        </h5>
                                                    </div>
                                                    <form id="form" method="POST" action="{{route('add-tds')}}"
                                                          enctype="multipart/form-data">
                                                        <div class="modal-body">
                                                            {{ csrf_field() }}

                                                            <input hidden name="plant_id" value="{{Auth::user()->plant_id}}">

                                                            <div class="form-group">
                                                                <lable for="exampleInputCat" class="control-label">
                                                                  <h4>Value</h4>
                                                                </lable>
                                                                <input  class="form-control" id="tda" name="tda"
                                                                       type="number" placeholder="Enter Value"
                                                                       required>
                                                            </div>


                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <table id="simple-table" class="table  table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="detail-col">NO#</th>
                                        <th>TDS</th>
                                        <th>Plant Name</th>
                                        <th>Date</th>
                                        <th style="text-align:center" class="col-md-1">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tdas as $key => $tda)

                                        <tr>
                                            <td style="text-align: center">
                                                {{ ($tdas->currentpage() - 1) * $tdas->perpage() + 1 + $key }}

                                            </td>
                                            <td style="text-transform: capitalize">{{ $tda->tda }}</td>
                                            <td style="text-transform: capitalize">{{$tda->plant->name}}</td>
                                            <td style="text-transform: capitalize">{{$tda->created_at->format('Y/m/d')  }}</td>
                                            {{--<td style="text-transform: capitalize">{{ @$tda->plants->name }}</td>--}}
                                            <td style="text-align: center">
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <button title="Update" class="btn btn-xs btn-success edit-btn"
                                                            data-toggle="modal"
                                                            data-action="{{route('edit-tds',['id'=>$tda->id])}}"
                                                            data-target="#exampleModal2">
                                                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                    </button>
                                                    @if(Auth::user()->role==1)
                                                        <button title="Delete" type="button" data-toggle="modal"
                                                                data-target="#delete-tehsil"
                                                                data-action="{{route('delete-tds',['id'=>$tda->id])}}"
                                                                class="show-delete-modal btn btn-xs btn-danger delete-btn ">
                                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                        </button>
                                                        @endif

                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                    @endforeach
                                </table>
                                <div class="text-right">{{ $tdas->links() }}</div>
                            </div><!-- /.span -->
                        </div><!-- /.row -->
                    </div>
                </div>
            </div><!-- /.page-content -->
        </div>
    </div>
    {{--edit part--}}
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><h3>Edit Tehsil
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button></h3>
                    </h5>
                </div>
                <div id="edit-content">
                </div>
            </div>
        </div>
    </div>
    {{--delete modal part--}}
    <div class="modal fade" id="delete-tehsil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form  method="POST" id="delete-form">
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                        <h3>Delete</h3>
                    </div>
                    <div class="modal-body">
                        <p> Are you sure to delete?</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" type="submit">Delete</button>
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-success').fadeOut('slow');
            }, 3000);
        });
        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-danger').fadeOut('slow');
            }, 3000);
        });
        $('.edit-btn').click(function (e) {
            var request = $.ajax({
                url: $(this).data('action'),
                type: "get",
                dataType: "html",
                cache: false,
                success:
                    function (result) {
                        $('#edit-content').html(result);

                    },
                error: function (result) {
                    alert('error this form');
                }
            });
        });

        $('.delete-btn').click(function (e) {
            var url = $(this).data('action');
            $('#delete-form').attr('action',url);
        });

        // Summernote
        $('#summernote').summernote({
            height: 200
        });
    </script>
@endsection

