@extends('admin.layouts.app2')
@section('content')
<style>
    .card-title{
        margin: 0;
        text-transform: uppercase;
        color: darkgrey !important;
        font-size: 15px !important;
        font-weight: 400;
        margin-right: 10px;
    }



</style>


    <div style="background: url('{{asset('background.png')}}')" class="content">
        <div class="container-fluid">
            <div class="row">

                {{--Plant tabs--}}
                @if(Auth::user()->role==1)
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-warning card-header-icon">
                                <div style="background: linear-gradient(118deg, #4facfe, #00f2fe) !important;box-shadow: 1px 4px 5px -1px #ccc;"
                                     class="card-icon">
                                    <i class="material-icons">
                                        <img style="margin-top: -15px;margin-bottom: 0px;margin-top: -15px;
                                            margin-bottom: 0px;height: 30px;"
                                             src="{{asset('web-assets/images/plant-icon.png')}}">
                                    </i>
                                </div>
                                <h3 class="card-title">Plants</h3>
                            </div>
                            <div style="display: block;text-align: right;" class="card-footer">
                                <div class="stats">
                                    <div style="font-size: 30px;">
                                        {{\App\Plant::all()->count()}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-warning card-header-icon">
                                <div style="background: linear-gradient(118deg, #4facfe, #00f2fe) !important; box-shadow: 1px 4px 5px -1px #ccc;"
                                     class="card-icon">
                                    <i class="material-icons">
                                        <img style="margin-top: -15px;margin-bottom: 0px;margin-top: -15px;
                                            margin-bottom: 0px;height: 30px;"
                                             src="{{asset('web-assets/images/today-consumption-icon.png')}}">
                                    </i>
                                </div>
                                <h3 class="card-title">Today Consumptions</h3>
                            </div>
                            <div style="display: block;text-align: right;" class="card-footer">
                                <div class="stats">
                                    <div style="font-size: 30px;">
                                        {{DB::table('consumptions')->where('plant_id',Auth::user()->plant_id)->pluck('today_total_consumer_serve')->sum()}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                {{--End Plant tabs--}}

                {{--Maintaniner tabs--}}
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-success card-header-icon">
                            <div style="background: linear-gradient(118deg, #fa709a, #fee140) !important; box-shadow: 1px 4px 5px -1px #ccc;"
                                 class="card-icon">
                                <i class="material-icons">
                                    <img style="margin-top: -15px;margin-bottom: 0px;margin-top: -15px;
                                            margin-bottom: 0px;height: 30px;"
                                         src="{{asset('web-assets/images/maintainer-icon.png')}}">
                                </i>
                            </div>
                            <h3 class="card-title">Maintainers</h3>
                        </div>
                        <div style="display: block;text-align: right;" class="card-footer">
                            <div class="stats">
                                <div style="font-size: 30px;">
                                    @if(Auth::user()->role==1)
                                        {{\App\User::where('role',0)->count()}}
                                    @else
                                        {{\App\User::where('role',0)->where('plant_id',Auth::user()->plant_id)->count()}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End Maintaniner tabs--}}

                {{--Consumer Issue tabs--}}
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-danger card-header-icon">
                            <div style="background: linear-gradient(118deg, #667eea, #764ba2) !important; box-shadow: 1px 4px 5px -1px #ccc;"
                                 class="card-icon">
                                <i class="material-icons">
                                    <img style="margin-top: -15px;margin-bottom: 0px;margin-top: -15px;
                                            margin-bottom: 0px;height: 30px;"
                                         src="{{asset('web-assets/images/consumers-icon.png')}}">
                                </i>
                            </div>
                            <h3 class="card-title">Consumers</h3>
                        </div>
                        <div style="display: block;text-align: right;" class="card-footer">
                            <div class="stats">
                                <div style="font-size: 30px;">
                                    @if(Auth::user()->role == 1)
                                        <div>{{\App\Consumer::all()->count()}}</div>
                                    @else
                                        <div>{{\App\Consumer::where('plant_id',Auth::user()->plant_id)->count()}}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End Consumer Issue tabs--}}

                {{--Card Issue tabs--}}
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-info card-header-icon">
                            <div style="background: linear-gradient(118deg, #43e97b, #38f9d7) !important; box-shadow: 1px 4px 5px -1px #ccc;"
                                 class="card-icon">
                                <i class="material-icons">
                                    <img style="margin-top: -15px;margin-bottom: 0px;margin-top: -15px;
                                            margin-bottom: 0px;height: 30px;"
                                         src="{{asset('web-assets/images/card issued-icon.png')}}">
                                </i>
                            </div>
                            <h3 class="card-title">Card Issued</h3>
                        </div>
                        <div style="display: block;text-align: right;" class="card-footer">
                            <div class="stats">
                                <div style="font-size: 30px;">
                                    @if(Auth::user()->role == 1)
                                        <div>{{\App\Consumer::where('card_issued',1)->count()}}</div>
                                    @else
                                        <div>{{\App\Consumer::where('plant_id',Auth::user()->plant_id)->where('card_issued',1)->count()}}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End Card Issue tabs--}}

                {{--No Card Issue tabs--}}
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-info card-header-icon">
                            <div style="background: linear-gradient(118deg, #16a085, #f4d03f) !important; box-shadow: 1px 4px 5px -1px #ccc;"
                                 class="card-icon">
                                <i class="material-icons">
                                    <img style="margin-top: -15px;margin-bottom: 0px;margin-top: -15px;
                                            margin-bottom: 0px;height: 30px;"
                                         src="{{asset('web-assets/images/nocard-icon.png')}}">
                                </i>
                            </div>
                            <h3 class="card-title">Card Not Issued</h3>
                        </div>
                        <div style="display: block;text-align: right;" class="card-footer">
                            <div class="stats">
                                <div style="font-size: 30px;">
                                    @if(Auth::user()->role == 1)
                                        <div>{{\App\Consumer::where('card_issued',0)->count()}}</div>
                                    @else
                                        <div>{{\App\Consumer::where('plant_id',Auth::user()->plant_id)->where('card_issued',0)->count()}}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End No Card Issue tabs--}}

                {{--Total Consumaption tabs--}}
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-info card-header-icon">
                            <div style="background: linear-gradient(118deg, #d558c8, #24d292) !important; box-shadow: 1px 4px 5px -1px #ccc;"
                                 class="card-icon">
                                <i class="material-icons">
                                    <img style="margin-top: -15px;margin-bottom: 0px;margin-top: -15px;
                                            margin-bottom: 0px;height: 30px;"
                                         src="{{asset('web-assets/images/total-consumtion.png')}}">
                                </i>
                            </div>
                            <h3 class="card-title">Total Consumptions</h3>
                        </div>
                        <div style="display: block;text-align: right;" class="card-footer">
                            <div class="stats">
                                <div style="font-size: 30px;">
                                    @if(Auth::user()->role == 1)
                                        <div>{{DB::table('consumptions')->pluck('total_consumer_serve')->sum()}}</div>
                                    @else
                                        <div>{{DB::table('consumptions')->where('plant_id',Auth::user()->plant_id)->pluck('total_consumer_serve')->sum()}}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End Consumaption tabs--}}


            </div>

            <h2 style="font-size: 30px;
    color: #696969;
    font-weight: 300;
    margin-bottom:0px;
    margin-top: 20px;">rrrComplaint Tracking</h2>
            <div class="row">

                {{--Remaing tabs--}}
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div  class="card card-stats">
                        <div style="text-align: left" class="card-header card-header-success card-header-icon">

                            <i class="material-icons">
                                <img style="margin-top: -15px;margin-bottom: 0px;"
                                     src="{{asset('web-assets/images/pending.png')}}">
                            </i>
                        </div>
                        <div style="display: block;">
                            <div class="stats">

                                <div style="font-size: 30px;padding: 30px">
                                    <span style="font-size: 26px" class="card-title">Pending Complaints</span>
                                    <span style="float: right;font-size: 26px">{{\App\Complaint::where('status',0)->count()}}</span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End Maintaniner tabs--}}

                {{--Consumer Issue tabs--}}
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div  class="card card-stats">
                        <div style="text-align: left" class="card-header card-header-success card-header-icon">

                            <i class="material-icons">
                                <img style="margin-top: -15px;margin-bottom: 0px;"
                                     src="{{asset('web-assets/images/processing.png')}}">
                            </i>
                        </div>
                        <div style="display: block;">
                            <div class="stats">

                                <div style="font-size: 30px;padding: 30px">
                                    <span style="font-size: 26px" class="card-title">Processing Complaints</span>
                                    <span style="float: right;font-size: 26px">{{\App\Complaint::where('status',1)->count()}}</span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End Consumer Issue tabs--}}

                {{--Card Issue tabs--}}
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div  class="card card-stats">
                        <div style="text-align: left" class="card-header card-header-success card-header-icon">

                            <i class="material-icons">
                                <img style="margin-top: -15px;margin-bottom: 0px;"
                                     src="{{asset('web-assets/images/completed.png')}}">
                            </i>
                        </div>
                        <div style="display: block;">
                            <div class="stats">

                                <div style="font-size: 30px;padding: 30px">
                                    <span style="font-size: 26px" class="card-title">Completed Complaints</span>
                                    <span style="float: right;font-size: 26px">{{\App\Complaint::where('status',2)->count()}}</span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End Card Issue tabs--}}


            </div>
            {{--Graph tabs--}}
            {{--<div class="row">--}}
            {{--<div class="col-md-4">--}}
            {{--<div class="card card-chart">--}}
            {{--<div class="card-header card-header-success">--}}
            {{--<div class="ct-chart" id="dailySalesChart"></div>--}}
            {{--</div>--}}
            {{--<div class="card-body">--}}
            {{--<h4 class="card-title">Daily Sales</h4>--}}
            {{--<p class="card-category">--}}
            {{--<span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>--}}
            {{--</div>--}}
            {{--<div class="card-footer">--}}
            {{--<div class="stats">--}}
            {{--<i class="material-icons">access_time</i> updated 4 minutes ago--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4">--}}
            {{--<div class="card card-chart">--}}
            {{--<div class="card-header card-header-warning">--}}
            {{--<div class="ct-chart" id="websiteViewsChart"></div>--}}
            {{--</div>--}}
            {{--<div class="card-body">--}}
            {{--<h4 class="card-title">Email Subscriptions</h4>--}}
            {{--<p class="card-category">Last Campaign Performance</p>--}}
            {{--</div>--}}
            {{--<div class="card-footer">--}}
            {{--<div class="stats">--}}
            {{--<i class="material-icons">access_time</i> campaign sent 2 days ago--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4">--}}
            {{--<div class="card card-chart">--}}
            {{--<div class="card-header card-header-danger">--}}
            {{--<div class="ct-chart" id="completedTasksChart"></div>--}}
            {{--</div>--}}
            {{--<div class="card-body">--}}
            {{--<h4 class="card-title">Completed Tasks</h4>--}}
            {{--<p class="card-category">Last Campaign Performance</p>--}}
            {{--</div>--}}
            {{--<div class="card-footer">--}}
            {{--<div class="stats">--}}
            {{--<i class="material-icons">access_time</i> campaign sent 2 days ago--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--End Grpah tabs--}}

        </div>
    </div>

    {{--<footer class="footer">--}}
    {{--<div class="container-fluid">--}}
    {{--<nav class="float-left">--}}
    {{--<ul>--}}
    {{--<li>--}}
    {{--<a href="https://www.creative-tim.com">--}}
    {{--Creative Tim--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="https://creative-tim.com/presentation">--}}
    {{--About Us--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="http://blog.creative-tim.com">--}}
    {{--Blog--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="https://www.creative-tim.com/license">--}}
    {{--Licenses--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</nav>--}}
    {{--<div class="copyright float-right">--}}
    {{--&copy;--}}
    {{--<script>--}}
    {{--document.write(new Date().getFullYear())--}}
    {{--</script>, made with <i class="material-icons">favorite</i> by--}}
    {{--<a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</footer>--}}
@endsection

@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    {{--<script src="{{asset('web-assets/js/core/jquery2.min.js')}}"></script>--}}

@endsection