@extends('admin.layouts.app')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">

            <div class="page-content">


                <div class="page-header">
                    {{--<a href="{{asset(route('excel-export'))}}" class="btn btn-primary btn-sm pull-right">Create Excel</a>--}}
                    {{--<button id="print_button"  class="btn btn-primary btn-sm pull-right">Create Print</button>--}}
                    <h1>
                        Members
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Register Members
                        </small>
                    </h1>

                </div><!-- /.page-header -->

                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    @if(Session::has('message'))
                                        <div class="alert alert-danger"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em></div>
                                    @endif
                                <table id="printableTable" class="table  table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th  class="center">
                                            No
                                        </th>
                                        <th  class="center">Form #</th>
                                        <th  class="center">Img</th>
                                        <th  class="center">Name</th>
                                        <th  class="center">Father Name</th>
                                        <th  class="center">Barcode</th>
                                        <th  class="center">CNIC</th>
                                        {{--<th  class="center">Card Status</th>--}}

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($consumers as $key=> $consumer  )
                                        <tr>


                                            <td class="center">
                                                {{$key+1}}
                                            </td>
                                            <td class="center">{{$consumer->form_no}}</td>
                                            <td class="center">
                                                @if(isset($consumer->thumbnail))
                                                    {{--{{ $consumer->thumbnail}}--}}
                                                    <img src="{{ $consumer->thumbnail }}"
                                                         width="50px" height="50px">
                                                @else
                                                    <strong>{{'No Img'}}</strong>
                                                @endif
                                            </td>
                                            <td class="center">{{$consumer->name}}</td>
                                            <td class="center">{{$consumer->father_name}}</td>
                                            <td class="center">{{$consumer->barcode}}</td>
                                            <td class="center">{{$consumer->cnic}}</td>
                                            {{--<td class="center">--}}
                                                {{--@if($consumer->card_issued ==1)--}}
                                                    {{--<strong style="color: green">{{'Card Issue'}}</strong>--}}
                                                    {{--@else--}}
                                                {{--<strong style="color: red">{{'No Card Issue'}}</strong>--}}
                                                    {{--@endif--}}
                                            {{--</td>--}}

                                            {{--<td class="center">{{$consumer->address}}</td>--}}
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                                </div>
                            </div><!-- /.span -->
                        </div><!-- /.row -->

                        {{--<div class="hr hr-18 dotted hr-double"></div>--}}

                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div>
@endsection
@section('script')
    <script>

        $(document).ready(function() {

        } );

//        function printDiv() {
//            window.frames["print_frame"].document.body.innerHTML = document.getElementById("").innerHTML;
//            window.frames["print_frame"].window.focus();
//            window.frames["print_frame"].window.print();
//        }
//
        $('#print_button').on('click',function(){
            alert('ok' );
            window.frames["print_frame"].document.body.innerHTML = document.getElementById("printableTable").innerHTML;
            window.frames["print_frame"].window.print();
        });

        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-danger').fadeOut('slow');
            }, 3000);
        });
    </script>
@endsection