@extends('admin.layouts.app')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="{{route('dashboard')}}">Home</a>
                    </li>


                    <li>
                        <a href="{{route('reports')}}">Reports</a>
                    </li>
                </ul><!-- /.breadcrumb -->


            </div>
            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    {{--<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">--}}
                    {{--<i class="ace-icon fa fa-cog bigger-130"></i>--}}
                    {{--</div>--}}

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-navbar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-sidebar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-breadcrumbs" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-add-container" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->
                <div class="page-header">
                    {{--onchange="location = this.value;"--}}


                    <div class="row">
                        <div class="col-md-3">
                            <h1>
                                Welcome to Report Dashboard
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    {{--Static &amp; Dynamic Tables--}}
                                </small>
                            </h1>
                        </div>


                        <div class="col-md-4 col-md-offset-1">
                            <select style="color: #438EB9;
                                                font-size: 20px;
                                                padding: 8px;
                                                font-family: inherit;
                                                height: 44px;
                                                border: solid 2px #ccc;
                                                border: 1px solid #6FB3E0;
                                                border-radius: 15px;" class="form-control" name="forma"
                                    onchange="window.location.href = '/report/'+this.value">
                                <option hidden value="" href="">Select Plant</option>
                                @foreach(@$plants as $plant)
                                    <option value="{{(@$plant->id)}}" href=""
                                            @if(request()->url() == url('report/'.$plant->id))selected @endif>{{@$plant->name}}</option>
                                @endforeach
                            </select>

                        </div>

                    </div>

                </div><!-- /.page-header -->
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div id="page-wrapper">

                            <!-- /.row -->
                            <div class="row">


                                <div class="col-lg-2 col-md-6">
                                    <div style="border-color:#3aaca8;" class="panel panel-primary">
                                        <div style="background-color:#3aaca8 !important;height: 250px;border-color:#3aaca8;"
                                             class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i class="fa fa-user fa-5x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    {{--<div class="huge">{{ \App\Lecture::where('role', '=' , 1)->count() }}</div>--}}
                                                    <div> Water Plants</div>
                                                    <div>
                                                        {{\App\Plant::all()->count()}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: cursive;
                                                        margin-top:0px;">{{\App\Plant::all()->count()}}</div>
                                            <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: cursive;">Total Plants
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-2 col-md-6">
                                    <div class="panel panel-primary">
                                        <div style="height: 250px;
                                                    background-color: #263c87;
                                                    border-color: #263c87;" class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i class="fa fa-user fa-5x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    @if(!isset($users))
                                                        <div>Total Maintainers</div>
                                                    @else
                                                        <div> Maintainers</div>
                                                    @endif

                                                    @if(!isset($users))
                                                        {{\App\User::where('role',0)->count()}}
                                                    @endif
                                                    <div>{{@$users}}</div>
                                                </div>
                                            </div>

                                            @if(!isset($users))
                                                <div style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: cursive;
                                                        margin-top:0px;">{{\App\User::where('role',0)->count()}}</div>
                                            @else
                                                <div style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: cursive;
                                                        margin-top:0px;">{{@$users}}</div>
                                            @endif
                                            @if(!isset($users))
                                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: cursive;">Total Maintainers
                                                </div>
                                            @else
                                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: cursive;"> Maintainers
                                                </div>
                                            @endif

                                        </div>

                                    </div>
                                </div>


                                <div class="col-lg-2 col-md-6">
                                    <div class="panel panel-warning">
                                        <div style="color: #AB6D0A;
                                                background-color: wheat;
                                                border-color: #faebcc; height: 250px;" class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i class="fa fa-user fa-5x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    @if(!isset($member_total))
                                                        <div>Total Members</div>
                                                    @else
                                                        <div> Members</div>
                                                    @endif
                                                    @if(!isset($member_total))
                                                        <div>{{\App\Consumer::all()->count()}}</div>
                                                    @else
                                                        <div>{{@$member_total}}</div>
                                                    @endif

                                                </div>
                                            </div>

                                            @if(!isset($member_total))
                                                <div style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: cursive;
                                                        margin-top:0px;">{{\App\Consumer::all()->count()}}</div>
                                            @else
                                                <div id="member_register" style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: cursive;
                                                        margin-top:0px;">{{$member_total}}</div>
                                            @endif
                                            @if(!isset($member_total))
                                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: cursive;">Total Members
                                                </div>
                                            @else
                                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: cursive;"> Members
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-2 col-md-6">
                                    <div class="panel panel-success">
                                        <div style="height: 250px;" class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i class="fa fa-user fa-5x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    @if(!isset($user_card_issue))
                                                        <div>Total Consumers Card issued</div>
                                                    @else
                                                        <div> Consumers Card issued</div>
                                                    @endif

                                                    @if(!isset($user_card_issue))
                                                        <div>{{\App\Consumer::where('card_issued',1)->count()}}</div>
                                                    @else
                                                        <div>{{@$user_card_issue}}</div>
                                                    @endif


                                                </div>
                                            </div>

                                            @if(!isset($user_card_issue))
                                                <div style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: cursive;
                                                        margin-top:0px;">{{\App\Consumer::where('card_issued',1)->count()}}</div>
                                            @else
                                                <div id="user_card_issue" style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: cursive;
                                                        margin-top:0px;">{{$user_card_issue}}</div>
                                            @endif
                                            @if(!isset($user_card_issue))
                                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: cursive;">Total Consumers
                                                </div>
                                            @else
                                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: cursive;"> Consumers
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-2 col-md-6">
                                    <div class="panel panel-danger">
                                        <div style="height:250px;" class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i class="fa fa-user fa-5x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    @if(!isset($no_card_issue))
                                                        <div>Total Register Members</div>
                                                        <div>NO Card issue</div>
                                                    @else
                                                        <div>Register Members</div>
                                                        <div>NO Card issue</div>
                                                    @endif

                                                    @if(!isset($no_card_issue))
                                                        <div>{{\App\Consumer::where('card_issued',0)->count()}}</div>
                                                    @else
                                                        <div>{{@$no_card_issue}}</div>
                                                    @endif

                                                </div>
                                            </div>

                                            @if(!isset($no_card_issue))
                                                <div style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: cursive;
                                                        margin-top:0px;">{{\App\Consumer::where('card_issued',0)->count()}}</div>
                                            @else
                                                <div id="no_card_issue" style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: cursive;
                                                        margin-top:0px;">{{$no_card_issue}}</div>
                                            @endif
                                            @if(!isset($no_card_issue))
                                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: cursive;">Total Registers
                                                </div>
                                            @else
                                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: cursive;">Registers
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-2 col-md-6">
                                    <div style="border-color:#D15B47!important" class="panel panel-primary">
                                        <div style="height: 250px;background-color: #D15B47!important; border-color:#D15B47!important"
                                             class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <i class="fa fa-user fa-5x"></i>
                                                </div>
                                                <div class="col-xs-9 text-right">
                                                    {{--<div class="huge">{{ \App\Lecture::where('role', '=' , 1)->count() }}</div>--}}
                                                    <div> Water Consummation</div>
                                                    <div>
                                                        @if(isset($total_consumption))
                                                            {{@$total_consumption}}
                                                        @else
                                                            {{DB::table('consumptions')->pluck('total_consumer_serve')->sum()}}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            @if(!isset($total_consumption))
                                                <div style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: cursive;
                                                        margin-top:0px;">{{DB::table('consumptions')->pluck('total_consumer_serve')->sum()}}</div>
                                            @else
                                                <div id="consum" style="text-align: center;
                                                        font-size: 65px;
                                                        font-family: cursive;
                                                        margin-top:0px;">{{@$total_consumption}}</div>
                                            @endif
                                            @if(!isset($total_consumption))
                                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: cursive;">Total Consummation
                                                </div>
                                            @else
                                                <div style="text-align: center;
                                                        font-size: 20px;
                                                        font-family: cursive;">Consummation
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4 class="widget-title">Consumer List</h4>
                                            <span class="widget-toolbar">
                            <a href="#" data-action="settings">
                                <i class="ace-icon fa fa-cog"></i>
                            </a>

                            <a href="#" data-action="reload">
                                <i class="ace-icon fa fa-refresh"></i>
                            </a>

                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>

                            <a href="#" data-action="close">
                                <i class="ace-icon fa fa-times"></i>
                            </a>
                        </span>
                                        </div>

                                        <div class="widget-body">
                                            <div style="height:330px;" class="widget-main">
                                                <div class="table-responsive">
                                                    <table id="printableTable"
                                                           class="table  table-bordered table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th class="center">
                                                                No
                                                            </th>
                                                            <th class="center">Form #</th>
                                                            <th class="center">Img</th>
                                                            <th class="center">Name</th>
                                                            <th class="center">Father Name</th>
                                                            <th class="center">Barcode</th>
                                                            <th class="center">CNIC</th>
                                                            {{--<th  class="center">Card Status</th>--}}

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($total_consumers as $key=> $total_consumer  )
                                                            <tr>


                                                                <td class="center">
                                                                    {{($total_consumers->currentpage() - 1) * $total_consumers->perpage() + 1 + $key}}
                                                                </td>
                                                                <td class="center">{{$total_consumer->form_no}}</td>
                                                                <td class="center">
                                                                    @if(isset($total_consumer->thumbnail))
                                                                        {{--{{ $total_consumer->thumbnail}}--}}
                                                                        <img src="{{ $total_consumer->thumbnail }}"
                                                                             width="50px" height="50px">
                                                                    @else
                                                                        <strong>{{'No Img'}}</strong>
                                                                    @endif
                                                                </td>
                                                                <td class="center">{{$total_consumer->name}}</td>
                                                                <td class="center">{{$total_consumer->father_name}}</td>
                                                                <td class="center">{{$total_consumer->barcode}}</td>
                                                                <td class="center">{{$total_consumer->cnic}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>

                                                    </table>
                                                    <div class="text-left">{{ $total_consumers->links() }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4 class="widget-title">Date Filter</h4>
                                            <span class="widget-toolbar">
                            <a href="#" data-action="settings">
                                <i class="ace-icon fa fa-cog"></i>
                            </a>

                            <a href="#" data-action="reload">
                                <i class="ace-icon fa fa-refresh"></i>
                            </a>

                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>

                            <a href="#" data-action="close">
                                <i class="ace-icon fa fa-times"></i>
                            </a>
                        </span>
                                        </div>

                                        <div class="widget-body">
                                            <div class="widget-main">

                                                <form method="POST" action="{{route('report-get')}}">
                                                    {{--@csrf--}}
                                                    {{ csrf_field() }}
                                                    <label for="id-date-picker-1">Month Picker</label>

                                                    <div class="row">
                                                        <div class="col-xs-8 col-sm-11">
                                                            <div class="input-group">
                                                                <input readonly placeholder="Select Month"
                                                                       class="filter_by_month form-control"
                                                                       id="myDatepicker" value="" type="text"/>
                                                                <span class="input-group-addon">
																		<i class="fa fa-calendar bigger-110"></i>
																	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr/>

                                                    <label for="id-date-picker-1">Date Picker</label>

                                                    <div class="row">
                                                        <div class="col-xs-8 col-sm-11">
                                                            <div class="input-group">
                                                                <input readonly placeholder="Select Date"
                                                                       class="form-control date-picker filter_by_date"
                                                                       id="id-date-picker-1" value="" type="text"
                                                                       data-date-format="yyyy-mm-dd"/>
                                                                <span class="input-group-addon">
																		<i class="fa fa-calendar bigger-110"></i>
																	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="space space-8"></div>
                                                    <label>Date Range Picker</label>

                                                    <div class="row">
                                                        <div class="col-xs-8 col-sm-11">
                                                            <div class="input-daterange input-group"
                                                                 data-date-format="yyyy-mm-dd">
                                                                <input readonly type="text"
                                                                       class="filter_by_date_range1 input-sm form-control"
                                                                       value=""/>
                                                                <span class="input-group-addon">
																		<i class="fa fa-exchange"></i>
																	</span>

                                                                <input readonly type="text"
                                                                       class="filter_by_date_range2 input-sm form-control"
                                                                       value=""/>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <hr/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div>

        </div>

    </div>
@endsection

@section('script')
    <script>
        $('#myDatepicker').datepicker({
            format: "yyyy/mm",
            startView: "year",
            minViewMode: "months"
        });
        $('.filter_by_month').change(function () {
            var abc = $('.filter_by_month').val();
            var url = document.URL;
            var stuff = url.split('/');
            var id = stuff[stuff.length - 1];
//            alert(id);
            if (abc) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                $.ajax({
                    url: '/year-filter?id=' + id,
                    type: 'POST',
                    data: {data: abc},
                    dataType: 'JSON',


                    success: function (Result) {
                        $('#member_register').html(Result.member_registerd);
                        $('#user_card_issue').html(Result.card_issued);
                        $('#no_card_issue').html(Result.card_not_issued);
                        $('#consum').html(Result.consum);
                    },
                    error: function (Result) {

                    }
                });
            }


        });

        $('.filter_by_date').change(function () {
            var abc = $('.filter_by_date').val();
            var url = document.URL;
            var stuff = url.split('/');
            var id = stuff[stuff.length - 1];
//            alert(id);
            if (abc) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                $.ajax({
                    url: '/date-filter?id=' + id,
                    type: 'POST',
                    data: {data: abc},
                    dataType: 'JSON',


                    success: function (Result) {
                        $('#member_register').html(Result.member_registerd);
                        $('#user_card_issue').html(Result.card_issued);
                        $('#no_card_issue').html(Result.card_not_issued);
                        $('#consum').html(Result.consum);
                    },
                    error: function (Result) {

                    }
                });
            }


        });

        $('.filter_by_date_range2').change(function () {
            var start = $('.filter_by_date_range1').val();
            var end = $('.filter_by_date_range2').val();
//             alert('ok');
            var url = document.URL;
            var stuff = url.split('/');
            var id = stuff[stuff.length - 1];
//            alert(id);
            if (start && end) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                $.ajax({
                    url: '/range-filter?id=' + id,
                    type: 'POST',
                    data: {data: start, end},
                    dataType: 'JSON',


                    success: function (Result) {
                        $('#member_register').html(Result.member_registerd);
                        $('#user_card_issue').html(Result.card_issued);
                        $('#no_card_issue').html(Result.card_not_issued);
                        $('#consum').html(Result.consum);
                    },
                    error: function (Result) {

                    }
                });
            }


        });
    </script>
@endsection