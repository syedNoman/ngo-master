@extends('admin.layouts.app2')
@section('content')

    <style>
        .card-title{
            margin: 0;
            text-transform: uppercase;
            color: darkgrey !important;
            font-size: 15px !important;
            font-weight: 400;
            margin-right: 10px;
        }
    </style>
    <div style="background: url('{{asset('background.png')}}')" class="content">
        <div class="container-fluid">
            <div style="margin-bottom:25px" class="row">
                <div class="col-md-6 offset-3">
                    <select style="font-size: 18px;
                                    padding: 8px;
                                    font-family: inherit;
                                    height: 44px;
                                    border: solid 1px #ccc;
                                    box-shadow: 2px 2px 2px #ccc;
                                    background-color: white;" class="form-control" name="forma"
                            onchange="window.location.href = '/report/'+this.value">
                        <option hidden value="" href="">Select Plant</option>
                        @foreach(@$plants as $plant)
                            <option value="{{(@$plant->id)}}" href=""
                                    @if(request()->url() == url('report/'.$plant->id))selected @endif>{{@$plant->name}}</option>
                        @endforeach
                    </select>

                </div>
            </div>
            <div class="row">


                {{--Plant tabs--}}
                @if(Auth::user()->role==1)
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-warning card-header-icon">
                                <div style="background: linear-gradient(118deg, #4facfe, #00f2fe) !important;box-shadow: 1px 4px 5px -1px #ccc;"
                                     class="card-icon">
                                    <i class="material-icons">
                                        <img style="margin-top: -15px;margin-bottom: 0px;margin-top: -15px;
                                            margin-bottom: 0px;height: 30px;"
                                             src="{{asset('web-assets/images/plant-icon.png')}}">
                                    </i>
                                </div>
                                <h3 class="card-title">Plantse</h3>
                            </div>
                            <div style="display: block;text-align: right;" class="card-footer">
                                <div class="stats">
                                    <div style="font-size: 30px;">{{'1'}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-warning card-header-icon">
                                <div style="background: linear-gradient(118deg, #4facfe, #00f2fe) !important; box-shadow: 1px 4px 5px -1px #ccc;"
                                     class="card-icon">
                                    <i class="material-icons">
                                        <img style="margin-top: -15px;margin-bottom: 0px;margin-top: -15px;
                                            margin-bottom: 0px;height: 30px;"
                                             src="{{asset('web-assets/images/today-consumption-icon.png')}}">
                                    </i>
                                </div>
                                <h3 class="card-title">Today Consumptions</h3>
                            </div>
                            <div style="display: block;text-align: right;" class="card-footer">
                                <div class="stats">
                                    <div style="font-size: 30px;">
                                        {{DB::table('consumptions')->where('plant_id',Auth::user()->plant_id)->pluck('today_total_consumer_serve')->sum()}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                {{--End Plant tabs--}}

                {{--Maintaniner tabs--}}
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-success card-header-icon">
                            <div style="background: linear-gradient(118deg, #fa709a, #fee140) !important; box-shadow: 1px 4px 5px -1px #ccc;"
                                 class="card-icon">
                                <i class="material-icons">
                                    <img style="margin-top: -15px;margin-bottom: 0px;margin-top: -15px;
                                            margin-bottom: 0px;height: 30px;"
                                         src="{{asset('web-assets/images/maintainer-icon.png')}}">
                                </i>
                            </div>
                            @if(!isset($users))
                                <h3 class="card-title">Maintainers</h3>
                                @else
                                <h3 class="card-title">Maintainer</h3>
                            @endif
                        </div>
                        <div style="display: block;text-align: right;" class="card-footer">
                            <div class="stats">
                                <div style="font-size: 30px;">
                                    @if(Auth::user()->role==1)
                                        @if(!isset($users))
                                            {{\App\User::where('role',0)->count()}}
                                            @else
                                            {{@$users}}
                                        @endif
                                    @else
                                        {{\App\User::where('role',0)->where('plant_id',Auth::user()->plant_id)->count()}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End Maintaniner tabs--}}

                {{--Consumer Issue tabs--}}
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-danger card-header-icon">
                            <div style="background: linear-gradient(118deg, #667eea, #764ba2) !important; box-shadow: 1px 4px 5px -1px #ccc;"
                                 class="card-icon">
                                <i class="material-icons">
                                    <img style="margin-top: -15px;margin-bottom: 0px;margin-top: -15px;
                                            margin-bottom: 0px;height: 30px;"
                                         src="{{asset('web-assets/images/consumers-icon.png')}}">
                                </i>
                            </div>

                            @if(!isset($member_total))
                                <h3 class="card-title">Consumers</h3>
                            @else
                                <h3 class="card-title">Consumer</h3>
                            @endif
                        </div>
                        <div style="display: block;text-align: right;" class="card-footer">
                            <div class="stats">
                                <div style="font-size: 30px;">
                                    @if(Auth::user()->role == 1)
                                        <div id="member_register">{{@$member_total}}</div>
                                        @if(!isset($member_total))
                                            <div>{{\App\Consumer::all()->count()}}</div>
                                        @endif
                                    @else
                                        <div>{{\App\Consumer::where('plant_id',Auth::user()->plant_id)->count()}}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End Consumer Issue tabs--}}

                {{--Card Issue tabs--}}
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-info card-header-icon">
                            <div style="background: linear-gradient(118deg, #43e97b, #38f9d7) !important; box-shadow: 1px 4px 5px -1px #ccc;"
                                 class="card-icon">
                                <i class="material-icons">
                                    <img style="margin-top: -15px;margin-bottom: 0px;margin-top: -15px;
                                            margin-bottom: 0px;height: 30px;"
                                         src="{{asset('web-assets/images/card issued-icon.png')}}">
                                </i>
                            </div>
                            <h3 class="card-title">Card Issued</h3>
                        </div>
                        <div style="display: block;text-align: right;" class="card-footer">
                            <div class="stats">
                                <div style="font-size: 30px;">
                                    @if(Auth::user()->role == 1)
                                        <div id="user_card_issue">{{@$user_card_issue}}</div>
                                        @if(!isset($user_card_issue))
                                            <div>{{\App\Consumer::where('card_issued',1)->count()}}</div>
                                        @endif
                                    @else
                                        <div>{{\App\Consumer::where('plant_id',Auth::user()->plant_id)->where('card_issued',1)->count()}}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End Card Issue tabs--}}

                {{--No Card Issue tabs--}}
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-info card-header-icon">
                            <div style="background: linear-gradient(118deg, #16a085, #f4d03f) !important; box-shadow: 1px 4px 5px -1px #ccc;"
                                 class="card-icon">
                                <i class="material-icons">
                                    <img style="margin-top: -15px;margin-bottom: 0px;margin-top: -15px;
                                            margin-bottom: 0px;height: 30px;"
                                         src="{{asset('web-assets/images/nocard-icon.png')}}">
                                </i>
                            </div>
                            <h3 class="card-title">Card Not Issued</h3>
                        </div>
                        <div style="display: block;text-align: right;" class="card-footer">
                            <div class="stats">
                                <div style="font-size: 30px;">
                                    @if(Auth::user()->role == 1)
                                        <div id="no_card_issue">{{@$no_card_issue}}</div>
                                        @if(!isset($no_card_issue))
                                            <div>{{\App\Consumer::where('card_issued',0)->count()}}</div>
                                        @endif

                                    @else
                                        <div>{{\App\Consumer::where('plant_id',Auth::user()->plant_id)->where('card_issued',0)->count()}}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End No Card Issue tabs--}}

                {{--Total Consumaption tabs--}}
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-info card-header-icon">
                            <div style="background: linear-gradient(118deg, #d558c8, #24d292) !important; box-shadow: 1px 4px 5px -1px #ccc;"
                                 class="card-icon">
                                <i class="material-icons">
                                    <img style="margin-top: -15px;margin-bottom: 0px;margin-top: -15px;
                                            margin-bottom: 0px;height: 30px;"
                                         src="{{asset('web-assets/images/total-consumtion.png')}}">
                                </i>
                            </div>
                            <h3 class="card-title">Total Consumptions</h3>
                        </div>
                        <div style="display: block;text-align: right;" class="card-footer">
                            <div class="stats">
                                <div style="font-size: 30px;">
                                    @if(Auth::user()->role == 1)
                                        @if(isset($total_consumption))
                                            <div id="consum">{{@$total_consumption}}</div>
                                        @else
                                            <div>{{DB::table('consumptions')->pluck('total_consumer_serve')->sum()}}</div>
                                        @endif


                                    @else
                                        <div>{{DB::table('consumptions')->where('plant_id',Auth::user()->plant_id)->pluck('total_consumer_serve')->sum()}}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--End Consumaption tabs--}}


            </div>


            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">

                                <table id="printableTable" class="table  table-bordered table-hover">
                                    <thead class="text-primary">
                                    <tr>
                                        <th class="center">
                                            No
                                        </th>
                                        <th class="center">Form #</th>
                                        <th class="center">Img</th>
                                        <th class="center">Name</th>
                                        <th class="center">Father Name</th>
                                        <th class="center">Barcode</th>
                                        <th class="center">CNIC</th>
                                        {{--<th  class="center">Card Status</th>--}}

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($total_consumers as $key=> $total_consumer  )
                                        <tr>


                                            <td class="center">
                                                {{($total_consumers->currentpage() - 1) * $total_consumers->perpage() + 1 + $key}}
                                            </td>
                                            <td class="center">{{$total_consumer->form_no}}</td>
                                            <td class="center">
                                                @if(isset($total_consumer->thumbnail))
                                                    {{--{{ $total_consumer->thumbnail}}--}}
                                                    <img src="{{ $total_consumer->thumbnail }}"
                                                         width="50px" height="50px">
                                                @else
                                                    <strong>{{'No Img'}}</strong>
                                                @endif
                                            </td>
                                            <td class="center">{{$total_consumer->name}}</td>
                                            <td class="center">{{$total_consumer->father_name}}</td>
                                            <td class="center">{{$total_consumer->barcode}}</td>
                                            <td class="center">{{$total_consumer->cnic}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                                <div class="text-left">{{ $total_consumers->links() }}</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">

                    <div class="card">
                        <div class="card-body">
                            <div class="widget-main">

                                <form method="POST" action="{{route('report-get')}}">
                                    {{--@csrf--}}
                                    {{ csrf_field() }}
                                    <div style="    color: #07cdff !important;
    font-size: 16px;
    font-weight: 500;
    margin-bottom: 25PX;" class="text-primary">Date Filter
                                    </div>
                                    <label for="id-date-picker-1">Month Picker</label>

                                    <div class="row">
                                        <div class="col-xs-8 col-sm-11">
                                            <div class="input-group">
                                                {{--<input placeholder="Select Month"  class="filter_by_month form-control"  value="" type="month" name="start_date"/>--}}
                                                <input placeholder="Select Month" class="form-control date-picker filter_by_month" id="id-date-picker-1" value="" type="month" name="start_date"/>
                                            </div>
                                        </div>
                                    </div>


                                    <label style="margin-top: 35px" for="id-date-picker-1">Date Picker</label>

                                    <div class="row">
                                        <div class="col-xs-8 col-sm-11">
                                            <div class="input-group">
                                                <input placeholder="Select Date" class="form-control date-picker filter_by_date" id="id-date-picker-1" value="" type="date" name="start_date"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="margin-bottom: 28px" class="space space-8"></div>
                                    <label style="margin-top: 35px">Date Range Picker</label>

                                    <div class="row">
                                        <div class="input-daterange input-group" data-date-format="yyyy-mm-dd">
                                            <input  type="date" class="filter_by_date_range1 input-sm form-control" value=""  />
                                            <span class="input-group-addon">
																		<i class="fa fa-exchange"></i>
																	</span>

                                            <input  type="date" class="filter_by_date_range2 input-sm form-control" value=""  />
                                        </div>
                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
    </div>
@endsection

@section('script')
    <script>



        $('.filter_by_month').change(function (){
            var abc=$('.filter_by_month').val();
            var url = document.URL;
            var stuff = url.split('/');
            var id = stuff[stuff.length-1];
//            alert(id);
            if(abc){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                $.ajax({
                    url:'/year-filter?id='+id,
                    type:'POST',
                    data:{data:abc},
                    dataType:'JSON',


                    success:function (Result) {
                        $('#member_register').html(Result.member_registerd);
                        $('#user_card_issue').html(Result.card_issued);
                        $('#no_card_issue').html(Result.card_not_issued);
                        $('#consum').html(Result.consum);
                    },
                    error:function (Result) {

                    }
                });
            }


        });

        $('.filter_by_date').change(function (){
            var abc=$('.filter_by_date').val();
            var url = document.URL;
            var stuff = url.split('/');
            var id = stuff[stuff.length-1];
//            alert(id);
            if(abc){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                $.ajax({
                    url:'/date-filter?id='+id,
                    type:'POST',
                    data:{data:abc},
                    dataType:'JSON',


                    success:function (Result) {
                        $('#member_register').html(Result.member_registerd);
                        $('#user_card_issue').html(Result.card_issued);
                        $('#no_card_issue').html(Result.card_not_issued);
                        $('#consum').html(Result.consum);
                    },
                    error:function (Result) {

                    }
                });
            }


        });

        $('.filter_by_date_range2').click(function (){
//            alert('ok');

            var start=$('.filter_by_date_range1').val();
            var end=$('.filter_by_date_range2').val();
            var url = document.URL;
            var stuff = url.split('/');
            var id = stuff[stuff.length-1];


            if(start && end) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url:'/range-filter?id='+id,
                    type:'POST',
                    data:{data:start,end},
                    dataType:'JSON',


                    success: function (Result) {
                        $('#member_register').html(Result.member_registerd);
                        $('#user_card_issue').html(Result.card_issued);
                        $('#no_card_issue').html(Result.card_not_issued);
                        $('#consum').html(Result.consum);
                    },
                    error: function (Result) {

                    }
                });
            }

        });





    </script>
@endsection

