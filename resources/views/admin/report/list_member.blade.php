@extends('admin.layouts.app2')
@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Consumers</h4>

                            <p class="card-category"> Card not issued that's all Consumers</p>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">

                                    <thead>
                                    <tr>
                                        <th  class="center">
                                            No
                                        </th>
                                        <th  class="center">Form #</th>
                                        <th  class="center">Img</th>
                                        <th  class="center">Name</th>
                                        <th  class="center">Father Name</th>
                                        <th  class="center">Barcode</th>
                                        <th  class="center">CNIC</th>
                                        {{--<th  class="center">Card Status</th>--}}

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($consumers as $key=> $consumer  )
                                        <tr>


                                            <td class="center">
                                                {{$key+1}}
                                            </td>
                                            <td class="center">{{$consumer->form_no}}</td>
                                            <td class="center">
                                                @if(isset($consumer->thumbnail))
                                                    {{--{{ $consumer->thumbnail}}--}}
                                                    <img src="{{ $consumer->thumbnail }}"
                                                         width="50px" height="50px">
                                                @else
                                                    <strong>{{'No Img'}}</strong>
                                                @endif
                                            </td>
                                            <td class="center">{{$consumer->name}}</td>
                                            <td class="center">{{$consumer->father_name}}</td>
                                            <td class="center">{{$consumer->barcode}}</td>
                                            <td class="center">{{$consumer->cnic}}</td>
                                            {{--<td class="center">--}}
                                            {{--@if($consumer->card_issued ==1)--}}
                                            {{--<strong style="color: green">{{'Card Issue'}}</strong>--}}
                                            {{--@else--}}
                                            {{--<strong style="color: red">{{'No Card Issue'}}</strong>--}}
                                            {{--@endif--}}
                                            {{--</td>--}}

                                            {{--<td class="center">{{$consumer->address}}</td>--}}
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                                {{--<div class="text-left">{{ $consumers->links() }}</div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--edit part--}}
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><h3>Edit Tehsil
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button></h3>
                    </h5>
                </div>
                <div id="edit-content">
                </div>
            </div>
        </div>
    </div>
    {{--delete modal part--}}
    <div class="modal fade" id="delete-tehsil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST" id="delete-form" action="/complaintn/{{@$complaint->id}}">
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                        <h3>Delete</h3>
                    </div>
                    <div class="modal-body">
                        <p> Are you sure to delete?</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" type="submit">Delete</button>
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

        $(document).ready(function() {

        } );

        //        function printDiv() {
        //            window.frames["print_frame"].document.body.innerHTML = document.getElementById("").innerHTML;
        //            window.frames["print_frame"].window.focus();
        //            window.frames["print_frame"].window.print();
        //        }
        //
        $('#print_button').on('click',function(){
            alert('ok' );
            window.frames["print_frame"].document.body.innerHTML = document.getElementById("printableTable").innerHTML;
            window.frames["print_frame"].window.print();
        });

        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-danger').fadeOut('slow');
            }, 3000);
        });
    </script>
@endsection
