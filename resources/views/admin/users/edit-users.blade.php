<form id="editfrom" method="POST" action="/user/{{ $user->id }}" enctype="multipart/form-data">
    <div class="modal-body">
        {{ csrf_field() }}


        <div class="form-group">
            {{--<lable for="exampleInputCat" class="control-label"><h4>Plant</h4></lable>--}}
            <select style="text-transform:capitalize; margin-top: 30px" class="form-control" id="nameid" name="plant_id" type="text"  required>
                <option hidden value="">Select Plant</option>
                @foreach($plants as $plant)
                    <option @if($plant->id == @$user->plant_id ) selected @endif value="{{$plant->id}}">{{$plant->code}}{{'--'}}{{$plant->name}}{{'-(village)-'}}{{$plant->village}}</option>
                @endforeach
            </select>
        </div>

        <div style="margin-top: 30px" class="form-group">
            <input style="text-transform:capitalize;" class="form-control" id="nameid" name="name" type="text" placeholder="Enter Name" value="{{$user->name}}" required>
        </div>

        <div style="margin-top: 30px" class="form-group">
            <input class="form-control" id="email" name="email" type="text" placeholder="Enter Email" required value="{{$user->email}}">
        </div>

        <div style="margin-top: 30px" class="form-group">
            <input id="password" type="password" class="form-control" placeholder="Enter Password" name="password" required value="{{$user->password}}">
        </div>

    </div>
    <div style="border: none" class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
</form>
