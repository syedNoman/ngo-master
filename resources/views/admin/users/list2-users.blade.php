
@extends('admin.layouts.app')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>

                    <li>
                        <a href="{{route('dashboard')}}">Dashboard</a>
                    </li>
                </ul><!-- /.breadcrumb -->

                <div class="nav-search" id="nav-search">
                    <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
                    </form>
                </div><!-- /.nav-search -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        Users
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Show All Users
                        </small>
                    </h1>

                </div><!-- /.page-header -->

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="card-header">
                            <button style="margin-bottom: 15px" class="btn-info btn-sm pull-right b" data-toggle="modal" data-target="#exampleModal">Add
                                Users
                            </button>
                        </div>
                        <div class="col-md-8 col-md-offset-2">
                                @if(Session::has('message'))
                                    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em></div>
                                @endif
                            @if(Session::has('message-delete'))
                                @if(Session::has('message-delete'))
                                    <div class="alert alert-danger"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message-delete') !!}</em></div>
                                @endif
                            @endif
                        </div>

                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">

                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <!-- PAGE CONTENT BEGINS -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel"><h3>Add
                                                                Users
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button></h3>
                                                        </h5>
                                                    </div>
                                                    <form id="form" method="POST" action="{{route('add-user')}}"
                                                          enctype="multipart/form-data">
                                                        <div class="modal-body">
                                                            {{ csrf_field() }}
                                                            <div class="form-group">
                                                                <lable for="exampleInputCat" class="control-label"><h4>Plant</h4></lable>
                                                                <select style="text-transform:capitalize;" class="form-control" id="nameid" name="plant_id" type="text"  required>
                                                                    <option hidden value="">Select Plant</option>
                                                                    @foreach($plants as $plant)
                                                                    <option value="{{$plant->id}}">{{$plant->code}} {{'--'}} {{$plant->name}} {{'(Village)'}}  {{$plant->village}}</option>
                                                                        @endforeach
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <lable for="exampleInputCat" class="control-label"><h4>Name</h4></lable>
                                                                <input style="text-transform:capitalize;" class="form-control" id="nameid" name="name" type="text" placeholder="Enter Name" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <lable for="email" class="control-label">
                                                                  <h4>Email</h4>
                                                                </lable>
                                                                <input  class="form-control" id="email" name="email"
                                                                       type="text" placeholder="Enter Email"
                                                                       required>
                                                            </div>

                                                            <div class="form-group">
                                                                <lable for="email" class="control-label">
                                                                    <h4>Password</h4>
                                                                </lable>
                                                                <input id="password" type="password" class="form-control" name="password" required>
                                                            </div>




                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                <table id="simple-table" class="table  table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="detail-col">NO#</th>
                                            <th>Plant Name</th>
                                            {{--<th>Plant Village</th>--}}
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>password</th>
                                            <th style="text-align:center" class="col-md-1">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $key => $user)

                                            <tr>
                                                <td style="text-align: center">
                                                    {{ ($users->currentpage() - 1) * $users->perpage() + 1 + $key }}

                                                </td>
                                                <td style="text-transform: capitalize">{{ $user->plant->code }}{{'--'}}{{ $user->plant->name }}</td>
                                                {{--<td style="text-transform: capitalize">{{ $user->plant->village }}</td>--}}
                                                <td style="text-transform: capitalize">{{ $user->name }}</td>
                                                <td style="">{{ @$user->email }}</td>
                                                <td style="text-transform: capitalize">{{ str_limit(@$user->password,10)   }}</td>
                                                <td style="text-align: center">
                                                    <div class="btn-group">
                                                        <button title="Update" class="btn btn-xs btn-success edit-btn"
                                                                data-toggle="modal"
                                                                data-action="/user/{{ $user->id }}"
                                                                data-target="#exampleModal2">
                                                            <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                        </button>

                                                        <button title="Delete" type="button" data-toggle="modal"
                                                                data-target="#delete-user"
                                                                data-action="/del-user/{{$user->id}}"
                                                                class="show-delete-modal btn btn-xs btn-danger">
                                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                        @endforeach
                                </table>
                                </div>
                                <div class="text-right">{{ $users->links() }}</div>

                            </div><!-- /.span -->
                        </div><!-- /.row -->
                    </div>
                </div>
            </div><!-- /.page-content -->
        </div>
    </div>
    {{--edit part--}}
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><h3>Edit Tehsil
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button></h3>
                    </h5>
                </div>
                <div id="edit-content">
                </div>
            </div>
        </div>
    </div>
    {{--delete modal part--}}
    <div class="modal fade" id="delete-user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST" id="delete-form" action="/del-user/{{@$user->id}}">
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                        <h3>Delete</h3>
                    </div>
                    <div class="modal-body">
                        <p> Are you sure to delete?</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" type="submit">Delete</button>
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-success').fadeOut('slow');
            }, 3000);
        });
        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-danger').fadeOut('slow');
            }, 3000);
        });
        $('.edit-btn').click(function (e) {
            var request = $.ajax({
                url: $(this).data('action'),
                type: "get",
                dataType: "html",
                cache: false,
                success:
                    function (result) {
                        $('#edit-content').html(result);

                    },
                error: function (result) {
                    alert('error this form');
                }
            });
        });

        // Summernote
        $('#summernote').summernote({
            height: 200
        });
    </script>
@endsection

