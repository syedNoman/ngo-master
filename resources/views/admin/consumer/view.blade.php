@extends('admin.layouts.app2')
@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <a href="#pablo">
                                @if(isset($user->thumbnail))
                                    <img class="img" style="border-radius: 5px;
														height: 165px;
														width: 100%;
														margin-top: 0px;" src="{{$user->thumbnail}}">
                                @else
                                    <img class="img" style="height: 122px" src="{{asset('no-thumbnail.jpeg')}}">
                                @endif
                            </a>
                        </div>
                        <div class="card-body">
                            <h4 class="card-title">{{$user->name}}</h4>
                            <h6 class="card-category text-gray">{{'S/O  '}}{{$user->father_name}}</h6>
                            <p class="card-description">{{$user->address}}</p>
                            <a style="text-transform: capitalize;font-size: 15px;" href="#pablo"
                               class="btn btn-primary btn-round">
                                <strong>{{@$consump->total_consumer_serve}}</strong>{{' '}}{{' Litres Water'}}
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Consumer Profile</h4>
                            <p class="card-category">Complete consumer profile</p>
                        </div>
                        <div class="card-body">
                            <form>
                                <div style="margin-top: 40px !important;" class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Name</label>
                                            <input style="height: 50px" type="text" class="form-control" disabled
                                                   value="{{$user->name}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Father Name</label>
                                            <input style="height: 50px" type="text" class="form-control" disabled
                                                   value="{{$user->father_name}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">CNIC Number</label>
                                            <input style="height: 50px" type="text" class="form-control" disabled
                                                   value="{{$user->cnic}}">
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 40px !important;" class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Mobile</label>
                                            <input style="height: 50px" type="text" class="form-control" disabled
                                                   value="{{$user->mobile}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">

                                            @if(isset($user->card_issue_date))
                                                <label class="bmd-label-floating">Card Issue Date</label>
                                                <input style="height: 50px" type="text" class="form-control" disabled
                                                       value="{{$user->card_issue_date}}">
                                            @else
                                                <label class="bmd-label-floating">Card Issue Date</label>
                                                <input style="height: 50px" type="text" class="form-control" disabled
                                                       value="{{'----'}}">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Village</label>
                                            <input style="height: 50px" type="text" class="form-control" disabled
                                                   value="{{$user->village}}">
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 40px !important;" class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Address</label>
                                            <textarea rows="3" style="height: 50px" type="text" class="form-control"
                                                      disabled>{{$user->address}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
