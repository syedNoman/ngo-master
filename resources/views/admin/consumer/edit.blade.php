@extends('admin.layouts.app2')
@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Consumers</h4>

                            <a style="background: linear-gradient(145deg,#ffffff,#ffffff);
                                color: #07cdff;
                                margin-top: -22px;" class="btn pull-right" href="{{route('consumer')}}">Show Consumer</a>
                            <p class="card-category"> Showing all the consumers</p>
                        </div>

                        <div style="box-shadow: 2px 2px 2px 2px #ccc;" class="col-md-12" class="card-body">

                            <form action="{{route('consumer-update',$user->id)}}" method="post" accept-charset="utf-8"
                                  enctype="multipart/form-data">
                                <div class="row">
                                    @csrf
                                    <div class="col-md-9">
                                        {{--<label style="margin-top:20px;" for="form_no"><h4>Form Number</h4></label>--}}
                                        <div class="col-md-6 col-xs-12">
                                            <input style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" placeholder="Enter Form Number" id="form_no"
                                                   type="text" class="form-control" name="form_no"
                                                   value="{{$user->form_no}}" required>
                                            @if ($errors->has('form_no'))
                                                <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('form_no') }}</strong>
                                    						</span>
                                            @endif
                                        </div>
                                        <div style="text-align: right">
                                            <div id="results">
                                                @if(isset($user))
                                                    <img style="height: 94px;margin-top: -37px; margin-right: 13px;  border-radius: 10%;"
                                                         type="file" name="" src="{{$user->thumbnail}}"/>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <div class="col-md-3 col-xs-6">
                                                {{--<label style="margin-top:20px;" for="email"><h4>District</h4></label>--}}
                                                <select style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" id="district" type="text"
                                                        class="form-control select" name="distric_id" required>
                                                    <option value="" hidden>Select Distric Name</option>
                                                    @foreach($districs as $distric)
                                                        <option @if($user->distric_id == $distric->id) selected
                                                                @endif style="text-transform: capitalize"
                                                                value="{{$distric->id}}">{{$distric->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-3 col-xs-6">
                                                {{--<label style="margin-top:20px;" for="email"><h4>Tehsil</h4></label>--}}
                                                <select style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" id="tehsil" type="text" class="form-control"
                                                        name="tehsil_id" required>
                                                    <option value="" hidden>Select Tehsil Name</option>
                                                    @foreach($tehsils as $tehsil)
                                                        <option @if($user->tehsil_id == $tehsil->id) selected
                                                                @endif style="text-transform: capitalize"
                                                                value="{{$tehsil->id}}">{{$tehsil->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-md-3 col-xs-6">
                                                {{--<label style="margin-top:20px;" for="union_council"><h4>Union Council</h4></label>--}}
                                                <select style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" id="concil" type="text" class="form-control"
                                                        name="union_council_id" required>
                                                    <option value="" hidden>Select Union Council Name</option>
                                                    @foreach($concils as $concil)
                                                        <option @if($user->union_council_id == $concil->id) selected
                                                                @endif style="text-transform: capitalize"
                                                                value="{{$concil->id}}">{{$concil->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-md-3 col-xs-6">
                                                {{--<label style="margin-top:20px;" for="village"><h4>Village</h4></label>--}}
                                                <input style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" placeholder="Enter Village Name" id="village"
                                                       type="text"
                                                       class="form-control{{ $errors->has('village') ? ' is-invalid' : '' }}"
                                                       name="village" value="{{$user->village}}" required>
                                                @if ($errors->has('village'))
                                                    <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('village') }}</strong>
                                    						</span>
                                                @endif
                                            </div>

                                        </div>


                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                {{--<label class="form-control-label"><h4>Name</h4></label>--}}
                                                <input style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" placeholder="Enter Name" id="name" type="text"
                                                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                       required name="name" value="{{$user->name}}">
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('name') }}</strong>
                                    						</span>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                {{--<label class="form-control-label"><h4>Father Name</h4></label>--}}
                                                <input style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" placeholder="Enter Father Name"
                                                       id="father_name" type="text"
                                                       class="form-control{{ $errors->has('father_name') ? ' is-invalid' : '' }}"
                                                       value="{{$user->father_name}}" required name="father_name">
                                                @if ($errors->has('father_name'))
                                                    <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('father_name') }}</strong>
                                    						</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                {{--<label class="form-control-label"><h4>CNIC Number</h4></label>--}}
                                                <input style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" placeholder="36203-12345678-7" id="cnic"
                                                       type="text"
                                                       class="input-mask-cnic form-control{{ $errors->has('cnic') ? ' is-invalid' : '' }}"
                                                       required name="cnic" value="{{$user->cnic}}">
                                                @if ($errors->has('cnic'))
                                                    <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('cnic') }}</strong>
                                    						</span>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                {{--<label class="form-control-label"><h4>Mobile Number</h4></label>--}}
                                                <input style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" placeholder="(92)-300-1234567" id="mobile"
                                                       type="text"
                                                       class="input-mask-phone form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}"
                                                       value="{{$user->mobile}}" required name="mobile">
                                                {{--<input class="form-control input-mask-phone" type="text" id="form-field-mask-2" />--}}
                                                @if ($errors->has('mobile'))
                                                    <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('mobile') }}</strong>
                                    						</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            {{--<label for="email" class="col-md-12 col-form-label text-md-right"><h4>Address</h4></label>--}}
                                            <div class="col-md-12">
														<textarea style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" placeholder="Enter Address" rows="3"
                                                                  id="address" type="number"
                                                                  class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}"
                                                                  name="address" required>{{$user->address}}</textarea>
                                                @if ($errors->has('address'))
                                                    <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('address') }}</strong>
                                    						</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4 col-xs-6">
                                                {{--<label  for="way_of_income"><h4>Way of Income</h4></label>--}}
                                                <input style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" placeholder="Enter Way of Income"
                                                       id="way_of_income" type="text"
                                                       class="form-control{{ $errors->has('way_of_income') ? ' is-invalid' : '' }}"
                                                       value="{{$user->way_of_income}}" name="way_of_income" required>
                                                @if ($errors->has('way_of_income'))
                                                    <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('way_of_income') }}</strong>
                                    						</span>
                                                @endif
                                            </div>
                                            <div class="col-md-4 col-xs-6">
                                                {{--<label  for="email"><h4>House Member</h4></label>--}}
                                                <input style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" placeholder="House Member" id="house_member"
                                                       type="number"
                                                       class="form-control{{ $errors->has('house_member') ? ' is-invalid' : '' }}"
                                                       value="{{$user->house_member}}" name="house_member" required>
                                                @if ($errors->has('house_member'))
                                                    <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('house_member') }}</strong>
                                    						</span>
                                                @endif
                                            </div>
                                            <div class="col-md-4 col-xs-6">
                                                {{--<label  for="male"><h4>Male</h4></label>--}}
                                                <input style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" placeholder="Male" id="male" type="number"
                                                       class="form-control{{ $errors->has('male') ? ' is-invalid' : '' }}"
                                                       name="male" value="{{$user->male}}" required>
                                                @if ($errors->has('male'))
                                                    <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('male') }}</strong>
                                    						</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-4 col-xs-6">
                                                <input style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" placeholder="Female" id="female" type="number"
                                                       class="form-control{{ $errors->has('female') ? ' is-invalid' : '' }}"
                                                       value="{{$user->female}}" name="female" required>
                                                @if ($errors->has('female'))
                                                    <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('female') }}</strong>
                                    						</span>
                                                @endif
                                            </div>
                                            <div class="col-md-4 col-xs-6">
                                                <input style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" placeholder="Less than 5 year Children"
                                                       id="less_five_year_child" type="number" required
                                                       class="form-control{{ $errors->has('less_five_year_child') ? ' is-invalid' : '' }}"
                                                       value="{{$user->less_five_year_child}}"
                                                       name="less_five_year_child">
                                                @if ($errors->has('less_five_year_child'))
                                                    <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('less_five_year_child') }}</strong>
                                    						</span>
                                                @endif
                                            </div>
                                            <div class="col-md-4 col-xs-6">
                                                <input style="height: calc(4.875rem + 2px);
    													padding: .5rem 1rem;
    													font-size: 15px;" placeholder="Number of School Children"
                                                       id="school_child" type="number"
                                                       class="form-control{{ $errors->has('school_child') ? ' is-invalid' : '' }}"
                                                       value="{{$user->school_child}}" name="school_child" required>
                                                @if ($errors->has('school_child'))
                                                    <span class="invalid-feedback">
                                        					<strong>{{ $errors->first('school_child') }}</strong>
                                    						</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <ul class="list-group row">

                                            <li style="margin-top: 16px;" class="list-group-item">
                                                <div style="text-align: center" class="mb-3">
                                                    @if(Auth::user()->id==4)
                                                        <input type="file" class="custom-file-input"
                                                               name="thumbnail_upload" id="id-input-file-2">
                                                        <div>
                                                            <img style="height: 211px"
                                                                 src="{{asset('no-thumbnail.jpeg')}}"
                                                                 id="imgthumbnailupload" class="img-thumbnail" alt="">
                                                        </div>
                                                        <div style="display: none" id="my_camera"></div>
                                                    @else
                                                        <div id="my_camera"></div>
                                                    @endif
                                                    <script type="text/javascript"
                                                            src="{{asset('/webcam.min.js')}}"></script>
                                                    <!-- Configure a few settings and attach camera -->
                                                    <script language="JavaScript">
                                                        Webcam.set({
//                                                                    width: 320,
                                                            height: 240,
                                                            image_format: 'jpeg',
                                                            jpeg_quality: 90
                                                        });
                                                        Webcam.attach('#my_camera');
                                                    </script>

                                                    <!-- A button for taking snaps -->
                                                    <form>
                                                        <input id="take_pic" style="margin-top: 10px"
                                                               class="btn btn-primary btn-sm" type=button
                                                               value="Take Snapshot" onClick="take_snapshot()">
                                                    </form>

                                                    <!-- Code to handle taking the snapshot and displaying it locally -->
                                                    <script language="JavaScript">
                                                        function take_snapshot() {
                                                            // take snapshot and get image data
                                                            Webcam.snap(function (data_uri) {
                                                                // display results in page
                                                                document.getElementById('results').innerHTML =
                                                                    '<h2></h2>' +
                                                                    '<img style="height: 94px;margin-top: -37px; margin-right: 13px;  border-radius: 10%;" type="file" name="" src="' + data_uri + '"/>';
//                                                                        alert('ok');
//                                                                        $('#capimg').value('ok');
                                                                document.getElementById("capimg").value = data_uri;
                                                            });
                                                        }
                                                    </script>
                                                </div>

                                            </li>

                                        </ul>
                                    </div>


                                    <div class="form-group row">
                                        <div style="margin-left: 12px" class="col-md-12">
                                            <input type="submit" class="btn btn-primary" value="Update User">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')

    <script type="text/javascript">
        jQuery(function ($) {
            autosize($('textarea[class*=autosize]'));

            $('textarea.limited').inputlimiter({
                remText: '%n character%s remaining...',
                limitText: 'max allowed : %n.'
            });

            $.mask.definitions['~'] = '[+-]';
            $('.input-mask-date').mask('99/99/9999');
//            $('.input-mask-phone').cursor(false);
            $('.input-mask-phone').mask('(99) 999-9999999');

            $('.input-mask-cnic').mask('99999-9999999-9');
            $('.input-mask-eyescript').mask('~9.99 ~9.99 999');
            $(".input-mask-product").mask("a*-999-a999", {
                placeholder: " ", completed: function () {
                    alert("You typed the following: " + this.val());
                }
            });

//			$(document).one('ajaxloadstart.page', function(e) {
//                autosize.destroy('textarea[class*=autosize]')
//
//                $('.limiterBox,.autosizejs').remove();
//                $('.daterangepicker.dropdown-menu,.colorpicker.dropdown-menu,.bootstrap-datetimepicker-widget.dropdown-menu').remove();
//            });

        });
    </script>


    <script>
        $('#summernote').summernote({
            height: 100
        });


        $(document).ready(function () {
            setTimeout(function () {
                $('.alert-success').fadeOut('slow');
            }, 3000);
        });
        //
        //        $('#id-input-file-2').on('change', function() {
        //            var file = $(this).get(0).files;
        //            var reader = new FileReader();
        //            reader.readAsDataURL(file[0]);
        //            reader.addEventListener("load", function(e) {
        //                var image = e.target.result;
        //                $("#imgthumbnail").attr('src', image);
        //            });
        //        });


        $('#id-input-file-2').on('change', function () {
            $('#my_camera').hide();
            $('#take_pic').hide();
            var file = $(this).get(0).files;
            var reader = new FileReader();
            reader.readAsDataURL(file[0]);
            reader.addEventListener("load", function (e) {
                var image = e.target.result;
                $("#imgthumbnailupload").attr('src', image);
            });
        });


    </script>

@endsection