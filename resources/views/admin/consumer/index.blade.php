@extends('admin.layouts.app2')
@section('content')

    <style>
        .dataTables_length{
            float: left !important;
        }
        .dataTables_filter{
            float: right !important;
        }
        .pagination{
            float: right !important;
        }
        .pgi{
            float: left !important;
            display: none;
        }
    </style>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Consumers</h4>

                            <a style="background: linear-gradient(145deg,#ffffff,#ffffff);
                                color: #07cdff;
                                margin-top: -14px;"
                               class="btn pull-right" href="{{route('consumer-create')}}">Add Consumer</a>

                            <a href="{{asset(route('list-member'))}}" style="background: linear-gradient(145deg,#ffffff,#ffffff);
                                                                        color: #07cdff;

                                                                        padding: 3px;
                                                                        margin-top: 14px;"
                               class="btn btn-xs pull-left">Get Register Member List</a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="material-datatables">
                                    <table id="" class="table table-striped table-bordered table-hover">
                                        {{--<table id="datatables" class="table table-bordered table-hover">--}}
                                        @if(Session::has('message'))
                                            <div class="alert alert-danger"><span
                                                        class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em>
                                            </div>
                                        @endif
                                        <thead class="text-primary">
                                        <tr>
                                            <th>
                                                No
                                            </th>
                                            <th>Img</th>
                                            <th>Form no</th>
                                            <th>Name</th>
                                            <th>Father Name</th>
                                            <th>District</th>
                                            <th>Tehsil</th>
                                            <th>village</th>
                                            <th>Phone</th>
                                            <th>CNIC</th>
                                            <th>Barcode</th>
                                            <th>Register Date</th>
                                            <th>Card Status</th>
                                            <th>Card issued Date</th>
                                            {{--<th >Address</th>--}}
                                            <th>Action</th>
                                        </tr>

                                        </thead>
                                        <tbody>
                                        @foreach($consumers as $key=> $consumer  )
                                            <tr>


                                                <td>
                                                    {{($consumers->currentpage() - 1) * $consumers->perpage() + 1 + $key}}
                                                </td>
                                                <td>
                                                    @if(isset($consumer->thumbnail))
                                                        {{--{{ $consumer->thumbnail}}--}}
                                                        <img src="{{ $consumer->thumbnail }}"
                                                             width="50px" height="50px">
                                                    @else
                                                        <strong>{{'No Img'}}</strong>
                                                    @endif
                                                </td>
                                                <td>{{$consumer->form_no}}</td>
                                                <td>{{$consumer->name}}</td>
                                                <td>{{$consumer->father_name}}</td>
                                                <td>{{@$consumer->distric->name}}</td>
                                                <td>{{@$consumer->tehsil->name}}</td>
                                                <td>{{@$consumer->village}}</td>
                                                <td>{{$consumer->mobile}}</td>
                                                <td>{{$consumer->cnic}}</td>
                                                <td>{{$consumer->barcode}}</td>
                                                <td>{{$consumer->created_at->format('d-M-Y')}}</td>

                                                <td>
                                                    @if($consumer->card_issued ==1)
                                                        <strong style="color: green">{{'Card Issue'}}</strong>
                                                    @else
                                                        <strong style="color: red">{{'No Card Issue'}}</strong>
                                                    @endif
                                                </td>

                                                <td>
                                                    @if(isset($consumer->card_issue_date) )
                                                        <span>{{$consumer->card_issue_date}}</span>
                                                    @else
                                                        <span>{{'------'}}</span>
                                                    @endif
                                                </td>

                                                {{--<td>{{$consumer->address}}</td>--}}

                                                <td>
                                                    <div class="">
                                                        <div class="inline pos-rel">
                                                            <button class="btn btn-minier btn-primary dropdown-toggle"
                                                                    data-toggle="dropdown" data-position="auto">
                                                                <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                                            </button>
                                                            <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                                                <li>
                                                                    <a href="{{route('consumer-show',['id'=>$consumer->id])}}"
                                                                       class="tooltip-info" data-rel="tooltip"
                                                                       title="View">
                                                                    <span class="blue">
                                                                        <i class="ace-icon fa fa-search-plus bigger-120"></i>
                                                                    </span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="{{route('consumer-edit',$consumer->id)}}"
                                                                       class="tooltip-success" data-rel="tooltip"
                                                                       title="Edit">
                                                                    <span class="green">
                                                                        <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                                                    </span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    @if(Auth::user()->role == 1)
                                                                        <a href="{{route('consumer-del',$consumer->id)}}"
                                                                           class="tooltip-error" data-rel="tooltip"
                                                                           title="Delete">
                                                                    <span class="red">
                                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                                    </span>
                                                                        </a>
                                                                    @endif
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </td>


                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                    <div class="text-left">{{ $consumers->links() }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="https://demos.creative-tim.com/material-dashboard-pro/assets/js/plugins/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#datatables').DataTable({
//                "pagingType": "full_numbers",

                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                }
            });

            var table = $('#datatable').DataTable();

            // Edit record
            table.on('click', '.edit', function () {
                $tr = $(this).closest('tr');
                var data = table.row($tr).data();
                alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
            });

            // Delete a record
            table.on('click', '.remove', function (e) {
                $tr = $(this).closest('tr');
                table.row($tr).remove().draw();
                e.preventDefault();
            });

            //Like record
            table.on('click', '.like', function () {
                alert('You clicked on Like button');
            });
        });
    </script>
    <script>


        $(document).ready(function () {
            setTimeout(function () {
                $('.alert-danger').slideUp('slow');
            }, 1000);
        });

        //        function printDiv() {
        //            window.frames["print_frame"].document.body.innerHTML = document.getElementById("").innerHTML;
        //            window.frames["print_frame"].window.focus();
        //            window.frames["print_frame"].window.print();
        //        }
        //
        $('#print_button').on('click', function () {
            alert('ok');
            window.frames["print_frame"].document.body.innerHTML = document.getElementById("printableTable").innerHTML;
            window.frames["print_frame"].window.print();
        });


    </script>
@endsection