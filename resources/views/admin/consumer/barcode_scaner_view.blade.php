@extends('admin.layouts.app2')
@section('content')

	<div class="content">
		<div class="container-fluid">
			<div style="
			margin-left:0px;margin-right:0px;
			margin-top:100px;"
				 class="row">

				<div class="col-md-8">
					<div style="margin-right: 0px;margin-left: 0px;" class="row">
					<div style="    background-color: white;
    box-shadow: 1px 4px 4px #ccc;
    padding: 0px;
    height: 45px;" class="col-md-10 col-sm-12">
						<input style="height: 46px !important;padding-left: 15px;" class="scan_input form-control" type="text">
					</div>
					<div  class="hidden-xs col-md-2 ">
						<a style="    color: #fff;
    text-transform: capitalize;
    background-color: #07cdff;
    border-color: #07cdff;
    height: 46px;
    padding-top: 9px;
    margin-top: 0px;
    width: 100%;
    margin-left: 11px;
    box-shadow: 0 2px 2px 0 #ccc;
    font-size: 20px;" class="btn btn-primary btn-md">Enter</a>
					</div>
					</div>
					<div style="height: 386px;" class="card">
						<div class="card-body">
							<h3 style="font-weight:400">Water Consumption</h3>
							<h4 style="margin-top: 45px; font-weight:400">Today Consumption: <span style="color:#d70909; font-weight:500"><span id="today_total_consumer_serve"> {{ @$sum_total_consumer_serve = DB::table('consumptions')->pluck('today_total_consumer_serve')->sum()}} </span> Litter</span></h4>
							<h4 style="margin-top: 31px; font-weight:400">Total Consumption: <span style="color:#d70909; font-weight:500">  <span id="total_consumpation"> {{ @$sum_total_consumer_serve = DB::table('consumptions')->where('plant_id',Auth::user()->plant_id)->pluck('total_consumer_serve')->sum()}} </span> Liter</span></h4>
							<a style="color: #fff;
    text-transform: capitalize;
    background-color: #07cdff;
    border-color: #07cdff;
    height: 46px;
    padding-top: 9px;
    width: 130px;
    margin-left: 11px;
    box-shadow: 0 2px 2px 0 #ccc;
    font-size: 20px;
    float: right;
    margin-top: 119px;" id="crn-update" class="btn btn-primary btn-md">Reset</a>
						</div>
					</div>
				</div>



				<div style="margin-top: 50px" class="col-md-4">
					<div style="height: 386px;" class="card card-profile">
						<div class="card-avatar">
							<a href="#pablo">
								<div id="cons_pic">
                                    <img style="border-radius: 50%;padding: 2px;" src="{{asset('assets/images/panisab.jpg')}}">
								</div>

							</a>
						</div>
						<div style="text-align: left;
    						padding: 45px;" class="card-body">
							<h3 id="cons_name" style="margin-top: -15px;
														font-weight: 500;
														text-align: center;
														text-transform: capitalize;">Name</h3>
							<h4 style="margin-top:30px;font-weight:400">Status : <a id="show_serve_btn_serve" style=" display: none; text-decoration: none!important;
															background: #87B87F!important;
															color: white;
															padding: 5px;
															border-radius: 10px;
															font-size: 16px;" class="success">Water Serve</a>
								<a id="show_already_serve" style=" display: none; text-decoration: none!important;
															background: indianred!important;
															color: white;
															padding: 5px;
															border-radius: 10px;
															font-size: 16px;" class="success">Already Serve</a>
								<a id="invaild_btn" style=" display: none; text-decoration: none!important;
															background: indianred!important;
															color: white;
															padding: 5px;
															border-radius: 10px;
															font-size: 16px;" class="success">Invalid Card</a>
							</h4>
							<h4 style="font-weight:400;margin-top:20px">Today Consumption : <span style="color: #d70909;font-weight: 500;"><span  class="consmp_ser_water">0</span> Liters</span></h4>
							<h4 style="font-weight:400;margin-top:20px">Total Water Consume : <span style="color: #d70909;font-weight: 500;"><span id="total_consmp_ser_water">0</span> Liters</span></h4>
							{{--<h4 style="margin-top: 31px; font-weight:400">Total Consumption: <span style="color:#d70909; font-weight:500">  1882 Liters</span></h4>--}}

						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

@endsection
@section('script')
	<script>

        $('#crn-update').click(function () {
            alert('press ok');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/crone-update',
                type: 'POST',
				/* send the csrf-token and the input to the controller */
                data: {},
                dataType: 'JSON',

                success:function (result) {

                },

                error:function (resulr) {

                }

            });

        });


		$('.scan_input').change(function () {
//			alert('ok');
            var val = $('.consmp_ser_water').text();
            var code=($(this)).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '/scane',
                type: 'POST',
                data:{ id :code},
                dataType: 'JSON',
				success:
                    function (result) {
//                        result = JSON.parse(result);
                        $('#cons_name').html(result.options);
                        $('#cons_pic').html(result.cons_pics);

                        $('#sum_total_consumer_serve').html(result.sum_total_consumer_serve);
                        $('#today_total_consumer_serve').html(result.today_total_consumer_serve);
                        $('#total_consumpation').html(result.total_consumpation);
                            var check_user_consume=$('.consmp_ser_water').text(result.consmp_ser_water);
                        if(result.invalid_card == 0){
                            $('.consmp_ser_water').text('0');
                            $('#invaild_btn').show();
//                            $('#cons_name').html('');
//                            $('#cons_pic').html('');
                            $('#total_consmp_ser_water').html('0');

                            $('#show_already_serve').hide();
                            $('#show_serve_btn_serve').hide();

                        }

                        if(result.consmp_ser_water == 0){
                            $('#show_already_serve').show();
                            $('#invaild_btn').hide();
                            $('#show_serve_btn_serve').hide();

                        }

                        if(result.consmp_ser_water == 19){
                            $('#show_serve_btn_serve').show();
                            $('#invaild_btn').hide();
                            $('#show_already_serve').hide();
                        }


                        $('#total_consmp_ser_water').html(result.total_consmp_ser_water);
                        $('.scan_input').val('');

                    },
                error: function (result) {
//                    alert('error this form');
                }

            });



        });

       </script>

@endsection
