@extends('admin.layouts.app2')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>

                    <li>
                        <a href="{{route('dashboard')}}">Dashboard</a>
                    </li>
                </ul><!-- /.breadcrumb -->

                <div class="nav-search" id="nav-search">
                    <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
                    </form>
                </div><!-- /.nav-search -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    {{--<a href="{{asset(route('excel-export'))}}" class="btn btn-primary btn-sm pull-right">Create Excel</a>--}}
                    <a href="{{asset(route('list-member'))}}" class="btn btn-primary btn-sm pull-right">Get Register Member List</a>
                    {{--<button id="print_button"  class="btn btn-primary btn-sm pull-right">Create Print</button>--}}
                    <h1>
                        Members
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Show All Members
                        </small>
                    </h1>

                </div><!-- /.page-header -->

                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    @if(Session::has('message'))
                                        <div class="alert alert-danger"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em></div>
                                    @endif
                                <table id="printableTable" class="table  table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            No
                                        </th>
                                        <th class="center">Img</th>
                                        <th class="center">Form no</th>
                                        <th class="center">Name</th>
                                        <th class="center">Father Name</th>
                                        <th class="center">District</th>
                                        <th class="center">Tehsil</th>
                                        <th class="center">village</th>
                                        <th class="center">Phone</th>
                                        <th class="center">CNIC</th>
                                        <th class="center">Barcode</th>
                                        <th class="center">Card Status</th>
                                        <th class="center">Card issued Date</th>
                                        <th class="center">Address</th>

                                        <th class="center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($consumers as $key=> $consumer  )
                                        <tr>


                                            <td class="center">
                                                {{($consumers->currentpage() - 1) * $consumers->perpage() + 1 + $key}}
                                            </td>
                                            <td class="center">
                                                @if(isset($consumer->thumbnail))
                                                    {{--{{ $consumer->thumbnail}}--}}
                                                    <img src="{{ $consumer->thumbnail }}"
                                                         width="50px" height="50px">
                                                @else
                                                    <strong>{{'No Img'}}</strong>
                                                @endif
                                            </td>
                                            <td class="center">{{$consumer->form_no}}</td>
                                            <td class="center">{{$consumer->name}}</td>
                                            <td class="center">{{$consumer->father_name}}</td>
                                            <td class="center">{{@$consumer->distric->name}}</td>
                                            <td class="center">{{@$consumer->tehsil->name}}</td>
                                            {{--<td class="center">{{$consumer->tehsil->name}}</td>--}}
                                            <td class="center">{{@$consumer->village}}</td>
                                            <td class="center">{{$consumer->mobile}}</td>
                                            <td class="center">{{$consumer->cnic}}</td>
                                            <td class="center">{{$consumer->barcode}}</td>

                                            <td class="center">
                                                @if($consumer->card_issued ==1)
                                                    <strong style="color: green">{{'Card Issue'}}</strong>
                                                    @else
                                                <strong style="color: red">{{'No Card Issue'}}</strong>
                                                    @endif
                                            </td>

                                            <td class="center">
                                                @if(isset($consumer->card_issue_date) )
                                                    <span class="center">{{$consumer->card_issue_date}}</span>
                                                    @else
                                                <span>{{'------'}}</span>
                                                    @endif
                                            </td>

                                            <td class="center">{{$consumer->address}}</td>

                                            <td class="center">
                                                <div class="">
                                                    <div class="inline pos-rel">
                                                        <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                                            <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                                        </button>
                                                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                                            <li>
                                                                <a href="{{route('consumer-show',['id'=>$consumer->id])}}" class="tooltip-info" data-rel="tooltip" title="View">
                                                                    <span class="blue">
                                                                        <i class="ace-icon fa fa-search-plus bigger-120"></i>
                                                                    </span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="{{route('consumer-edit',$consumer->id)}}" class="tooltip-success" data-rel="tooltip" title="Edit">
                                                                    <span class="green">
                                                                        <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                                                    </span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                @if(Auth::user()->role == 1)
                                                                <a href="{{route('consumer-del',$consumer->id)}}" class="tooltip-error" data-rel="tooltip" title="Delete">
                                                                    <span class="red">
                                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                                    </span>
                                                                </a>
                                                                @endif
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>


                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                                </div>
                                <div class="text-left">{{ $consumers->links() }}</div>
                            </div><!-- /.span -->
                        </div><!-- /.row -->

                        <div class="hr hr-18 dotted hr-double"></div>

                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div>
@endsection
@section('script')
    <script>

        $(document).ready(function() {

        } );

//        function printDiv() {
//            window.frames["print_frame"].document.body.innerHTML = document.getElementById("").innerHTML;
//            window.frames["print_frame"].window.focus();
//            window.frames["print_frame"].window.print();
//        }
//
        $('#print_button').on('click',function(){
            alert('ok' );
            window.frames["print_frame"].document.body.innerHTML = document.getElementById("printableTable").innerHTML;
            window.frames["print_frame"].window.print();
        });

        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-danger').fadeOut('slow');
            }, 3000);
        });
    </script>
@endsection