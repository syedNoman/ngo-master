@extends('admin.layouts.app')
@section('content')
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>

					<li>
						<a href="{{route('dashboard')}}">Dashboard</a>
					</li>
					<li class="active">NGO</li>
				</ul><!-- /.breadcrumb -->

				<div class="nav-search" id="nav-search">
					<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
					</form>
				</div><!-- /.nav-search -->
			</div>

			<div class="page-content">
				<div class="ace-settings-container" id="ace-settings-container">
					<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
						<i class="ace-icon fa fa-cog bigger-130"></i>
					</div>

					<div class="ace-settings-box clearfix" id="ace-settings-box">
						<div class="pull-left width-50">
							<div class="ace-settings-item">
								<div class="pull-left">
									<select id="skin-colorpicker" class="hide">
										<option data-skin="no-skin" value="#438EB9">#438EB9</option>
										<option data-skin="skin-1" value="#222A2D">#222A2D</option>
										<option data-skin="skin-2" value="#C6487E">#C6487E</option>
										<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
									</select>
								</div>
								<span>&nbsp; Choose Skin</span>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
								<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
								<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
								<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
								<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
								<label class="lbl" for="ace-settings-add-container">
									Inside
									<b>.container</b>
								</label>
							</div>
						</div><!-- /.pull-left -->

						<div class="pull-left width-50">
							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
								<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
								<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
								<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
							</div>
						</div><!-- /.pull-left -->
					</div><!-- /.ace-settings-box -->
				</div><!-- /.ace-settings-container -->

				<div class="page-header">
					<h1>
						Memeber
						<small>
							<i class="ace-icon fa fa-angle-double-right"></i>
							View member
						</small>
					</h1>
				</div><!-- /.page-header -->

				<div class="row">
					<div class="col-xs-12">
						<!-- PAGE CONTENT BEGINS -->
						<div class="row">
							<div class="col-xs-12">
								@if(Session::has('message'))
									<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em></div>
								@endif
								<div style="box-shadow: 2px 2px 2px 2px #ccc;" class="col-md-12" class="card-body">
									<div style="height:auto; box-shadow: -1px 4px 2px #ccc;" class="col-md-12">
										<div class="row">
											<div class="col-md-2 col-xs-12">
												<img style="  border-radius: 43px; height: 202px; padding: 24px;" src="{{asset('assets/images/panisab.jpg')}}">
											</div>
											<div class="col-md-7 col-xs-12">
												<p style="margin-top: 37px">
												<h1>PANI SAB KAY LIYE</h1>
												<h4>A Project Of Rasheeda Trust</h4>
												</p>
											</div>
											<div class="col-md-3 col-xs-12">
												<img style="float: right;
											height: 102px;
											padding: 20px;
											margin-top: 41px;" src="{{asset('assets/images/rasheedalogo.png')}}">
											</div>
										</div>
									</div>
									{{--End header--}}
									<div class="col-xs-12">
										<h2>Consumer Profile Info</h2>
										<div class="col-md-6" style="">
											<div class="row">
											  <div class="col-md-12">
											       <div class="col-lg-3 col-md-6">
												      <img style="border-radius: 5px;
														height: 165px;
														width: 100%;
														margin-top: 21px;"  src="{{$user->thumbnail}}">
											        </div>
												    <div class="col-lg-8 col-md-8">
													   {{--<img style="  border-radius: 43px; height: 202px; padding: 24px;" src="{{asset('assets/images/panisab.jpg')}}">--}}
														<p><h1>{{$user->name}}</h1>
														<h3><strong>Father Name:</strong>{{' '}}{{$user->father_name}}</h3></p>
														<h3><strong>CNIC:</strong>{{' '}}{{$user->cnic}}</h3></p>
														<h3><strong>Phone:</strong>{{' '}}{{$user->mobile}}</h3></p>
														<h3><strong>Activation Date:</strong>{{' '}}{{$user->card_issue_date}}</h3></p>
														<h3><strong>Phone:</strong>{{' '}}{{$user->mobile}}</h3></p>
														<h3><strong>Village:</strong>{{' '}}{{$user->village}}</h3></p>
														<h3><strong>Address:</strong>{{' '}}{{$user->address}}</h3>
														<h3 style="color: green"><strong>Total Water Consumed :</strong>{{$consump->total_consumer_serve}}{{' '}}{{'Litres'}}</h3>
														<a  style="background-color: #00BE67; margin-bottom: 10px" class="col-md-3 col-sm-12 btn btn-success">Go Back</a>
												    </div>


											  </div>
											</div>
										</div>
									</div>


								</div>
							</div><!-- /.span -->
						</div><!-- /.row -->

						<div class="hr hr-18 dotted hr-double"></div>

						<!-- PAGE CONTENT ENDS -->
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.page-content -->
		</div>
	</div>
@endsection

@section('script')
	<script>
        $('#summernote').summernote({
            height: 100
        });


        //        $(".select").chosen({max_selected_options: 5});
        $(".select").chosen({width: "100%"});

        $('.slugify').keyup(function () {
            var url=($(this)).val();
//          str = url.split(" ").join("-").toLowerCase();
            str = url.replace(/\s+/g, '-').toLowerCase();
            $('#slug-text').text(str);
            $('#slug_title').val(str);
        });

        //        $('#title').on('keyup',(function () {
        //            $('#slug-text').text('ok');
        //        });
        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-success').fadeOut('slow');
            }, 3000);
        });

        $('#id-input-file-2').on('change', function() {
            var file = $(this).get(0).files;
            var reader = new FileReader();
            reader.readAsDataURL(file[0]);
            reader.addEventListener("load", function(e) {
                var image = e.target.result;
                $("#imgthumbnail").attr('src', image);
            });
        });



	</script>

@endsection