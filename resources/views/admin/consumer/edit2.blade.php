@extends('admin.layouts.app')
@section('content')
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>

					<li>
						<a href="{{route('dashboard')}}">Dashboard</a>
					</li>
					<li class="active">NGO</li>
				</ul><!-- /.breadcrumb -->

				<div class="nav-search" id="nav-search">
					<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
					</form>
				</div><!-- /.nav-search -->
			</div>

			<div class="page-content">
				<div class="ace-settings-container" id="ace-settings-container">
					<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
						<i class="ace-icon fa fa-cog bigger-130"></i>
					</div>

					<div class="ace-settings-box clearfix" id="ace-settings-box">
						<div class="pull-left width-50">
							<div class="ace-settings-item">
								<div class="pull-left">
									<select id="skin-colorpicker" class="hide">
										<option data-skin="no-skin" value="#438EB9">#438EB9</option>
										<option data-skin="skin-1" value="#222A2D">#222A2D</option>
										<option data-skin="skin-2" value="#C6487E">#C6487E</option>
										<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
									</select>
								</div>
								<span>&nbsp; Choose Skin</span>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
								<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
								<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
								<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
								<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
								<label class="lbl" for="ace-settings-add-container">
									Inside
									<b>.container</b>
								</label>
							</div>
						</div><!-- /.pull-left -->

						<div class="pull-left width-50">
							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
								<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
								<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
								<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
							</div>
						</div><!-- /.pull-left -->
					</div><!-- /.ace-settings-box -->
				</div><!-- /.ace-settings-container -->

				<div class="page-header">
					<h1>
						Labour
						<small>
							<i class="ace-icon fa fa-angle-double-right"></i>
							Update Labour
						</small>
					</h1>
				</div><!-- /.page-header -->

				<div class="row">
					<div class="col-xs-12">
						<!-- PAGE CONTENT BEGINS -->
						<div class="row">
							<div class="col-xs-12">
								@if(Session::has('message'))
									<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em></div>
								@endif
								<div style="box-shadow: 2px 2px 2px 2px #ccc;" class="col-xs-12" class="card-body">

									<form  action="{{route('consumer-update',$user->id)}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
										<div class="row">
											@csrf
											<div class="col-lg-9">
												<div class="row">
													<label style="margin-top:20px;" for="form_no" class="col-xs-12 col-form-label text-md-right">{{ __('Form Number') }}</label>
													<div class="col-xs-2">
														<input placeholder="Enter Form Number" id="form_no" type="text" class="form-control{{ $errors->has('form_no') ? ' is-invalid' : '' }}" name="form_no" value="{{$user->form_no}}"  required>
														@if ($errors->has('form_no'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('form_no') }}</strong>
                                    						</span>
														@endif
													</div>

													<div style="text-align: right">
														<div id="results">
															@if(isset($user))
																<img style="height: 94px;margin-top: -37px; margin-right: 13px;  border-radius: 10%;" type="file" name="" src="{{$user->thumbnail}}"/>
																@endif
														</div>
													</div>

												</div>
												<div class="form-group row">
													<div class="col-xs-3">
														<label style="margin-top:20px;" for="email" class="col-xs-12 col-form-label text-md-right">{{ __('District') }}</label>
														<select style="text-transform: capitalize" id="district" type="text" class="form-control" name="distric_id"  required>
															<option value="" hidden>Select Distric Name</option>
															@foreach($districs as $distric)
																<option @if($user->distric_id == $distric->id) selected @endif style="text-transform: capitalize" value="{{$distric->id}}" >{{$distric->name}}</option>
															@endforeach
														</select>
													</div>
													<div class="col-xs-3">
														<label style="margin-top:20px;" for="email" class="col-xs-12 col-form-label text-md-right">{{ __('Tehsil') }}</label>
														<select style="text-transform: capitalize"  id="district" type="text" class="form-control" name="tehsil_id"  required>
															<option value="" hidden>Select Tehsil Name</option>
															@foreach($tehsils as $tehsil)
																<option @if($user->tehsil_id == $tehsil->id) selected @endif style="text-transform: capitalize" value="{{$tehsil->id}}" >{{$tehsil->name}}</option>
															@endforeach
														</select>
													</div>

													<div class="col-xs-3">
														<label style="margin-top:20px;" for="union_council" class="col-xs-12 col-form-label text-md-right">{{ __('Union Council') }}</label>
														<select style="text-transform: capitalize"  id="district" type="text" class="form-control" name="union_council_id"  required>
															<option value="" hidden>Select Union Council Name</option>
															@foreach($concils as $concil)
																<option @if($user->union_council_id == $concil->id) selected @endif style="text-transform: capitalize" value="{{$concil->id}}" >{{$concil->name}}</option>
															@endforeach
														</select>
													</div>

													<div class="col-xs-3">
														<label style="margin-top:20px;" for="village" class="col-xs-12 col-form-label text-md-right">{{ __('Village') }}</label>
														<input style="text-transform: capitalize" placeholder="Enter Village Name" id="village" type="text" class="form-control{{ $errors->has('village') ? ' is-invalid' : '' }}" value="{{$user->village}}" name="village"  required>
														@if ($errors->has('village'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('village') }}</strong>
                                    						</span>
														@endif
													</div>

												</div>


												<div class="form-group row">
													<div class="col-xs-6">
														<label class="form-control-label">Name</label>
														<input placeholder="Enter Name" id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required name="name" value="{{$user->name}}" >
														@if ($errors->has('name'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('name') }}</strong>
                                    						</span>
														@endif
													</div>
													<div class="col-xs-6">
														<label class="form-control-label">Father Name</label>
														<input placeholder="Enter Father Name" id="father_name" type="text" class="form-control{{ $errors->has('father_name') ? ' is-invalid' : '' }}" required name="father_name" value="{{$user->father_name}}"  >
														@if ($errors->has('father_name'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('father_name') }}</strong>
                                    						</span>
														@endif
													</div>

												</div>

												<div class="form-group row">
													<div class="col-xs-6">
														<label class="form-control-label">CNIC Number</label>
														<input style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="36203-12345678-7" id="cnic" type="text" class="input-mask-cnic form-control{{ $errors->has('cnic') ? ' is-invalid' : '' }}" value="{{$user->cnic}}" required name="cnic" >
														@if ($errors->has('cnic'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('cnic') }}</strong>
                                    						</span>
														@endif
													</div>
													<div class="col-xs-6">
														<label class="form-control-label">Mobile Number</label>
														<input  style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;"   id="mobile" type="text"  class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" value="{{$user->mobile}}" required name="mobile"  >
														@if ($errors->has('mobile'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('mobile') }}</strong>
                                    						</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label for="email" class="col-xs-12 col-form-label text-md-right">{{ __('Address') }}</label>
													<div class="col-xs-12">
														<textarea placeholder="Enter Address" rows="3" id="email" type="number" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address"  required>{{$user->address}}</textarea>
														@if ($errors->has('address'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('address') }}</strong>
                                    						</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<div class="col-xs-3">
														<label style="margin-top:20px;" for="way_of_income" class="col-xs-12 col-form-label text-md-right">{{ __('Way of Income') }}</label>
														<input placeholder="Enter Way of Income" id="way_of_income" type="text" class="form-control{{ $errors->has('way_of_income') ? ' is-invalid' : '' }}" name="way_of_income" value="{{$user->way_of_income}}"  required>
														@if ($errors->has('way_of_income'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('way_of_income') }}</strong>
                                    						</span>
														@endif
													</div>
													<div class="col-xs-3">
														<label style="margin-top:20px;" for="email" class="col-xs-12 col-form-label text-md-right">{{ __('House Member') }}</label>
														<input placeholder="0" id="house_member" type="number" class="form-control{{ $errors->has('house_member') ? ' is-invalid' : '' }}" name="house_member" value="{{$user->house_member}}"  required>
														@if ($errors->has('house_member'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('house_member') }}</strong>
                                    						</span>
														@endif
													</div>
													<div class="col-xs-3">
														<label style="margin-top:20px;" for="male" class="col-xs-12 col-form-label text-md-right">{{ __('Male') }}</label>
														<input placeholder="0" id="male" type="number" class="form-control{{ $errors->has('male') ? ' is-invalid' : '' }}" name="male" value="{{$user->male}}"  required>
														@if ($errors->has('male'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('male') }}</strong>
                                    						</span>
														@endif
													</div>
													<div class="col-xs-3">
														<label style="margin-top:20px;" for="female" class="col-xs-12 col-form-label text-md-right">{{ __('Female') }}</label>
														<input placeholder="0" id="female" type="number" class="form-control{{ $errors->has('female') ? ' is-invalid' : '' }}" name="female" value="{{$user->female}}"  required>
														@if ($errors->has('female'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('female') }}</strong>
                                    						</span>
														@endif
													</div>

												</div>

												<div class="form-group row">
													<div class="col-xs-6">
														<label class="form-control-label">Less than 5 year Children</label>
														<input placeholder="0" id="less_five_year_child" type="number" required class="form-control{{ $errors->has('less_five_year_child') ? ' is-invalid' : '' }}" name="less_five_year_child" value="{{$user->less_five_year_child}}" >
														@if ($errors->has('less_five_year_child'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('less_five_year_child') }}</strong>
                                    						</span>
														@endif
													</div>
													<div class="col-xs-6">
														<label class="form-control-label">Number of School Children</label>
														<input placeholder="0" id="school_child" type="number" class="form-control{{ $errors->has('school_child') ? ' is-invalid' : '' }}" name="school_child" required value="{{$user->school_child}}" >
														@if ($errors->has('school_child'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('school_child') }}</strong>
                                    						</span>
														@endif
													</div>
												</div>

												<div class="form-group row mb-0">
													<div class="col-xs-12">
														<input type="submit" class="btn btn-primary" value="Update User">
													</div>
												</div>

											</div>

											<div class="col-lg-3">
												<ul class="list-group row">
													<li class="list-group-item active"><h5>Take Picture</h5></li>
													<li class="list-group-item">
														<div style="text-align: center" class="mb-3">
															{{--<input id="capimg" hidden  type="text" value="{{$user->thumbnail}}"    name="thumbnail"  >--}}



															@if(Auth::user()->id==4)
																<input type="file"  class="custom-file-input" name="thumbnail_upload" id="id-input-file-2">
																<div>
																	<img style="height: 211px" src="{{asset('no-thumbnail.jpeg')}}" id="imgthumbnailupload" class="img-thumbnail" alt="" >
																</div>
																<div style="display: none" id="my_camera"></div>
															@else
																<div id="my_camera"></div>
															@endif

															<!-- First, include the Webcam.js JavaScript Library -->
															<script type="text/javascript" src="{{asset('/webcam.min.js')}}"></script>

															<!-- Configure a few settings and attach camera -->
															<script language="JavaScript">
                                                                Webcam.set({
//                                                                    width: 320,
                                                                    height: 240,
                                                                    image_format: 'jpeg',
                                                                    jpeg_quality: 90
                                                                });
                                                                Webcam.attach( '#my_camera' );
															</script>

															<!-- A button for taking snaps -->
															<form>
																<input id="take_pic" style="margin-top: 10px" class="btn btn-primary btn-sm" type=button value="Take Snapshot" onClick="take_snapshot()">
															</form>

															<!-- Code to handle taking the snapshot and displaying it locally -->
															<script language="JavaScript">
                                                                function take_snapshot() {
                                                                    // take snapshot and get image data
                                                                    Webcam.snap( function(data_uri) {
                                                                        // display results in page
                                                                        document.getElementById('results').innerHTML =
                                                                            '<h2></h2>' +
                                                                            '<img style="height: 94px;margin-top: -37px; margin-right: 13px;  border-radius: 10%;" type="file" name="" src="'+data_uri+'"/>';
//                                                                        alert('ok');
//                                                                        $('#capimg').value('ok');
                                                                        document.getElementById("capimg").value = data_uri;
                                                                    } );


//                                                                    margin-top: -37px;
//                                                                    margin-right: 13px;
//                                                                    border-radius: 10%;

                                                                }

															</script>

														</div>
														{{--<div class="img-thumbnail text-center">--}}
														{{--<img src="@if(isset($user)) {{asset('assets/uploads/labour-pics/'.$user->img)}} @else {{asset('assets/images/no-thumbnail.jpeg')}} @endif" id="imgthumbnail" class="img-thumbnail" alt="">--}}
														{{--</div>--}}
													</li>

												</ul>


											</div>
										</div>
									</form>

								</div>
							</div><!-- /.span -->
						</div><!-- /.row -->

						<div class="hr hr-18 dotted hr-double"></div>

						<!-- PAGE CONTENT ENDS -->
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.page-content -->
		</div>
	</div>
@endsection

@section('script')

	<script type="text/javascript">
        jQuery(function($) {
            autosize($('textarea[class*=autosize]'));

            $('textarea.limited').inputlimiter({
                remText: '%n character%s remaining...',
                limitText: 'max allowed : %n.'
            });

            $.mask.definitions['~']='[+-]';
            $('.input-mask-date').mask('99/99/9999');
//            $('.input-mask-phone').cursor(false);
            $('.input-mask-phone').mask('(99) 999-9999999');

            $('.input-mask-cnic').mask('99999-9999999-9');
            $('.input-mask-eyescript').mask('~9.99 ~9.99 999');
            $(".input-mask-product").mask("a*-999-a999",{placeholder:" ",completed:function(){alert("You typed the following: "+this.val());}});

//			$(document).one('ajaxloadstart.page', function(e) {
//                autosize.destroy('textarea[class*=autosize]')
//
//                $('.limiterBox,.autosizejs').remove();
//                $('.daterangepicker.dropdown-menu,.colorpicker.dropdown-menu,.bootstrap-datetimepicker-widget.dropdown-menu').remove();
//            });

        });
	</script>


	<script>
        $('#summernote').summernote({
            height: 100
        });



        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-success').fadeOut('slow');
            }, 3000);
        });
//
//        $('#id-input-file-2').on('change', function() {
//            var file = $(this).get(0).files;
//            var reader = new FileReader();
//            reader.readAsDataURL(file[0]);
//            reader.addEventListener("load", function(e) {
//                var image = e.target.result;
//                $("#imgthumbnail").attr('src', image);
//            });
//        });


        $('#id-input-file-2').on('change', function() {
            $('#my_camera').hide();
            $('#take_pic').hide();
            var file = $(this).get(0).files;
            var reader = new FileReader();
            reader.readAsDataURL(file[0]);
            reader.addEventListener("load", function(e) {
                var image = e.target.result;
                $("#imgthumbnailupload").attr('src', image);
            });
        });




	</script>

@endsection