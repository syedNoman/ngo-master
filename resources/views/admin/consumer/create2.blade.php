@extends('admin.layouts.app')
@section('content')
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>

					<li>
						<a href="{{route('dashboard')}}">Dashboard</a>
					</li>
					<li class="active">Labour</li>
				</ul><!-- /.breadcrumb -->

				<div class="nav-search" id="nav-search">
					<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
					</form>
				</div><!-- /.nav-search -->
			</div>

			<div class="page-content">
				<div class="ace-settings-container" id="ace-settings-container">
					<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
						<i class="ace-icon fa fa-cog bigger-130"></i>
					</div>

					<div class="ace-settings-box clearfix" id="ace-settings-box">
						<div class="pull-left width-50">
							<div class="ace-settings-item">
								<div class="pull-left">
									<select id="skin-colorpicker" class="hide">
										<option data-skin="no-skin" value="#438EB9">#438EB9</option>
										<option data-skin="skin-1" value="#222A2D">#222A2D</option>
										<option data-skin="skin-2" value="#C6487E">#C6487E</option>
										<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
									</select>
								</div>
								<span>&nbsp; Choose Skin</span>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
								<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
								<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
								<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
								<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
								<label class="lbl" for="ace-settings-add-container">
									Inside
									<b>.container</b>
								</label>
							</div>
						</div><!-- /.pull-left -->

						<div class="pull-left width-50">
							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
								<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
								<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
							</div>

							<div class="ace-settings-item">
								<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
								<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
							</div>
						</div><!-- /.pull-left -->
					</div><!-- /.ace-settings-box -->
				</div><!-- /.ace-settings-container -->

				<div class="page-header">
					<h1>
						Labour
						<small>
							<i class="ace-icon fa fa-angle-double-right"></i>
							Add Labour
						</small>
					</h1>
				</div><!-- /.page-header -->

				<div class="row">
					<div class="col-xs-12">
						<!-- PAGE CONTENT BEGINS -->
						<div class="row">
							<div class="col-xs-12">
								@if(Session::has('message'))
									<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em></div>
								@endif
								<div style="box-shadow: 2px 2px 2px 2px #ccc;" class="col-md-12" class="card-body">
										<div style="height:auto; box-shadow: -1px 4px 2px #ccc;" class="col-md-12">

											<div class="row">
												<div class="col-md-2 col-xs-12">
													<img style="  border-radius: 43px; height: 202px; padding: 24px;" src="{{asset('assets/images/panisab.jpg')}}">
												</div>
												<div class="col-md-7 col-xs-12">
													<p style="margin-top: 37px">
													<h1>PANI SAB KAY LIYE</h1>
													<h4>A Project Of Rasheeda Trust</h4>
													</p>
												</div>
												<div class="col-md-3 col-xs-12">
													<img style="float: right;
											height: 102px;
											padding: 20px;
											margin-top: 41px;" src="{{asset('assets/images/rasheedalogo.png')}}">
												</div>
											</div>
										</div>
									<div class="col-xs-12">
										<u style="color:#858585 "><h1 style="font-size: 39px;
    											color: #858585;">Consumer Registration</h1></u>
									</div>
									<form  action="{{route('consumer-save')}}"  method="post" accept-charset="utf-8" enctype="multipart/form-data">
										<div class="row">
											@csrf
											<div class="col-lg-9">
												<div class="row">
													<label style="margin-top:20px;" for="form_no" class="col-xs-12 col-form-label text-md-right"><h4>Form Number</h4></label>
													<div class="col-md-3 col-xs-12">
															<input style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="Enter Form Number" id="form_no" type="text" class="form-control" name="form_no"  required>
														@if ($errors->has('form_no'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('form_no') }}</strong>
                                    						</span>
														@endif
													</div>

													{{--<div style="text-align: right">--}}
														{{--<div id="results"></div>--}}
													{{--</div>--}}

												</div>
												<div class="form-group row">
													<div class="col-md-3 col-xs-6">
														<label style="margin-top:20px;" for="email"><h4>District</h4></label>
														<select style="text-transform: capitalize;height: calc(4.875rem + 2px);
																padding: .5rem 1rem;
																font-size: 1.9rem;
																line-height: 1.5;
																border-radius: 0.6rem !important; " id="district" type="text" class="form-control" name="distric_id"  required>
															<option value="" hidden>Select Distric Name</option>
															@foreach($districs as $distric)
																<option style="text-transform: capitalize" value="{{$distric->id}}" >{{$distric->name}}</option>
																@endforeach
														</select>
													</div>
													<div class="col-md-3 col-xs-6">
														<label style="margin-top:20px;" for="email"><h4>Tehsil</h4></label>
														<select style="text-transform: capitalize;height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;"  id="tehsil" type="text" class="form-control" name="tehsil_id"  required>
															<option value="" hidden>Select Tehsil Name</option>
															@foreach($tehsils as $tehsil)
																<option style="text-transform: capitalize" value="{{$tehsil->id}}" >{{$tehsil->name}}</option>
															@endforeach
														</select>
													</div>

													<div class="col-md-3 col-xs-6">
														<label style="margin-top:20px;" for="union_council"><h4>Union Council</h4></label>
														<select style="text-transform: capitalize;height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;"  id="concil" type="text" class="form-control" name="union_council_id"  required>
															<option value="" hidden>Select Union Council Name</option>
															@foreach($concils as $concil)
																<option style="text-transform: capitalize" value="{{$concil->id}}" >{{$concil->name}}</option>
															@endforeach
														</select>
													</div>

													<div class="col-md-3 col-xs-6">
														<label style="margin-top:20px;" for="village"><h4>Village</h4></label>
														<input style="text-transform: capitalize;height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="Enter Village Name" id="village" type="text" class="form-control{{ $errors->has('village') ? ' is-invalid' : '' }}" name="village"  required>
														@if ($errors->has('village'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('village') }}</strong>
                                    						</span>
														@endif
													</div>

												</div>


												<div class="form-group row">
													<div class="col-xs-6">
														<label class="form-control-label"><h4>Name</h4></label>
														<input style="text-transform: capitalize;height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="Enter Name" id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required name="name" >
														@if ($errors->has('name'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('name') }}</strong>
                                    						</span>
														@endif
													</div>
													<div class="col-xs-6">
														<label class="form-control-label"><h4>Father Name</h4></label>
														<input style="text-transform: capitalize;height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="Enter Father Name" id="father_name" type="text" class="form-control{{ $errors->has('father_name') ? ' is-invalid' : '' }}" required name="father_name"  >
														@if ($errors->has('father_name'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('father_name') }}</strong>
                                    						</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<div class="col-xs-6">
														<label class="form-control-label"><h4>CNIC Number</h4></label>
														<input style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="36203-12345678-7" id="cnic" type="text" class="input-mask-cnic form-control{{ $errors->has('cnic') ? ' is-invalid' : '' }}" required name="cnic" >
														@if ($errors->has('cnic'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('cnic') }}</strong>
                                    						</span>
														@endif
													</div>
													<div class="col-xs-6">
														<label class="form-control-label"><h4>Mobile Number</h4></label>
														<input  style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="(92)-300-1234567"  id="mobile" type="text"  class="input-mask-phone form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" required name="mobile"  >
														{{--<input class="form-control input-mask-phone" type="text" id="form-field-mask-2" />--}}
														@if ($errors->has('mobile'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('mobile') }}</strong>
                                    						</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label for="email" class="col-xs-12 col-form-label text-md-right"><h4>Address</h4></label>
													<div class="col-xs-12">
														<textarea style="text-transform: capitalize;
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="Enter Address" rows="3" id="address" type="number" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address"  required></textarea>
														@if ($errors->has('address'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('address') }}</strong>
                                    						</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<div class="col-md-3 col-xs-6">
														<label  for="way_of_income"><h4>Way of Income</h4></label>
														<input style="text-transform: capitalize;height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="Enter Way of Income" id="way_of_income" type="text" class="form-control{{ $errors->has('way_of_income') ? ' is-invalid' : '' }}" name="way_of_income"  required>
														@if ($errors->has('way_of_income'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('way_of_income') }}</strong>
                                    						</span>
														@endif
													</div>
													<div class="col-md-3 col-xs-6">
														<label  for="email"><h4>House Member</h4></label>
														<input style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="0" id="house_member" type="number" class="form-control{{ $errors->has('house_member') ? ' is-invalid' : '' }}" name="house_member"  required>
														@if ($errors->has('house_member'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('house_member') }}</strong>
                                    						</span>
														@endif
													</div>
													<div class="col-md-3 col-xs-6">
														<label  for="male"><h4>Male</h4></label>
														<input style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="0" id="male" type="number" class="form-control{{ $errors->has('male') ? ' is-invalid' : '' }}" name="male"  required>
														@if ($errors->has('male'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('male') }}</strong>
                                    						</span>
														@endif
													</div>
													<div class="col-md-3 col-xs-6">
														<label  for="female" class="col-xs-12 col-form-label text-md-right"><h4>Female</h4></label>
														<input style="margin-top:20px; text-transform: capitalize;height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="0" id="female" type="number" class="form-control{{ $errors->has('female') ? ' is-invalid' : '' }}" name="female"  required>
														@if ($errors->has('female'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('female') }}</strong>
                                    						</span>
														@endif
													</div>

												</div>

												<div class="form-group row">
													<div class="col-md-3 col-xs-12">
														<label class="form-control-label"><h4>Less than 5 year Children</h4></label>
														<input style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="0" id="less_five_year_child" type="number" required class="form-control{{ $errors->has('less_five_year_child') ? ' is-invalid' : '' }}" name="less_five_year_child" >
														@if ($errors->has('less_five_year_child'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('less_five_year_child') }}</strong>
                                    						</span>
														@endif
													</div>
													<div class="col-md-3 col-xs-12">
														<label class="form-control-label"><h4>Number of School Children</h4></label>
														<input style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="0" id="school_child" type="number" class="form-control{{ $errors->has('school_child') ? ' is-invalid' : '' }}" name="school_child" required >
														@if ($errors->has('school_child'))
															<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('school_child') }}</strong>
                                    						</span>
														@endif
													</div>
												</div>
												{{--add-coonsumer--}}
												{{--<div class="form-group row mb-0">--}}
													{{--<div class="col-xs-12">--}}
														{{--<input type="submit" class="btn btn-primary" value="Finish & Save">--}}
													{{--</div>--}}
												{{--</div>--}}




											</div>

											<div style="margin-left: -11px;margin-top: -32px;"class="col-lg-3">
												<ul class="list-group row">
													<li class="list-group-item active"><h5>Take Picture</h5></li>
													<li class="list-group-item">
														<div style="text-align: center" class="mb-3">

																<input id="capimg" hidden  type="text" value=""    name="thumbnail"  >

															<div id="my_camera"></div>

															<!-- First, include the Webcam.js JavaScript Library -->
															<script type="text/javascript" src="{{asset('assets/js/webcam.min.js')}}"></script>

															<!-- Configure a few settings and attach camera -->
															<script language="JavaScript">
                                                                Webcam.set({
//                                                                    width: 320,
                                                                    height: 240,
                                                                    image_format: 'jpeg',
                                                                    jpeg_quality: 90
                                                                });
                                                                Webcam.attach( '#my_camera' );
															</script>

															<!-- A button for taking snaps -->
															<form>
																<input id="take_pic" style="margin-top: 10px" class="btn btn-primary btn-sm" type=button value="Take Snapshot" onClick="take_snapshot()">
															</form>

															<!-- Code to handle taking the snapshot and displaying it locally -->
															<script language="JavaScript">
                                                                function take_snapshot() {
                                                                    // take snapshot and get image data
                                                                    Webcam.snap( function(data_uri) {
                                                                        // display results in page
                                                                        document.getElementById('results').innerHTML =
                                                                            '<h2></h2>' +
                                                                            '<img style="height: 94px;margin-top: -37px; margin-right: 13px;  border-radius: 10%;" type="file" name="" src="'+data_uri+'"/>';
                                                                        document.getElementById('result_check').innerHTML =
                                                                            '<h2></h2>' +
                                                                            '<img style="height: 94px;margin-top: -63px; margin-right: 13px;  border-radius: 10%;" type="file" name="" src="'+data_uri+'"/>';
//                                                                        alert('ok');
//                                                                        $('#capimg').value('ok');
                                                                        document.getElementById("capimg").value = data_uri;
                                                                        document.getElementById("capimg2").value = data_uri;
                                                                    } );


//                                                                    margin-top: -37px;
//                                                                    margin-right: 13px;
//                                                                    border-radius: 10%;

                                                                }

															</script>

														</div>
														{{--<div class="img-thumbnail text-center">--}}
															{{--<img src="@if(isset($user)) {{asset('assets/uploads/labour-pics/'.$user->img)}} @else {{asset('assets/images/no-thumbnail.jpeg')}} @endif" id="imgthumbnail" class="img-thumbnail" alt="">--}}
														{{--</div>--}}
													</li>

												</ul>


											</div>
										</div>
									</form>

									<div class="form-group row mb-0">
										<div class="col-xs-12">
											<button id="check_consumer" style="margin-bottom: 15px" class="btn-info btn-lg pull-left b" data-toggle="modal" data-target="#add-coonsumer">
												Save
											</button>
										</div>
									</div>

								</div>
							</div><!-- /.span -->
						</div><!-- /.row -->

						<div class="hr hr-18 dotted hr-double"></div>

						<!-- PAGE CONTENT ENDS -->
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.page-content -->
		</div>
	</div>


	{{--Add modal part--}}
	<div class="modal fade" id="add-coonsumer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form  action="{{route('consumer-save')}}"  method="post" accept-charset="utf-8" enctype="multipart/form-data">
					@csrf
					<input id="capimg2" hidden  type="text" value=""    name="thumbnail">
					<div class="modal-header">
						<button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
						<u style="color:#858585 "><h1 style="font-size: 39px;
    											color: #858585;">Consumer Registration</h1></u>
					</div>
					<div class="modal-body">
						<div class="row">
							@csrf
							<div class="col-lg-12">
								<div class="row">
									<label style="margin-top:20px;" for="form_no" class="col-xs-10 col-form-label text-md-right"><h4>Form Number</h4></label>
									<div class="row">
									<div class="col-md-10">
										<div class="col-md-10">
										<input style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="Enter Form Number" id="form_no2" type="text" class="form-control" name="form_no"  required>
										</div>
										<div class="col-md-2">
											<div id="result_check"></div>
										</div>
									</div>
									</div>
								</div>



								<div class="form-group row">
									<div class="col-md-12">
										<label class="form-control-label"><h4>Name</h4></label>
										<input style="text-transform: capitalize;height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="Enter Name" id="name2" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required name="name" >
										@if ($errors->has('name'))
											<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('name') }}</strong>
                                    						</span>
										@endif
									</div>
									<div class="col-md-12">
										<label class="form-control-label"><h4>Father Name</h4></label>
										<input style="text-transform: capitalize;height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="Enter Father Name" id="father_name2" type="text" class="form-control{{ $errors->has('father_name') ? ' is-invalid' : '' }}" required name="father_name"  >
										@if ($errors->has('father_name'))
											<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('father_name') }}</strong>
                                    						</span>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<div class="col-xs-6">
										<label class="form-control-label"><h4>CNIC Number</h4></label>
										<input style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="36203-12345678-7" id="cnic2" type="text" class="input-mask-cnic form-control{{ $errors->has('cnic') ? ' is-invalid' : '' }}" required name="cnic" >
										@if ($errors->has('cnic'))
											<span class="invalid-feedback">
                                        					<strong style="color:red">{{ $errors->first('cnic') }}</strong>
                                    						</span>
										@endif
									</div>
									<div class="col-xs-6">
										<label class="form-control-label"><h4>Mobile Number</h4></label>
										<input style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="03001234567" id="mobile2" type="text" class="input-mask-phone form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" required name="mobile"  >
										@if ($errors->has('mobile'))
											<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('mobile') }}</strong>
                                    						</span>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label for="email" class="col-xs-12 col-form-label text-md-right"><h4>Address</h4></label>
									<div class="col-xs-12">
														<textarea style="text-transform: capitalize;
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="Enter Address" rows="3" id="address2" type="number" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address"  required></textarea>
										@if ($errors->has('address'))
											<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('address') }}</strong>
                                    						</span>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<div  class="col-md-6 ">
										<label style="margin-top:20px;" for="email"><h4>District</h4></label>
										<select style="text-transform: capitalize;height: calc(4.875rem + 2px);
																padding: .5rem 1rem;
																font-size: 1.9rem;
																line-height: 1.5;
																border-radius: 0.6rem !important; " id="district2" type="text" class="form-control" name="distric_id"  required>
											<option value="" hidden>Select Distric Name</option>
											@foreach($districs as $distric)
												<option style="text-transform: capitalize" value="{{$distric->id}}" >{{$distric->name}}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-6 ">
										<label style="margin-top:20px;" for="email"><h4>Tehsil</h4></label>
										<select style="text-transform: capitalize;height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;"  id="tehsil2" type="text" class="form-control" name="tehsil_id"  required>
											<option value="" hidden>Select Tehsil Name</option>
											@foreach($tehsils as $tehsil)
												<option style="text-transform: capitalize" value="{{$tehsil->id}}" >{{$tehsil->name}}</option>
											@endforeach
										</select>
									</div>

									<div class="col-md-6 ">
										<label style="margin-top:20px;" for="union_council"><h4>Union Council</h4></label>
										<select style="text-transform: capitalize;height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;"  id="concil2" type="text" class="form-control" name="union_council_id"  required>
											<option value="" hidden>Select Union Council Name</option>
											@foreach($concils as $concil)
												<option style="text-transform: capitalize" value="{{$concil->id}}" >{{$concil->name}}</option>
											@endforeach
										</select>
									</div>

									<div class="col-md-6 ">
										<label style="margin-top:20px;" for="village"><h4>Village</h4></label>
										<input style="text-transform: capitalize;height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="Enter Village Name" id="village2" type="text" class="form-control{{ $errors->has('village') ? ' is-invalid' : '' }}" name="village"  required>
										@if ($errors->has('village'))
											<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('village') }}</strong>
                                    						</span>
										@endif
									</div>

								</div>

								<div class="form-group row">
									<div class="col-md-6">
										<label  for="way_of_income"><h4>Way of Income</h4></label>
										<input style="text-transform: capitalize;height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="Enter Way of Income" id="way_of_income2" type="text" class="form-control{{ $errors->has('way_of_income') ? ' is-invalid' : '' }}" name="way_of_income"  required>
										@if ($errors->has('way_of_income'))
											<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('way_of_income') }}</strong>
                                    						</span>
										@endif
									</div>
									<div class="col-md-6">
										<label  for="email"><h4>House Member</h4></label>
										<input style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="0" id="house_member2" type="number" class="form-control{{ $errors->has('house_member') ? ' is-invalid' : '' }}" name="house_member"  required>
										@if ($errors->has('house_member'))
											<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('house_member') }}</strong>
                                    						</span>
										@endif
									</div>
									<div class="col-md-6">
										<label  for="male"><h4>Male</h4></label>
										<input style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="0" id="male2" type="number" class="form-control{{ $errors->has('male') ? ' is-invalid' : '' }}" name="male"  required>
										@if ($errors->has('male'))
											<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('male') }}</strong>
                                    						</span>
										@endif
									</div>
									<div class="col-md-6">
										<label  for="female" class="col-xs-12 col-form-label text-md-right"><h4>Female</h4></label>
										<input style="margin-top:20px; text-transform: capitalize;height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="0" id="female2" type="number" class="form-control{{ $errors->has('female') ? ' is-invalid' : '' }}" name="female"  required>
										@if ($errors->has('female'))
											<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('female') }}</strong>
                                    						</span>
										@endif
									</div>

								</div>

								<div class="form-group row">
									<div class="col-md-6">
										<label class="form-control-label"><h4>Less than 5 year Children</h4></label>
										<input style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="0" id="less_five_year_child2" type="number" required class="form-control{{ $errors->has('less_five_year_child') ? ' is-invalid' : '' }}" name="less_five_year_child" >
										@if ($errors->has('less_five_year_child'))
											<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('less_five_year_child') }}</strong>
                                    						</span>
										@endif
									</div>
									<div class="col-md-6">
										<label class="form-control-label"><h4>Number of School Children</h4></label>
										<input style="height: calc(4.875rem + 2px);
															padding: .5rem 1rem;
															font-size: 1.9rem;
															line-height: 1.5;
															border-radius: 0.6rem !important;" placeholder="0" id="school_child2" type="number" class="form-control{{ $errors->has('school_child') ? ' is-invalid' : '' }}" name="school_child" required >
										@if ($errors->has('school_child'))
											<span class="invalid-feedback">
                                        					<strong>{{ $errors->first('school_child') }}</strong>
                                    						</span>
										@endif
									</div>
								</div>



							</div>

						</div>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Finish & Save">
						<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>

@endsection

@section('script')
	<script src="{{asset('assets/js/jquery.maskedinput.min.js')}}"></script>
	<script type="text/javascript">
        jQuery(function($) {
			autosize($('textarea[class*=autosize]'));

            $('textarea.limited').inputlimiter({
                remText: '%n character%s remaining...',
                limitText: 'max allowed : %n.'
            });

            $.mask.definitions['~']='[+-]';
            $('.input-mask-date').mask('99/99/9999');
//            $('.input-mask-phone').cursor(false);
            $('.input-mask-phone').mask('(99) 999-9999999');

            $('.input-mask-cnic').mask('99999-9999999-9');
            $('.input-mask-eyescript').mask('~9.99 ~9.99 999');
            $(".input-mask-product").mask("a*-999-a999",{placeholder:" ",completed:function(){alert("You typed the following: "+this.val());}});


        });
	</script>
	<script>
        $('#summernote').summernote({
            height: 100
        });

        $('#check_consumer').click(function () {
           var frm= $('#form_no').val();
            $('#form_no2').val(frm);
            var mame2= $('#name').val();
            $('#name2').val(mame2);
            var father_name2= $('#father_name').val();
            $('#father_name2').val(father_name2);
            var cnic2= $('#cnic').val();
            $('#cnic2').val(cnic2);
            var mobile2= $('#mobile').val();
            $('#mobile2').val(mobile2);
            var address2= $('#address').val();
            $('#address2').val(address2);
            var way_of_income2= $('#way_of_income').val();
            $('#way_of_income2').val(way_of_income2);
            var house_member2= $('#house_member').val();
            $('#house_member2').val(house_member2);
            var male2= $('#male').val();
            $('#male2').val(male2);
            var female2= $('#female').val();
            $('#female2').val(female2);
            var less_five_year_child2= $('#less_five_year_child').val();
            $('#less_five_year_child2').val(less_five_year_child2);
            var school_child2= $('#school_child').val();
            $('#school_child2').val(school_child2);

            var district2= $('#district').val();
            $('#district2').val(district2);
            var tehsil2= $('#tehsil').val();
            $('#tehsil2').val(tehsil2);
			var concil2= $('#concil').val();
            $('#concil2').val(concil2);
            var village2= $('#village').val();
            $('#village2').val(village2);







//            var code=($(this)).val();
////            alert(code);
//            $('#barcd').text(code);
//            $.ajaxSetup({
//                headers: {
//                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                }
//            });
//            $.ajax({
//				/* the route pointing to the post function */
////                url: '/scane',
//                url: '/scane/?id=' + code,
//                type: 'POST',
//				/* send the csrf-token and the input to the controller */
//                data: {},
//                dataType: 'JSON',
//				/* remind that 'data' is the response of the AjaxController */
//                success: function (data) {
//                    $(".writeinfo").append(data.msg);
//                }
//            });
        });


        //        $(".select").chosen({max_selected_options: 5});
        $(".select").chosen({width: "100%"});

        $('.slugify').keyup(function () {
            var url=($(this)).val();
//          str = url.split(" ").join("-").toLowerCase();
            str = url.replace(/\s+/g, '-').toLowerCase();
            $('#slug-text').text(str);
            $('#slug_title').val(str);
        });

        //        $('#title').on('keyup',(function () {
        //            $('#slug-text').text('ok');
        //        });
        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-success').fadeOut('slow');
            }, 3000);
        });

//        $('#id-input-file-2').on('change', function() {
//            var file = $(this).get(0).files;
//            var reader = new FileReader();
//            reader.readAsDataURL(file[0]);
//            reader.addEventListener("load", function(e) {
//                var image = e.target.result;
//                $("#imgthumbnail").attr('src', image);
//            });
//        });






	</script>

@endsection