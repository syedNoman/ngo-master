@extends('admin.layouts.webapp')
@section('content')
	<style>

	</style>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="page-content">


				<div class="row">
					<div class="col-xs-12">
						<!-- PAGE CONTENT BEGINS -->
						<div class="row">
							<div class="col-xs-12">
								{{--header--}}
								<div class="col-md-12" class="card-body">
									<div style="height:auto; box-shadow: 1px 1px 6px -1px #ccc;" class="col-md-12">
										<div class="row">
											<div class="col-md-2 col-xs-12">
											<img style="  border-radius: 43px; height: 202px; padding: 24px;" src="{{asset('assets/images/panisab.jpg')}}">
											</div>
											<div class="col-md-7 col-xs-12">
												<p style="margin-top: 37px">
												<h1>PANI SAB KAY LIYE</h1>
												<h4>A Project Of Rasheeda Trust</h4>
												</p>
											</div>
											<div class="col-md-3 col-xs-12">
											<img style="float: right;
											height: 102px;
											padding: 20px;
											margin-top: 41px;" src="{{asset('assets/images/rasheedalogo.png')}}">
											</div>
										</div>
									</div>
									{{--End header--}}
									<div style="margin-top: 25px;
    box-shadow: 0px 0px 8px 2px #ccc;height: 670px;" class="col-md-7 col-xs-12">


											<div class="row">
												<div  class="col-md-12">
												<div class="form-group col-md-12">

													<h2 style="color:#2B3D53">WATER COUNSUMPATION REPORT</h2>

													<label  style="height: auto;
    font-size: 25px;
    padding: 16px;
    margin-top: 37px;" type="text" class="form-control form-control-lg">Water Consumed Today <h3 style="float: right;"><span id="today_total_consumer_serve"> {{ $sum_total_consumer_serve = DB::table('consumptions')->pluck('today_total_consumer_serve')->sum()}} </span>	<span style="margin-left: 7px">Liters</span></h3></label>
												</div>
													<div class="form-group col-md-12">
													<label style="height: auto;
    font-size: 25px;
    padding: 16px;" type="text" class="form-control form-control-lg">Total Water Consumed <h3 style="float: right; color: red"><span id="total_consumpation"> {{ $sum_total_consumer_serve = DB::table('consumptions')->where('plant_id',Auth::user()->plant_id)->pluck('total_consumer_serve')->sum()}} </span> <span style="margin-left: 7px">Liters</span></h3></label>


													</div>
													<button style="margin-top:23%;margin-left: 10px;border-radius: 30px" id="crn-update" class="btn btn-danger">Start New Day</button>
												</div>


											</div>

									</div>
									<div style="margin-top: 25px;
        	box-shadow: 2px 2px 40px #ccc;height: 670px;" class="col-md-5 col-xs-12">
                                      <p><h2 style="padding: 10px;color: #8c8c8c">Currently Serving</h2></p>
										<div class="row">
											<div class="col-md-12">

												<div class="col-md-10">
												<input id="" class="scan_input form-control" autofocus required  placeholder="Scan Barcode Here" type="text">
												</div>
												<div calass="col-md-3 col-xs-6">
												<input id="scane_btn" type="submit" class=" btn btn-success btn-sm" value="check">
												</div>


												<div class="col-md-4 col-xs-12">

													<div id="cons_pic">
														<img style="  border-radius: 43px; height: 202px; padding: 24px;" src="{{asset('assets/images/panisab.jpg')}}">
													</div>

												</div>

												<div class="col-md-6 col-xs-12">
													{{--<img style="  border-radius: 43px; height: 202px; padding: 24px;" src="{{asset('assets/images/panisab.jpg')}}">--}}
													<p><h1 id="cons_name">------</h1>
													<h3>Water Consume : <span style="color: #8c8c8c"><span  class="consmp_ser_water">0</span> Liters</span></h3>

													<h3>Status <a id="show_serve_btn_serve" style=" display: none; text-decoration: none!important;
															background: #87B87F!important;
															color: white;
															padding: 5px;
															border-radius: 10px;
															font-size: 16px;" class="success">Water Serve</a>
														<a id="show_already_serve" style=" display: none; text-decoration: none!important;
															background: indianred!important;
															color: white;
															padding: 5px;
															border-radius: 10px;
															font-size: 16px;" class="success">Already Serve</a>
														<a id="invaild_btn" style=" display: none; text-decoration: none!important;
															background: indianred!important;
															color: white;
															padding: 5px;
															border-radius: 10px;
															font-size: 16px;" class="success">Invalid Card</a>
													</h3>
													<h3>Total Water Consume : <span style="color: #8c8c8c"><span id="total_consmp_ser_water">0</span> Liters</span></h3>
													{{--<h3 id="barcd">----</h3></p>--}}
												</div>

											</div>
										</div>
									</div>



								</div>
								{{--End header--}}
							</div><!-- /.span -->
						</div><!-- /.row -->

						<div class="hr hr-18 dotted hr-double"></div>

						<!-- PAGE CONTENT ENDS -->
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.page-content -->
		</div>
	</div>
@endsection

@section('script')
	<script>

		$('#crn-update').click(function () {
            alert('vale');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/crone-update',
                type: 'POST',
				/* send the csrf-token and the input to the controller */
                data: {},
                dataType: 'JSON',

                success:function (result) {

                },

                error:function (resulr) {

                }

            });

        });


//        after 3:30pm reset the values funcation
//        (function showUp(){
//			var date=new Date();
////				after 3:30pm reset the values
//			if(date.getHours() >= 3 && date.getMinutes() <= 30){
////                alert(date.getMinutes());
//			    //8-9AM
//                $.ajaxSetup({
//                    headers: {
//                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                    }
//                });
//                $.ajax({
//                    url: '/crone-update',
//                    type: 'POST',
//					/* send the csrf-token and the input to the controller */
//                    data: {},
//                    dataType: 'JSON',
//
//                    success:function (result) {
//
//                    },
//
//                    error:function (resulr) {
//
//                    }
//
//                });
//			}
//			setTimeout(showUp, 3000);
//		})();


        $('.scan_input').change(function () {

            var val = $('.consmp_ser_water').text();
              var code=($(this)).val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: '/scane',
					type: 'POST',
                    data:{ id :code},
                    dataType: 'JSON',
					/* remind that 'data' is the response of the AjaxController */
//                success: function (data) {
//                    $(".writeinfo").append(data.msg);
//                }

                    success:
                        function (result) {
//                        result = JSON.parse(result);
                            $('#cons_name').html(result.options);
                            $('#cons_pic').html(result.cons_pics);

                            $('#sum_total_consumer_serve').html(result.sum_total_consumer_serve);
                            $('#today_total_consumer_serve').html(result.today_total_consumer_serve);
                            $('#total_consumpation').html(result.total_consumpation);
//                            var check_user_consume=$('.consmp_ser_water').text(result.consmp_ser_water);
                            if(result.invalid_card == 0){
                                $('.consmp_ser_water').text('0');
                                $('#invaild_btn').show();
                                $('#cons_name').html('');
                                $('#cons_pic').html('');
                                $('#total_consmp_ser_water').html('0');

                                $('#show_already_serve').hide();
                                $('#show_serve_btn_serve').hide();

                            }

                            if(result.consmp_ser_water == 0){
                                $('#show_already_serve').show();
                                $('#invaild_btn').hide();
                                $('#show_serve_btn_serve').hide();

							}

                            if(result.consmp_ser_water == 19){
                                $('#show_serve_btn_serve').show();
                                $('#invaild_btn').hide();
                                $('#show_already_serve').hide();
                            }


                            $('#total_consmp_ser_water').html(result.total_consmp_ser_water);
                            $('.scan_input').val('');

                        },
                    error: function (result) {
//                    alert('error this form');
                    }

                });



            });

//            var code=($(this)).val();
////            alert(code);
//            $('#barcd').text(code);
//           //        });
//        $('#scane_btn').click(function () {
//            var code=($(this)).val();
////            alert(code);
//            $('#barcd').text(code);
//            $.ajaxSetup({
//                headers: {
//                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                }
//            });
//            $.ajax({
//				/* the route pointing to the post function */
////                url: '/scane',
//                url: '/scane/?id=' + code,
//                type: 'POST',
//				/* send the csrf-token and the input to the controller */
//                data: {},
//                dataType: 'JSON',
//				/* remind that 'data' is the response of the AjaxController */
////                success: function (data) {
////                    $(".writeinfo").append(data.msg);
////                }
//
//                success:
//                    function (result) {
////                        result = JSON.parse(result);
//                        $('#cons_name').html(result.options);
//                        $('#cons_pic').html(result.cons_pics);
//                        $('.scan_input').val('');
//
//                    },
//                error: function (result) {
////                    alert('error this form');
//                }
//
//            });
//        });






	</script>

@endsection