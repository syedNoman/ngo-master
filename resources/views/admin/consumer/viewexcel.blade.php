
<h2 style="text-align: center">Rasheeda Trust Registration Info For Card</h2>
<table>
	<thead>
	<tr>

		<th style="text-align: center" >Name</th>
		<th style="text-align: center" >Father</th>
		<th style="text-align: center" >CNIC</th>
		<th style="text-align: center" >Barcode</th>
	</tr>
	</thead>
	<tbody>
	@foreach($users as $user)
		<tr style="width: 20%" >
			<td style="text-align: center" width="20%" >{{ $user->name }}</td>
			<td style="text-align: center" >{{ $user->father_name }}</td>
			<td style="text-align: center" >{{ $user->cnic }}</td>
			<td style="text-align: center" >{{ $user->barcode }}</td>
			<td class="center">
				@if(isset($user->thumbnail))
					{{--{{ $user->thumbnail}}--}}
					<img src="{{ $user->thumbnail }}"
						 width="50px" height="50px">
				@else
					<strong>{{'No Img'}}</strong>
				@endif
			</td>
		</tr>
	@endforeach
	</tbody>
</table>
