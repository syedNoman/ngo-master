@extends('admin.layouts.app2')
@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Districs</h4>

                            {{--<a style="background: linear-gradient(145deg,#ffffff,#ffffff);--}}
                            {{--color: #07cdff;--}}
                            {{--margin-top: -22px;" class="btn pull-right" href="{{route('tds.create')}}">Add TDS</a>--}}
                            @if(Auth::user()->role==1)
                                <a style="cursor: pointer; background: linear-gradient(145deg,#ffffff,#ffffff);
                                color: #07cdff;
                                margin-top: -22px;" class="btn-info btn-sm pull-right b" data-toggle="modal" data-target="#exampleModal">Add
                                    Distric
                                </a>
                            @endif

                            {{--<p class="card-category"> Showing all the TDS</p>--}}
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    @if(Session::has('message'))
                                        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em></div>
                                    @endif
                                    @if(Session::has('message-delete'))
                                        @if(Session::has('message-delete'))
                                            <div class="alert alert-danger"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message-delete') !!}</em></div>
                                        @endif
                                    @endif
                                    <thead class="text-primary">
                                    <tr>
                                        <th style="text-align:center">NO#</th>
                                        <th style="text-align:center">Name</th>
                                        <th style="text-align:center">Province</th>
                                        <th style="text-align:center; width: 20%">Action</th>
                                    </tr>
                                    </thead>
                                        <tbody>
                                        @foreach($districs as $key => $distric)

                                            <tr>
                                                <td style="text-align: center">
                                                    {{ ($districs->currentpage() - 1) * $districs->perpage() + 1 + $key }}

                                                </td>
                                                <td style="text-transform:capitalize;text-align: center">{{ @$distric->name }}</td>
                                                <td style="text-transform:capitalize;text-align: center">{{ @$distric->province->name }}</td>
                                                <td style="text-align: center">
                                                    <div class="hidden-sm hidden-xs btn-group">
                                                        <button title="Update" class="btn btn-xs btn-success edit-btn"
                                                                data-toggle="modal"
                                                                data-action="/distric/{{ $distric->id }}"
                                                                data-target="#exampleModal2">
                                                            <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                        </button>
                                                        <button title="Delete" type="button" data-toggle="modal"
                                                                data-target="#delete-distric"
                                                                data-action="/del-distric/{{ $distric->id }}"
                                                                class="show-delete-modal btn btn-xs btn-danger">
                                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                        @endforeach
                                </table>
                                <div class="text-left">{{ $districs->links() }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Add Module -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div style="background: linear-gradient(145deg, #07cdff, #46e3ff);" class="modal-header">

                    <u style="color:#fff"><h1 style="font-size: 39px;
    											color: #fff;">Add Distric</h1></u>
                    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                </div>
                <form id="form" method="POST" action="{{route('add-distric')}}"
                      enctype="multipart/form-data">
                    <div class="modal-body">
                        {{ csrf_field() }}

                        <div style="margin-top:40px" class="form-group">

                            <input style="text-transform:capitalize;" class="form-control" id="nameid" name="name"
                                   type="text" placeholder="Enter Name"
                                   required>
                        </div>

                        <div style="margin-top:30px" class="form-group">
                            {{--<label for="exampleInputEmail1"><h4>Select Province</h4></label>--}}
                            <select data-placement="Select Provinces" class="form-control"  required name="province_id">
                                <option value="" hidden>Select Provinces</option>
                                @foreach ($provinces as $province)
                                    <option value="{{$province->id}}">{{$province->name}}</option>
                                @endforeach
                            </select>
                        </div>


                    </div>
                    <div style="border: none" class="modal-footer">
                        <button type="button" class="btn btn-dark"
                                data-dismiss="modal">Close
                        </button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{--edit part--}}
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div style="background: linear-gradient(145deg, #07cdff, #46e3ff);" class="modal-header">

                    <u style="color:#fff"><h1 style="font-size: 39px;
    											color: #fff;">Edit
                              Distric</h1></u>
                    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                </div>
                <div id="edit-content">
                </div>
            </div>
        </div>
    </div>
    {{--delete modal part--}}
    <div class="modal fade" id="delete-distric" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST" id="delete-form" action="/del-distric/{{@$distric->id}}">
                    @csrf
                    <div style="background: linear-gradient(367deg, #f44336, #ccc);" class="modal-header">

                        <u style="color:#fff"><h1 style="font-size: 39px;
    											color: #fff;">Delete</h1></u>
                        <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <p> Are you sure you want to delete?</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" type="submit">Delete</button>
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-success').slideUp('');
            }, 2000);
        });
        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-danger').slideUp('');
            }, 2000);
        });
        $('.edit-btn').click(function (e) {
            var request = $.ajax({
                url: $(this).data('action'),
                type: "get",
                dataType: "html",
                cache: false,
                success:
                    function (result) {
                        $('#edit-content').html(result);

                    },
                error: function (result) {
                    alert('error this form');
                }
            });
        });

        // Summernote
        $('#summernote').summernote({
            height: 200
        });
    </script>
@endsection