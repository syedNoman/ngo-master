<form id="editfrom" method="POST" action="/distric/{{ $distric->id }}" enctype="multipart/form-data">
    <div class="modal-body">
        {{ csrf_field() }}

        <div style="margin-top: 40px" class="form-group">

            <input class="form-control" id="nameid" value="{{ $distric->name }}" name="name"
                   type="text" placeholder="Enter Name"
                   required>
        </div>
        <div style="margin-top: 30px" class="form-group">
            {{--<label for="exampleInputEmail1"><h4>Select Province</h4></label>--}}
            <select class="form-control" name="province_id" required>
                <option value="" hidden>Select Province </option>
                @foreach ($provinces as $province)
                    <option @if($distric->province_id == $province->id ) selected @endif  value="{{$province->id}}">{{$province->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div style="border: none" class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
</form>


