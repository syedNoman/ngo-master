@extends('admin.layouts.app2')
@section('content')

    <style>
        .chosen-select {
            width: 100%;
        }
        .chosen-select-deselect {
            width: 100%;
        }
        .chosen-container {
            display: inline-block;
            font-size: 14px;
            position: relative;
            vertical-align: middle;
        }
        .chosen-container .chosen-drop {
            background: #ffffff;
            border: 1px solid #cccccc;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px;
            -webkit-box-shadow: 0 8px 8px rgba(0, 0, 0, .25);
            box-shadow: 0 8px 8px rgba(0, 0, 0, .25);
            margin-top: -1px;
            position: absolute;
            top: 100%;
            left: -9000px;
            z-index: 1060;
        }
        .chosen-container.chosen-with-drop .chosen-drop {
            left: 0;
            right: 0;
        }
        .chosen-container .chosen-results {
            color: #555555;
            margin: 0 4px 4px 0;
            max-height: 240px;
            padding: 0 0 0 4px;
            position: relative;
            overflow-x: hidden;
            overflow-y: auto;
            -webkit-overflow-scrolling: touch;
        }
        .chosen-container .chosen-results li {
            display: none;
            line-height: 1.42857143;
            list-style: none;
            margin: 0;
            padding: 5px 6px;
        }
        .chosen-container .chosen-results li em {
            background: #feffde;
            font-style: normal;
        }
        .chosen-container .chosen-results li.group-result {
            display: list-item;
            cursor: default;
            color: #999;
            font-weight: bold;
        }
        .chosen-container .chosen-results li.group-option {
            padding-left: 15px;
        }
        .chosen-container .chosen-results li.active-result {
            cursor: pointer;
            display: list-item;
        }
        .chosen-container .chosen-results li.highlighted {
            background-color: #428bca;
            background-image: none;
            color: white;
        }
        .chosen-container .chosen-results li.highlighted em {
            background: transparent;
        }
        .chosen-container .chosen-results li.disabled-result {
            display: list-item;
            color: #777777;
        }
        .chosen-container .chosen-results .no-results {
            background: #eeeeee;
            display: list-item;
        }
        .chosen-container .chosen-results-scroll {
            background: white;
            margin: 0 4px;
            position: absolute;
            text-align: center;
            width: 321px;
            z-index: 1;
        }
        .chosen-container .chosen-results-scroll span {
            display: inline-block;
            height: 1.42857143;
            text-indent: -5000px;
            width: 9px;
        }
        .chosen-container .chosen-results-scroll-down {
            bottom: 0;
        }
        .chosen-container .chosen-results-scroll-down span {
            background: url("chosen-sprite.png") no-repeat -4px -3px;
        }
        .chosen-container .chosen-results-scroll-up span {
            background: url("chosen-sprite.png") no-repeat -22px -3px;
        }
        .chosen-container-single .chosen-single {
            background-color: #ffffff;
            -webkit-background-clip: padding-box;
            -moz-background-clip: padding;
            background-clip: padding-box;
            border: 1px solid #cccccc;
            border-top-right-radius: 4px;
            border-top-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            color: #555555;
            display: block;
            height: 34px;
            overflow: hidden;
            line-height: 34px;
            padding: 0 0 0 8px;
            position: relative;
            text-decoration: none;
            white-space: nowrap;
        }
        .chosen-container-single .chosen-single span {
            display: block;
            margin-right: 26px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
        .chosen-container-single .chosen-single abbr {
            background: url("chosen-sprite.png") right top no-repeat;
            display: block;
            font-size: 1px;
            height: 10px;
            position: absolute;
            right: 26px;
            top: 12px;
            width: 12px;
        }
        .chosen-container-single .chosen-single abbr:hover {
            background-position: right -11px;
        }
        .chosen-container-single .chosen-single.chosen-disabled .chosen-single abbr:hover {
            background-position: right 2px;
        }
        .chosen-container-single .chosen-single div {
            display: block;
            height: 100%;
            position: absolute;
            top: 0;
            right: 0;
            width: 18px;
        }
        .chosen-container-single .chosen-single div b {
            background: url("chosen-sprite.png") no-repeat 0 7px;
            display: block;
            height: 100%;
            width: 100%;
        }
        .chosen-container-single .chosen-default {
            color: #777777;
        }
        .chosen-container-single .chosen-search {
            margin: 0;
            padding: 3px 4px;
            position: relative;
            white-space: nowrap;
            z-index: 1000;
        }
        .chosen-container-single .chosen-search input[type="text"] {
            background: url("chosen-sprite.png") no-repeat 100% -20px, #ffffff;
            border: 1px solid #cccccc;
            border-top-right-radius: 4px;
            border-top-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            margin: 1px 0;
            padding: 4px 20px 4px 4px;
            width: 100%;
        }
        .chosen-container-single .chosen-drop {
            margin-top: -1px;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px;
            -webkit-background-clip: padding-box;
            -moz-background-clip: padding;
            background-clip: padding-box;
        }
        .chosen-container-single-nosearch .chosen-search input {
            position: absolute;
            left: -9000px;
        }
        .chosen-container-multi .chosen-choices {
            background-color: #ffffff;
            border: 1px solid #cccccc;
            border-top-right-radius: 4px;
            border-top-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            cursor: text;
            height: auto !important;
            height: 1%;
            margin: 0;
            overflow: hidden;
            padding: 0;
            position: relative;
        }
        .chosen-container-multi .chosen-choices li {
            float: left;
            list-style: none;
        }
        .chosen-container-multi .chosen-choices .search-field {
            margin: 0;
            padding: 0;
            white-space: nowrap;
        }
        .chosen-container-multi .chosen-choices .search-field input[type="text"] {
            background: transparent !important;
            border: 0 !important;
            -webkit-box-shadow: none;
            box-shadow: none;
            color: #555555;
            height: 32px;
            margin: 0;
            padding: 4px;
            outline: 0;
        }
        .chosen-container-multi .chosen-choices .search-field .default {
            color: #999;
        }
        .chosen-container-multi .chosen-choices .search-choice {
            -webkit-background-clip: padding-box;
            -moz-background-clip: padding;
            background-clip: padding-box;
            background-color: #eeeeee;
            border: 1px solid #cccccc;
            border-top-right-radius: 4px;
            border-top-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px;
            background-image: -webkit-linear-gradient(top, #ffffff 0%, #eeeeee 100%);
            background-image: -o-linear-gradient(top, #ffffff 0%, #eeeeee 100%);
            background-image: linear-gradient(to bottom, #ffffff 0%, #eeeeee 100%);
            background-repeat: repeat-x;
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffeeeeee', GradientType=0);
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            color: #333333;
            cursor: default;
            line-height: 13px;
            margin: 6px 0 3px 5px;
            padding: 3px 20px 3px 5px;
            position: relative;
        }
        .chosen-container-multi .chosen-choices .search-choice .search-choice-close {
            background: url("chosen-sprite.png") right top no-repeat;
            display: block;
            font-size: 1px;
            height: 10px;
            position: absolute;
            right: 4px;
            top: 5px;
            width: 12px;
            cursor: pointer;
        }
        .chosen-container-multi .chosen-choices .search-choice .search-choice-close:hover {
            background-position: right -11px;
        }
        .chosen-container-multi .chosen-choices .search-choice-focus {
            background: #d4d4d4;
        }
        .chosen-container-multi .chosen-choices .search-choice-focus .search-choice-close {
            background-position: right -11px;
        }
        .chosen-container-multi .chosen-results {
            margin: 0 0 0 0;
            padding: 0;
        }
        .chosen-container-multi .chosen-drop .result-selected {
            display: none;
        }
        .chosen-container-active .chosen-single {
            border: 1px solid #66afe9;
            -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .075) inset, 0 0 8px rgba(82, 168, 236, .6);
            box-shadow: 0 1px 1px rgba(0, 0, 0, .075) inset, 0 0 8px rgba(82, 168, 236, .6);
            -webkit-transition: border linear .2s, box-shadow linear .2s;
            -o-transition: border linear .2s, box-shadow linear .2s;
            transition: border linear .2s, box-shadow linear .2s;
        }
        .chosen-container-active.chosen-with-drop .chosen-single {
            background-color: #ffffff;
            border: 1px solid #66afe9;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
            -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .075) inset, 0 0 8px rgba(82, 168, 236, .6);
            box-shadow: 0 1px 1px rgba(0, 0, 0, .075) inset, 0 0 8px rgba(82, 168, 236, .6);
            -webkit-transition: border linear .2s, box-shadow linear .2s;
            -o-transition: border linear .2s, box-shadow linear .2s;
            transition: border linear .2s, box-shadow linear .2s;
        }
        .chosen-container-active.chosen-with-drop .chosen-single div {
            background: transparent;
            border-left: none;
        }
        .chosen-container-active.chosen-with-drop .chosen-single div b {
            background-position: -18px 7px;
        }
        .chosen-container-active .chosen-choices {
            border: 1px solid #66afe9;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
            -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .075) inset, 0 0 8px rgba(82, 168, 236, .6);
            box-shadow: 0 1px 1px rgba(0, 0, 0, .075) inset, 0 0 8px rgba(82, 168, 236, .6);
            -webkit-transition: border linear .2s, box-shadow linear .2s;
            -o-transition: border linear .2s, box-shadow linear .2s;
            transition: border linear .2s, box-shadow linear .2s;
        }
        .chosen-container-active .chosen-choices .search-field input[type="text"] {
            color: #111 !important;
        }
        .chosen-container-active.chosen-with-drop .chosen-choices {
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .chosen-disabled {
            cursor: default;
            opacity: 0.5 !important;
        }
        .chosen-disabled .chosen-single {
            cursor: default;
        }
        .chosen-disabled .chosen-choices .search-choice .search-choice-close {
            cursor: default;
        }
        .chosen-rtl {
            text-align: right;
        }
        .chosen-rtl .chosen-single {
            padding: 0 8px 0 0;
            overflow: visible;
        }
        .chosen-rtl .chosen-single span {
            margin-left: 26px;
            margin-right: 0;
            direction: rtl;
        }
        .chosen-rtl .chosen-single div {
            left: 7px;
            right: auto;
        }
        .chosen-rtl .chosen-single abbr {
            left: 26px;
            right: auto;
        }
        .chosen-rtl .chosen-choices .search-field input[type="text"] {
            direction: rtl;
        }
        .chosen-rtl .chosen-choices li {
            float: right;
        }
        .chosen-rtl .chosen-choices .search-choice {
            margin: 6px 5px 3px 0;
            padding: 3px 5px 3px 19px;
        }
        .chosen-rtl .chosen-choices .search-choice .search-choice-close {
            background-position: right top;
            left: 4px;
            right: auto;
        }
        .chosen-rtl.chosen-container-single .chosen-results {
            margin: 0 0 4px 4px;
            padding: 0 4px 0 0;
        }
        .chosen-rtl .chosen-results .group-option {
            padding-left: 0;
            padding-right: 15px;
        }
        .chosen-rtl.chosen-container-active.chosen-with-drop .chosen-single div {
            border-right: none;
        }
        .chosen-rtl .chosen-search input[type="text"] {
            background: url("chosen-sprite.png") no-repeat -28px -20px, #ffffff;
            direction: rtl;
            padding: 4px 5px 4px 20px;
        }
        @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min-resolution: 144dpi) {
            .chosen-rtl .chosen-search input[type="text"],
            .chosen-container-single .chosen-single abbr,
            .chosen-container-single .chosen-single div b,
            .chosen-container-single .chosen-search input[type="text"],
            .chosen-container-multi .chosen-choices .search-choice .search-choice-close,
            .chosen-container .chosen-results-scroll-down span,
            .chosen-container .chosen-results-scroll-up span {
                background-image: url("chosen-sprite@2x.png") !important;
                background-size: 52px 37px !important;
                background-repeat: no-repeat !important;
            }
        }
    </style>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Complaints</h4>

                            <a style="background: linear-gradient(145deg,#ffffff,#ffffff);
                                color: #07cdff;
                                margin-top: -22px;" class="btn pull-right" href="{{route('complaint.index')}}">Show Complaints</a>
                            <p class="card-category"> Enter the Complaints</p>
                        </div>


                        <div style="box-shadow: 0px 0px 3px 1px #ccc;"   class="col-md-6  offset-md-3">

                            @if(Session::has('message'))
                                <div class="alert alert-success"><em> {!! session('message') !!}</em></div>
                            @endif
                            @if(Session::has('message-delete'))
                                @if(Session::has('message-delete'))
                                    <div class="alert alert-danger"></span><em> {!! session('message-delete') !!}</em></div>
                                @endif
                            @endif
                            <form id="myForm" method="POST" action="{{ route('complaint.update',['id'=>$complaint->id]) }}" enctype="multipart/form-data" >
                                {{ method_field('PUT') }}
                                @csrf
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label for="name" class="col-form-label"><label for="form-field-select-3"><h4>Enter CNIC No</h4>
                                            </label></label>
                                        <select  name="user_id" data-placeholder="Choose a CNIC" class="chosen-select dropdown_selector" tabindex="2" id="form-field-select-3">
                                            <option hidden value=""></option>
                                            @foreach(\App\Consumer::all() as $value)
                                                <option  value="{{$value->id}}" @if($complaint->user_id==$value->id) selected @endif>{{$value->cnic}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('user_id'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong style="color: darkred">{{ $errors->first('user_id') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <div id="cons_pict">
                                            <img style="  border-radius: 43px; height: 202px; padding: 24px;" src="{{asset('no-thumbnail.jpeg')}}">
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <p style="margin-top: 40px">
                                        <p><h4 style="color: #807f7f" >Cell No: <span style="color: #438EB9" id="cons_cell"></span> </h4>
                                        <p><h4 style="color: #807f7f" >Name: <span style="color: #438EB9" id="cons_name"></span> </h4>
                                        <p><h4 style="color: #807f7f" >Father Name: <span style="color: #438EB9" id="cons_fname"></span> </h4>
                                        <p><h4 style="color: #807f7f" >Village: <span style="color: #438EB9" id="cons_village"></span> </h4>
                                        <p><h4 style="color: #807f7f" >Card Issue: <span id="cons_card"></span> </h4>
                                        </p>

                                    </div>


                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label for="name" class="col-form-label"><label for="form-field-select-3"><h4>Complaint Status</h4>
                                            </label></label>
                                        <select  name="status" class="dropdown_se chosen-select form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" id="form-field-select-3">
                                            <option hidden value=""></option>
                                            <option  value="0" @if($complaint->status==0) selected @endif>Pending</option>
                                            <option  value="1" @if($complaint->status==1) selected @endif>Processing</option>
                                            <option  value="2" @if($complaint->status==2) selected @endif>Complete</option>
                                        </select>
                                        @if ($errors->has('status'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong style="color: darkred">{{ $errors->first('status') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label style="margin-top: 30px" for="summernote"><h4>Complaint</h4></label>
                                        <textarea class="form-control{{ $errors->has('complaint') ? ' is-invalid' : '' }}" id="summernote" type="text" name="complaint">{{$complaint->complaint}}</textarea>
                                        @if ($errors->has('complaint'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong style="color: darkred">{{ $errors->first('complaint') }}</strong>
                                                    </span>
                                        @endif
                                    </div>


                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary pull-left">
                                            {{ __('Update') }}
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
    <script>

        $(function() {
            $('.chosen-select').chosen();
            $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
        });

        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-success').fadeOut('slow');
            }, 3000);
        });
        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-danger').fadeOut('slow');
            }, 3000);
        });
        $('.edit-btn').click(function (e) {
            var request = $.ajax({
                url: $(this).data('action'),
                type: "get",
                dataType: "html",
                cache: false,
                success:
                    function (result) {
                        $('#edit-content').html(result);

                    },
                error: function (result) {
                    alert('error this form');
                }
            });
        });

        // Summernote
        $('#summernote').summernote({
            height: 200,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', ]],
                ['para', ['ul', 'ol', 'paragraph']],
            ]
        });


        $(document).ready(function () {
//            var $option = $(this).find('option:selected');
            var code =$('.dropdown_selector :selected').text();
//            var code = $option.text();
//            alert(code);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $.ajax({
                url: '/scane2',
                type: 'POST',
                data:{ id :code},
                dataType: 'JSON',

                success:
                    function (result) {
//                        result = JSON.parse(result);
                        $('#cons_pict').html(result.cons_pics);
                        $('#cons_cell').html(result.cons_cell);
                        $('#cons_name').html(result.options);
                        $('#cons_fname').html(result.$cons_fname);
                        $('#cons_village').html(result.$cons_village);
                        $('#cons_card').html(result.cons_card);
                        $('#card_issue_date').html(result.card_issue_date);

                    },
                error: function (result) {
                    alert('error this form');
                }

            });
        });



        $('.dropdown_selector').change(function() {
            var $option = $(this).find('option:selected');
            var code = $option.text();
//            alert(code);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $.ajax({
                url: '/scane2/',
                type: 'POST',
                data:{ id :code},
                dataType: 'JSON',

                success:
                    function (result) {
//                        result = JSON.parse(result);
                        $('#cons_pict').html(result.cons_pics);
                        $('#cons_cell').html(result.cons_cell);
                        $('#cons_name').html(result.options);
                        $('#cons_fname').html(result.$cons_fname);
                        $('#cons_village').html(result.$cons_village);
                        $('#cons_card').html(result.cons_card);
                        $('#card_issue_date').html(result.card_issue_date);

                    },
                error: function (result) {
                    alert('error this form');
                }

            });
        });
    </script>
@endsection

