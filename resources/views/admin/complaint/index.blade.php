@extends('admin.layouts.app2')
@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Complaints</h4>

                            @if(Auth::user()->role==2)
                            <a style="background: linear-gradient(145deg,#ffffff,#ffffff);
                                color: #07cdff;
                                margin-top: -22px;" class="btn pull-right" href="{{route('complaint.create')}}">Add Complaints</a>
                            @endif
                            <p class="card-category"> Showing all the Complaints</p>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    @if(Session::has('message'))
                                        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em></div>
                                    @endif
                                    @if(Session::has('message-delete'))
                                        @if(Session::has('message-delete'))
                                            <div class="alert alert-danger"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message-delete') !!}</em></div>
                                        @endif
                                    @endif
                                    <thead class="text-primary">
                                    <tr>
                                            <th class="detail-col">NO#</th>
                                            <th>Name</th>
                                            <th>Father Name</th>
                                            <th>Village</th>
                                            <th>Cnic</th>
                                            <th style="text-align: center">Status</th>
                                            @if(Auth::user()->role==2)
                                                <th style="text-align:center">Action</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($complaints as $key => $complaint)
                                            <tr>
                                                <td style="text-align: center">
                                                    {{ ($complaints->currentpage() - 1) * $complaints->perpage() + 1 + $key }}

                                                </td>
                                                <td style="text-transform: capitalize">{{ @$complaint->user->name }}</td>
                                                <td style="text-transform: capitalize">{{ @$complaint->user->father_name }}</td>
                                                <td style="text-transform: capitalize">{{ @$complaint->user->village }}</td>
                                                <td style="text-transform: capitalize">{{ @$complaint->user->cnic }}</td>
                                                @if($complaint->status==1)
                                                    <td style="text-align: center"><span class="btn btn-primary  btn-xs">Processing</span></td>
                                                @elseif($complaint->status==2)
                                                    <td style="text-align: center"><span class="btn btn-success  btn-xs">Complete</span></td>
                                                @else
                                                    <td style="text-align: center"><span class="btn btn-warning  btn-xs">Pending</span></td>
                                                @endif

                                                @if(Auth::user()->role==2)
                                                    <td style="text-align: center">
                                                        <div class="hidden-sm hidden-xs btn-group">
                                                            <a title="Update" class="btn btn-xs btn-success edit-btn" href="{{route('complaint.edit',['id'=>$complaint->id])}}" >
                                                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                            </a>

                                                            {{--<a style="color: white" class="btn btn-primary btn-xs edit-btn"  >Edit</a>--}}
                                                            {{--<button title="Delete" type="button" data-toggle="modal"--}}
                                                            {{--data-target="#delete-tehsil"--}}
                                                            {{--data-action="/complaint/{{$complaint->id}}"--}}
                                                            {{--class="show-delete-modal btn btn-xs btn-danger">--}}
                                                            {{--<i class="ace-icon fa fa-trash-o bigger-120"></i>--}}
                                                            {{--</button>--}}
                                                        </div>
                                                    </td>
                                                @endif



                                            </tr>
                                        </tbody>
                                        @endforeach

                                </table>
                                <div class="text-left">{{ $complaints->links() }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--edit part--}}
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><h3>Edit Tehsil
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button></h3>
                    </h5>
                </div>
                <div id="edit-content">
                </div>
            </div>
        </div>
    </div>
    {{--delete modal part--}}
    <div class="modal fade" id="delete-tehsil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST" id="delete-form" action="/complaintn/{{@$complaint->id}}">
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                        <h3>Delete</h3>
                    </div>
                    <div class="modal-body">
                        <p> Are you sure to delete?</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" type="submit">Delete</button>
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-success').fadeOut('slow');
            }, 3000);
        });
        $(document).ready(function () {
            setTimeout(function() {
                $('.alert-danger').fadeOut('slow');
            }, 3000);
        });
        $('.edit-btn').click(function (e) {
            var request = $.ajax({
                url: $(this).data('action'),
                type: "get",
                dataType: "html",
                cache: false,
                success:
                    function (result) {
                        $('#edit-content').html(result);

                    },
                error: function (result) {
                    alert('error this form');
                }
            });
        });

        $('.delete-btn').click(function (e) {
            var url = $(this).data('action');
            $('#delete-form').attr('action',url);
            $("#DangerModalhdbgcl").modal('show');
        });

        // Summernote
        $('#summernote').summernote({
            height: 200
        });
    </script>
@endsection

